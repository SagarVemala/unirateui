Application.$controller("CredentialManagementPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $(document).click(function(event) {
            if ($(event.target).hasClass('modal')) {
                $scope.Widgets.dialogEditCredentials.close();
            }
        });
    };


    $scope.svUpdateCredentialsonSuccess = function(variable, data, options) {
        var message = ($scope.Widgets.FormEditCredentials.formWidgets.form_field_Bulk.datavalue == "Yes") ? (" all states for ") : ($scope.Widgets.FormEditCredentials.formWidgets.form_field_State.datavalue + " for ");
        $scope.$root.notifyToUser("Credentials of " + message + $scope.Widgets.FormEditCredentials.formWidgets.form_field_Carrier.displayValue + " updated successfully", "success");
    };

}]);


Application.$controller("tableCredentialsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);


Application.$controller("dialogEditCredentialsController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.form_field_BulkChange = function($event, $isolateScope, newVal, oldVal) {
            if (newVal == "Yes") {
                $scope.Widgets.FormEditCredentials.formWidgets.form_field_State.datavalue = undefined;
            } else {
                $scope.Widgets.FormEditCredentials.formWidgets.form_field_State.datavalue = $scope.Widgets.tableCredentials.selecteditem.stateCd;
                $scope.Widgets.FormEditCredentials.formWidgets.form_field_Carrier.datavalue = $scope.Widgets.tableCredentials.selecteditem.carrierListId;
            }
        };


        $scope.buttonSubmitClick = function($event, $isolateScope) {
            $scope.Widgets.FormEditCredentials.submit();
        };

    }
]);

Application.$controller("dialogConfirmEditController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);