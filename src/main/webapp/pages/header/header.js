Application.$controller("headerPageController", ["$scope", "$window", "$timeout", function($scope, $window, $timeout) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         * 
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.button1Click = function($event, $isolateScope) {

        var key = $scope.Widgets.nameKey.datavalue;
        $scope.Variables.getClientDetailsByName.setInput("k", key);
        $scope.Variables.getClientDetailsByName.invoke();
    };


    $scope.button4Click = function($event, $isolateScope, $window) {

        localStorage.removeItem('policyid');
        if (window.location.href.indexOf('AutoQuote') > 0) {
            window.location.href = $scope.removeParam("policyid");
            window.location.reload();
        } else {
            $scope.Variables.RequestFromClientPage.dataSet.dataValue = false
            $scope.Variables.requestForQuotesTab.dataSet.dataValue = false
            $scope.Actions.goToPage_AutoQuote.invoke();
        }
        //this is for page reload 
        sessionStorage.setItem('completedNewIteration', false);
        sessionStorage.setItem('currentPolicyId', 'undefined');
    };

    $scope.getClientDetailsByNameTable1Click = function($event, $isolateScope, $rowData) {

        console.log($scope.Variables.GetRecentQuotesByUser.dataSet.content[0].applicantId);
        console.log($scope.Variables.GetRecentQuotesByUser.dataSet.content[0].policyId);
        /*var polId = $policyId;
        $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;
        $scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
        $scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
        //if ($rowData.status == 'Y') {
        $scope.Variables.completedNewIteration.dataSet.dataValue = true;*/
        //} else {
        //    $scope.Variables.completedNewIteration.dataSet.dataValue = false;
        //}
        var polId = $scope.Variables.GetRecentQuotesByUser.dataSet.content[0].policyId;
        $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;
        $scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
        $scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
        $scope.Variables.completedNewIteration.dataSet.dataValue = false;

        //$scope.Actions.goToPage_AutoQuote.invoke();
    };

    $scope.getClientDataForPolicy = function(polId) {
        // var q = "policyId = " + polId;
        // $scope.Variables.getApplicantByPolicyId.setInput("q", q);
        // $scope.Variables.getApplicantByPolicyId.invoke();

        //$rootScope.loadAllDataForPolicy(polId);
        $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;



        // $rootScope.$emit('loadQuoteData');
    };


    $scope.label5Mouseenter = function($event, $isolateScope) {
        $scope.showRecentQuotesPopOver();
    };


    $scope.picture3Mouseenter = function($event, $isolateScope) {
        $scope.showRecentQuotesPopOver();
    };

    $scope.showRecentQuotesPopOver = function() {
        $scope.Widgets;
        console.log('Show Recent Quotes Pop Over');
        console.log($scope.Widgets.popover1);
        $scope.Widgets.popover1.show = true;
    }

    $scope.hideRecentQuotesPopOver = function() {
        $scope.Widgets;
        console.log('Hide Recent Quotes Pop Over');
        console.log($scope.Widgets.popover1);
        $scope.Widgets.popover1.show = false;
    }


    $scope.label5Mouseleave = function($event, $isolateScope) {
        $scope.hideRecentQuotesPopOver();
    };


    $scope.picture3Mouseleave = function($event, $isolateScope) {
        $scope.hideRecentQuotesPopOver();
    };


    $scope.nameKeyKeydown = function($event, $isolateScope) {

    };


    $scope.nameKeyKeyup = function($event, $isolateScope) {

        if ($event.key == "Enter") {
            var key = $scope.Widgets.nameKey.datavalue;
            $scope.Variables.getClientDetailsByName.setInput("k", key);
            $scope.Variables.getClientDetailsByName.invoke();
            $scope.Actions.goToPage_ClientsPage.invoke();
        }


    };

    $scope.removeParam = function(parameter) {
        var url = window.location.href;
        var urlparts = url.split('?');

        if (urlparts.length >= 2) {
            var urlBase = urlparts.shift();
            var queryString = urlparts.join("?");

            var prefix = encodeURIComponent(parameter) + '=';
            var pars = queryString.split(/[&;]/g);
            for (var i = pars.length; i-- > 0;)
                if (pars[i].lastIndexOf(prefix, 0) !== -1)
                    pars.splice(i, 1);
            url = urlBase + '?' + pars.join('&');
            window.history.pushState('', document.title, url); // added this line to push the new url directly to url bar .

        }
        return url;
    };

}]);