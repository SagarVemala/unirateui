Application.$controller("ansContainerPageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {

        $scope.carrierQAnswersDropdownMap = $rootScope.carrierQAnswersDropdownMap;





        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };



    // $scope.getDBValue = function(qmi) {

    //     debugger;
    //     //console.log($scope);
    //     console.log("setting carrier page value to" + $scope.$root.carriersAnswersMap.get(qmi));
    //     return $scope.$root.carriersAnswersMap.get(qmi);
    //     // return $rootScope.get(qmi);

    // }



    $scope.getAnswersByQuestiononSuccess = function(variable, data, options) {


    };


    $scope.RequestBody_questionAnswerIdBlur = function($event, $isolateScope) {
        debugger;
        console.log($event);
        console.log(this);
        console.log($isolateScope);
        $scope.Widgets;

        if ($scope.Widgets.RequestBody_questionAnswerId.required && !$scope.Widgets.RequestBody_questionAnswerId._model_ && !$scope.Widgets.RequestBody_questionAnswerId.disabled && !$scope.Widgets.RequestBody_questionAnswerId.readonly) {
            $scope.Widgets.ErrorMessageText.show = true;
        } else {
            $scope.Widgets.ErrorMessageText.show = false;
        }
    };


    $scope.answersBlur = function($event, $isolateScope) {
        debugger;
        console.log($event);
        console.log(this);
        console.log($isolateScope);
        $scope.Widgets;

        if ($scope.Widgets.answers.required && ($scope.Widgets.answers._model_ == -1 || !$scope.Widgets.answers.displayValue || $scope.Widgets.answers.displayValue.toUpperCase() == "SELECT ONE" || $scope.Widgets.answers.displayValue.toUpperCase() == "SELECT") && !$scope.Widgets.answers.disabled && !$scope.Widgets.answers.readonly) {
            $scope.Widgets.ErrorMessageSelect.show = true;
        } else {
            $scope.Widgets.ErrorMessageText.show = false;
        }
    };

}]);
