Application.$controller("QuoteSubmissionPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        console.log($scope)

        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */

        $(document).click(function(event) {
            if ($(event.target).hasClass('modal')) {
                $scope.Widgets.dialog1.close();
            }
        });

    };


    $scope.GetQuotesTable1Rowclick = function($event, $isolateScope, $rowData) {
        if ($event.target.textContent == "RESUBMIT" || $event.target.textContent == "VIEW / EDIT") {

        } else {
            $scope.Variables.PusherCarrier.dataSet = []
            //$scope.$parent.Widgets.PusherCarrierList1.binddataset;


            $scope.policyId = $rowData.policyId
            $scope.submittedStatus = $rowData.submittedStatus
            $scope.quoteTime = $rowData.submitted
            $scope.$apply();

            var driver = $scope.Variables.GetDriverByPolicyId
            driver.invoke();

            var coverage = $scope.Variables.GetImCoverageByPolicyId
            coverage.invoke();

            var vehicle = $scope.Variables.GetImVehicleCoverage
            vehicle.invoke();

            $scope.Variables.getPolicyVehicle.invoke();
            // $scope.Variables.getPolicyVehicle.update();

            //imaginea
            console.log($scope)


            // Variables.getPolicyVehicles.dataSet.year + " " + Variables.getPolicyVehicles.dataSet.make + "-" + Variables.getPolicyVehicles.dataSet.model


            // var vehicleDesc=


            var sv = $scope.Variables.GetQuoteXml
            sv.setInput("policyId", $rowData.policyId)
            sv.invoke();

            if ($rowData.submittedStatus != "incomplete") {
                $scope.Widgets.dialog1.open()
            }
        }
    };

    $scope.GetQuotesTable1Datarender = function($isolateScope, $data) {
        for (var i = 0; i < $data.length; i++) {
            if ($data[i].submittedTime != null) {
                var date1 = new Date(new Date().toISOString().slice(0, 19));
                var date2 = new Date($data[i].submittedTime);
                var startTime = new Date($data[i].submittedTime);
                startTime = new Date(startTime.getTime() - (startTime.getTimezoneOffset() * 60000));
                $data[i].submittedStatus = 'complete';
                var hrs = startTime.getHours();
                var min = startTime.getMinutes();
                var ampm = hrs >= 12 ? 'PM' : 'AM';
                hrs = hrs % 12;
                hrs = hrs ? hrs : 12;
                min = min < 10 ? '0' + min : min;
                $data[i].dateCust = (startTime.getMonth() + 1) + "/" + startTime.getDate() + "/" + startTime.getFullYear() + "  " + hrs + ':' + min + ' ' + ampm;
                var theevent = date2;
                var sec_num = (date1 - theevent) / 1000;
                var days = Math.floor(sec_num / (3600 * 24));
                var hours = Math.floor((sec_num - (days * (3600 * 24))) / 3600);
                var minutes = Math.floor((sec_num - (days * (3600 * 24)) - (hours * 3600)) / 60);
                var seconds = Math.floor(sec_num - (days * (3600 * 24)) - (hours * 3600) - (minutes * 60));
                if (days > 0) {
                    $data[i]["submitted"] = days + ((days > 1) ? " days ago" : " day ago");
                } else if (hours > 0) {
                    $data[i]["submitted"] = hours + ((hours > 1) ? " hours ago" : " hour ago");
                } else if (minutes > 0) {
                    $data[i]["submitted"] = minutes + ((minutes > 1) ? " minutes ago" : " minute ago");
                } else if (seconds > 0) {
                    $data[i]["submitted"] = seconds + ((seconds > 1) ? " seconds ago" : " second ago");
                }
            } else if ($data[i].submittedTime == null) {
                $data[i].submittedStatus = 'incomplete';
                $data[i].dateCust = "Incomplete";
            }
        }
    };


    $scope.CopyQuoteProconSuccess = function(variable, data, options) {
        $scope.Variables.selectedPolicyId.dataSet.dataValue = data.newPolicyId;
        $scope.Variables.requestForQuotesTab.dataSet.dataValue = true;
        $scope.Variables.RequestFromClientPage.dataSet.dataValue = false;
        $scope.Variables.goToPage_AutoQuote.invoke()
    };


    $scope.getPolicyVehicleonSuccess = function(variable, data, options) {

        var i = 0;
        for (var k in $scope.Widgets.GetImVehicleCoverageList1.Widgets) {

            if (k.indexOf('VehicleDisplay') > -1) {

                $scope.Widgets.GetImVehicleCoverageList1.Widgets[k].caption = data.content[i].year + " " + data.content[i].make + " " + data.content[i].model;
                i++;
            }
        }
    };

}]);


Application.$controller("GetQuotesTable1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;


        $scope.customRow1Action = function($event, $rowData) {

            if ($rowData.submittedStatus == 'complete') {
                $scope.Variables.completedNewIteration.dataSet.dataValue = true;
            } else {
                $scope.Variables.completedNewIteration.dataSet.dataValue = false;
            }
            var sv = $scope.Variables.CopyQuoteProc
            sv.setInput("policy_id", $rowData.policyId)
            sv.setInput("new_policy_id", 1)
            sv.invoke();
        };


        $scope.customRowAction = function($event, $rowData) {

            $scope.policyId = $rowData.policyId
            $scope.Variables.selectedPolicyId.dataSet.dataValue = $rowData.policyId
            $scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
            $scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
            $scope.$apply();
            if ($rowData.submittedStatus == 'complete') {
                $scope.Variables.completedNewIteration.dataSet.dataValue = true
                $scope.Variables.QuoteSubmittedTime.dataSet.dataValue = $rowData.dateCust;

            } else {
                $scope.Variables.completedNewIteration.dataSet.dataValue = false;
                $scope.Variables.QuoteSubmittedTime.dataSet.dataValue = "";

            }
            $scope.Variables.goToPage_AutoQuote.invoke()
        };

    }
]);

Application.$controller("dialog1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.button2Click = function($event, $isolateScope, item, currentItemWidgets) {
            $scope.Widgets.dialog1.close()
            if ($scope.submittedStatus == 'complete') {
                $scope.Variables.completedNewIteration.dataSet.dataValue = true;
            } else {
                $scope.Variables.completedNewIteration.dataSet.dataValue = false;
            }
            var sv = $scope.Variables.CopyQuoteProc
            sv.setInput("policy_id", $scope.policyId)
            sv.setInput("new_policy_id", 1)
            sv.invoke();
        };

        $scope.button1Click = function($event, $isolateScope, item, currentItemWidgets) {
            $scope.Widgets.dialog1.close()
            $scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
            $scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
            $scope.$apply();
            if ($scope.submittedStatus == 'complete') {
                $scope.Variables.completedNewIteration.dataSet.dataValue = true;
            } else {
                $scope.Variables.completedNewIteration.dataSet.dataValue = false;
            }
            $scope.Variables.goToPage_AutoQuote.invoke()
        };

    }
]);
