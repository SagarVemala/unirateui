Application.$controller("AutoQuoteNavPageController", ["$scope", "$timeout", "$location", "$rootScope", "$window", function($scope, $timeout, $location, $rootScope, $window) {
    "use strict";
    $scope.newGroupFields = [];
    $scope.carrierTabData = [];
    // This is industry lis for which we are controlling occupation options and functionality
    var specialIndustries = ["Homemaker/Houseprsn", "Retired", "Unemployed", "Disabled"];
    //State exclusions for Medical Payments
    var mp = ['DC', 'DE', 'OR', 'PA', 'MI']
    //State exclusions for uninsured motorist
    var um = ['CA', 'FL', 'MD'];
    //State exclusions for underinsured motorist
    var uim = ['CA', 'FL', 'MD'];

    // function to mask ssn widget value 
    function maskSSN(element, value) {
        if (value !== undefined && value !== "" && value !== null) {
            var res = value,
                len = res.length,
                max = 9,
                stars = len > 0 ? len > 1 ? len > 2 ? len > 3 ? len > 4 ? 'XXX-XX-' : 'XXX-X' : 'XXX-' : 'XX' : 'X' : '',

                result = stars + res.substring(5);
            element.val(result);
        }

    }

    // function to mask date widget value 
    //Separate function for DL due to frequent requirment changes
    function maskDL(element, value) {
        if (value !== undefined && value !== "" && value !== null) {
            var unMaskVal = (value.length > 4) ? value.slice(0, value.length - 4) : '';
            element.val(unMaskVal + "****");
        }
    }

    // function to remove unwanted characters from SSN
    $scope.formatSSN = function(value) {
        if (value !== undefined && value !== "" && value !== null) {
            return value.replace("-", "");
        }
    }

    // function to format Date
    $scope.formatDate = function(value) {
        if (value !== undefined && value !== "" && value !== null) {
            var formatedDate = value.replace(/[\W_]/g, "");

            var month = formatedDate.substring(0, 2);
            var day = formatedDate.substring(2, 4);
            var year = formatedDate.substring(4);
            formatedDate = month + '/' + day + '/' + year;

            return formatedDate;
        }
    }

    // function to mask pre loaded data
    function maskLoadedData() {
        // For Applicant
        maskDL($("[name=licenseToDrive_formWidget]"), $scope.Widgets.CreateImApplicantForm1.formWidgets.licenseToDrive.datavalue);
        maskSSN($("[name=ssn_formWidget]"), $scope.formatSSN($scope.Widgets.CreateImApplicantForm1.formWidgets.ssn.datavalue));
        // For Co Applicant
        maskSSN($("[name=RequestBody_ssn_formWidget]"), $scope.formatSSN($scope.Widgets.CoApplicant.formWidgets.RequestBody_ssn.datavalue));
    }

    /**Function to control disable property of fields in UI. 
      Needs to be refactored later for better performance.
      Diasble each field on variable success
    **/
    $scope.isLocked = function(fieldName) {
        var disable = false;
        _.forEach($scope.Variables.svFetchGeneralDefaultsForListOfStates.dataSet.content, function(obj) {
            if (obj.listDesc == fieldName) {
                if (obj.defaultLock == 'Yes') {
                    disable = true;
                }
            }
        });
        return disable;
    };

    //Setting default drivers for vehicle coverages
    $scope.setCoverageDrivers = function() {
        if ($scope.Variables.getDriversByPolicyId.dataSet.content && $scope.Variables.getPolicyVehicles.dataSet.content) {
            for (var i = 0; i < $scope.Variables.getPolicyVehicles.dataSet.content.length; i++) {
                var formName = "CreateImVehicleCoverageForm" + i;
                if ($scope.Widgets[formName] != undefined) {
                    debugger
                    // if ($scope.Widgets[formName].formWidgets.Driver_assignment.datavalue == undefined) {

                    if ($scope.Variables.getDriversByPolicyId.dataSet.content.length > i) {
                        $scope.Widgets[formName].formWidgets.Driver_assignment.datavalue = $scope.Variables.getDriversByPolicyId.dataSet.content[i].driverId;
                    } else {
                        $scope.Widgets[formName].formWidgets.Driver_assignment.datavalue = $scope.Variables.getDriversByPolicyId.dataSet.content[0].driverId
                    }


                    // }
                }
            }
        }
    }

    // Set element  visibility
    $scope.setColumnVisibility = function(stateList, element) {
        debugger
        //var element = $("[name=gridcolumnMI]")
        if (stateList.includes($scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_stateCode.datavalue)) {
            element.addClass('hide-elementcls');
        } else {
            element.removeClass('hide-elementcls');
        }
    }

    // Toggle Display of Column in Coverages to fill empty spaces of unavailable fileds
    function manageCoverageDisplayFields() {
        $scope.setColumnVisibility(mp, $("[name=gridcolumnMI]"));
        $scope.setColumnVisibility(um, $("[name=gridcolumnUM]"));
        $scope.setColumnVisibility(uim, $("[name=gridcolumnUIM]"));
    }


    $scope.carriersAnswersMap = new Map();



    $scope.carrierQAnswersDropdownMap = new Map();

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {

        debugger;
        $scope.HideAutoTabs();
        //$scope.policyId = "";

        // $scope.eligibilityId = null;
        $scope.stateLoopVariable = "";


        $scope.inCompleted = false;

        // $scope.completedNewIteration = false;
        $scope.currentDate = null;
        $scope.courseMinDate = null;
        var polId = localStorage.getItem('policyid');
        var currPolicyId = null
        if (sessionStorage.hasOwnProperty('currentPolicyId')) {
            currPolicyId = sessionStorage.getItem('currentPolicyId');
        }
        if (!$scope.Variables.RequestFromClientPage.dataSet.dataValue && !$scope.Variables.requestForQuotesTab.dataSet.dataValue && (currPolicyId == null || currPolicyId == "undefined" || currPolicyId == "null" || currPolicyId == undefined) &&
            (polId == "null" || polId == null || polId == undefined)) {
            $scope.Variables.CreateImPolicyInfoInvoke.invoke();
        }
        //reload refresh issue

        //check for page reload , not result of client search or quote submission click
        if (currPolicyId != null && currPolicyId.length > 0 && currPolicyId != "undefined" && currPolicyId != "null" && $scope.Variables.selectedPolicyId.dataSet.dataValue.length == 0 && (polId == "null" || polId == null || polId == undefined)) {
            $scope.ShowAutoTabs()
            //$scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
            $scope.policyId = currPolicyId;
            $scope.Variables.selectedPolicyId.dataSet.dataValue = currPolicyId;
            $scope.loadAllQuoteData();

            var completedNewIteration = sessionStorage.getItem('completedNewIteration')

            if (completedNewIteration != null && completedNewIteration != 'undefined') {
                if (completedNewIteration == "true") {
                    $scope.Variables.completedNewIteration.dataSet.dataValue = true;
                } else {
                    $scope.Variables.completedNewIteration.dataSet.dataValue = false;
                }

            }
        }

        // Pusher listener
        Pusher.logToConsole = true;
        $scope.pusher = new Pusher('f56e6bdbe2295ad8fef7', {
            cluster: 'mt1',
            forceTLS: true
        });
        $scope.carriersMap = new Map();


        if ($scope.Variables.RequestFromClientPage.dataSet.dataValue) {
            $scope.clientButtonClick = true
            $scope.ShowAutoTabs()
            sessionStorage.setItem('completedNewIteration', $scope.Variables.completedNewIteration.dataSet.dataValue);
            //reload issue
            sessionStorage.setItem('currentPolicyId', $scope.Variables.selectedPolicyId.dataSet.dataValue);

            // $scope.Variables.RequestFromClientPage.dataSet.dataValue = false;
            $scope.inCompleted = !$scope.Variables.completedNewIteration.dataSet.dataValue;
            $scope.respolicyId = $scope.Variables.selectedPolicyId.dataSet.dataValue;
            $scope.loadAllQuoteData();
            $scope.Variables.PusherCarrier.dataSet = [];
            if ($scope.Variables.completedNewIteration.dataSet.dataValue) {
                if ($scope.Variables.requestForQuotesTab.dataSet.dataValue) {
                    $scope.Widgets.tabs1.goToTab(9, {});
                } else {
                    $scope.Widgets.tabs1.goToTab(1, {});
                }

                var sv = $scope.$parent.$parent.Variables.GetQuoteXml
                sv.setInput("policyId", $scope.policyId)
                sv.invoke();
            } else {
                $scope.Widgets.tabs1.goToTab(1, {});
            }
        }
        if ($scope.Variables.requestForQuotesTab.dataSet.dataValue) {
            $scope.clientButtonClick = true
            $scope.Variables.PusherCarrier.dataSet = [];
            var query = "policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;
            $scope.viewQuery = query;
            sessionStorage.setItem('completedNewIteration', $scope.Variables.completedNewIteration.dataSet.dataValue);
            //reload issue
            sessionStorage.setItem('currentPolicyId', $scope.Variables.selectedPolicyId.dataSet.dataValue);

            $scope.ShowAutoTabs()
            $scope.Widgets.tabs1.goToTab(9, {});
            $scope.policyId = $scope.Variables.selectedPolicyId.dataSet.dataValue;


            //$scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
            var sv = $scope.Variables.GetCarrierXml;
            sv.setInput("policyId", $scope.policyId);

            if ($scope.Variables.SelectedCarrierIds.dataSet !== undefined && $scope.Variables.SelectedCarrierIds.dataSet.length > 0) {
                sv.setInput("carrierIds", $scope.Variables.SelectedCarrierIds.dataSet);
                $scope.Variables.SelectedCarrierIds.dataSet = [];
            }
            sv.invoke();
            debugger;
            $scope.loadAllQuoteData();

            if (!$scope.Variables.requestFromSearchPage.dataSet.dataValue) {
                //from quote resubmit
                $timeout(function() {
                    debugger
                    $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.ID = $scope.policyId;

                    $scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime = new Date().toISOString().slice(0, 19);
                    $scope.Variables.getPolicyInfo.dataSet.content[0].submittedStatus = 'Y';

                    $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.RequestBody = $scope.Variables.getPolicyInfo.dataSet.content[0]
                    $scope.Variables.UpdateImPolicyInfoInvoke.invoke();
                }, 3000);

            }

        }
        $scope.todaysDate = new Date();

        $scope.courseMinDate = new Date().setFullYear($scope.todaysDate.getFullYear() - 3);




    };

    $scope.getGenderName = function(appContent) {
        return "";

    }

    $scope.CreateImPolicyInfoInvokeonSuccess = function(variable, data, options) {
        $scope.policyId = data.policyId;
        $scope.Variables.selectedPolicyId.dataSet.dataValue = data.policyId
        //refresh page load changes
        sessionStorage.setItem('currentPolicyId', $scope.policyId);
        sessionStorage.setItem('completedNewIteration', false);
    };


    $scope.HideAutoTabs = function() {

        for (var i = 1; i < $scope.Widgets.tabs1.tabs.length; i++) {
            $scope.Widgets.tabs1.tabs[i].show = false
        }
    };

    $scope.ShowAutoTabs = function() {
        for (var i = 1; i < $scope.Widgets.tabs1.tabs.length; i++) {
            $scope.Widgets.tabs1.tabs[i].show = true
        }
    };

    $scope.CreateImIncidentInvokeonSuccess = function(variable, data, options) {;
        var driverIdList = "";
        if ($scope.Variables.driversIds.dataSet.length > 0) {
            for (var id in $scope.Variables.driversIds.dataSet) {
                driverIdList = driverIdList + $scope.Variables.driversIds.dataSet[id] + ",";
            }
        }

        driverIdList = driverIdList.substring(0, driverIdList.length - 1);
        var driverId = driverIdList;
        driverIdList = '(' + driverIdList + ')';

        var query = "incidentType='Accident'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke.invoke();

        //$scope.Variables.GetAccidentList.setInput("driverIds", driverId);
        //$scope.Variables.GetAccidentList.invoke();

        $scope.Variables.GetAccidentListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetAccidentListByPolicyId.invoke();

        query = "incidentType='Violation'  and driverId in " + driverIdList;
        $scope.Variables.GetIncidentTypeListInvoke1.setInput("q", query);
        $scope.Variables.GetIncidentTypeListInvoke1.invoke();

        //query = "incidentType='Comp Loss'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke2.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke2.invoke();
        $scope.Variables.GetCompLossListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetCompLossListByPolicyId.invoke();

        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = "";

        $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setPristine();
        $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setUntouched();

        $scope.Widgets.RequestBody_incidentType.datavalue = 'Accident';

    };

    $scope.GetApplicantDataOnLoad = function($event, $isolateScope) {;
        var polId = localStorage.getItem('policyid');
        var salesforce_userid = localStorage.getItem('salesforce_userid');
        var clientappid = localStorage.getItem('clientappid');
        var quoteNumber = localStorage.getItem('quotenumber');
        var aiuserid = localStorage.getItem('aiuserid');

        if (polId != "null" && polId != null && polId != undefined) {
            $scope.policyId = polId;
            /*$scope.Variables.GetApplicantInfoByPolicyIdVar.setInput("policyId", $scope.policyId);
            $scope.Variables.GetApplicantInfoByPolicyIdVar.invoke();

            $scope.Variables.GetCoApplicantInfoByPolicyIdVar.setInput("policyId", $scope.policyId);
            $scope.Variables.GetCoApplicantInfoByPolicyIdVar.invoke();*/

            $scope.inCompleted = true;
            $scope.Variables.selectedPolicyId.dataSet.dataValue = $scope.policyId;
            debugger;
            $scope.loadAllQuoteData();

            $scope.salesForceUserId = salesforce_userid;
            $scope.clientAppId = clientappid;
            $scope.aiUserId = aiuserid;
            //$scope.userId = aiuserid;

        }

    };

    $scope.TriggerEligibilityValidations = function($event, $isolateScope) {

        $scope.Widgets.CreateImEligibilityForm1.CreateImEligibilityForm1.RequestBody_questionAnswer6_formWidget.$touched = true;

        $scope.Widgets.CreateImEligibilityForm1.CreateImEligibilityForm1.RequestBody_questionAnswer11_formWidget.$touched = true;

        $scope.Widgets.CreateImEligibilityForm1.CreateImEligibilityForm1.RequestBody_questionAnswer8_formWidget.$touched = true;

        $scope.Widgets.CreateImEligibilityForm1.CreateImEligibilityForm1.toyType_formWidget.$touched = true;

        $scope.Widgets.CreateImEligibilityForm1.CreateImEligibilityForm1.homeType_formWidget.$touched = true;

    };

    $scope.TriggerClientValidations = function($event, $isolateScope) {

        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.firstName_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.lastName_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.dob_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.RequestBody_education_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.RequestBody_industry_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.occupation_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.maritalStatus_formWidget.$touched = true;


        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.CreateImAddressForm1.RequestBody_address2_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.CreateImAddressForm1.RequestBody_city_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.CreateImAddressForm1.RequestBody_stateCode_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.CreateImAddressForm1.RequestBody_county_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.CreateImAddressForm1.RequestBody_zipCode5_formWidget.$touched = true;

        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.CreateImAddressForm1.createResidenceInfoForm1.years_at_current_formWidget.$touched = true;
        $scope.Widgets.CreateImApplicantForm1.CreateImApplicantForm1.CreateImAddressForm1.createResidenceInfoForm1.months_at_current_formWidget.$touched = true;

        $scope.Widgets.CoApplicant.CoApplicant.RequestBody_firstName_formWidget.$touched = true;
        $scope.Widgets.CoApplicant.CoApplicant.form_field121_formWidget.$touched = true;
        $scope.Widgets.CoApplicant.CoApplicant.RequestBody_dob_formWidget.$touched = true;
        $scope.Widgets.CoApplicant.CoApplicant.RequestBody_maritalStatus_formWidget.$touched = true;
        $scope.Widgets.CoApplicant.CoApplicant.RequestBody_relation_formWidget.$touched = true;
        $scope.Widgets.CoApplicant.CoApplicant.RequestBody_industry_formWidget.$touched = true;
        $scope.Widgets.CoApplicant.CoApplicant.RequestBody_occupation_formWidget.$touched = true;

    };

    $scope.TriggerPolicyValidations = function($event, $isolateScope) {

        $scope.Widgets.CreateImPriorPolicyInfoForm1.CreateImPriorPolicyInfoForm1.RequestBody_priorCarrierType_formWidget.$touched = true;
        $scope.Widgets.CreateImPriorPolicyInfoForm1.CreateImPriorPolicyInfoForm1.RequestBody_expiration_formWidget.$touched = true;
        $scope.Widgets.CreateImPriorPolicyInfoForm1.CreateImPriorPolicyInfoForm1.RequestBody_yearsWithPriorCarrier_formWidget.$touched = true;
        $scope.Widgets.CreateImPriorPolicyInfoForm1.CreateImPriorPolicyInfoForm1.RequestBody_priorLiabilityLimit_formWidget.$touched = true;
        $scope.Widgets.CreateImPriorPolicyInfoForm1.CreateImPriorPolicyInfoForm1.RequestBody_yearsWithContinuousCoverage_formWidget.$touched = true;

        $scope.Widgets.CreateImPriorPolicyInfoForm1.CreateImPriorPolicyInfoForm1.UpdateImPolicyInfoForm1.RequestBody_effective_formWidget.$touched = true;
    };

    $scope.TriggerDriverValidations = function($event, $isolateScope) {
        debugger

        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_firstname_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_lastName_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_dob_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_relation_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_maritalStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_driverLicenseStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_ageLicensed_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_industry0_formWidget.$touched = true;
        $scope.Widgets.DriverForm0.DriverForm0.RequestBody_occupation_formWidget.$touched = true;

        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_firstname_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_lastName_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_dob_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_relation_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_maritalStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_driverLicenseStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_ageLicensed_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_industry1_formWidget.$touched = true;
        $scope.Widgets.DriverForm1.DriverForm1.RequestBody_occupation1_formWidget.$touched = true;

        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_firstname_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_lastName_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_dob_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_relation_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_maritalStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_driverLicenseStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_ageLicensed_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_industry2_formWidget.$touched = true;
        $scope.Widgets.DriverForm2.DriverForm2.RequestBody_occupation1_formWidget.$touched = true;

        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_firstname_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_lastName_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_dob_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_relation_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_maritalStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_driverLicenseStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_ageLicensed_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_industry3_formWidget.$touched = true;
        $scope.Widgets.DriverForm3.DriverForm3.RequestBody_occupation1_formWidget.$touched = true;

        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_firstname_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_lastName_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_dob_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_relation_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_maritalStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_driverLicenseStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_ageLicensed_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_industry4_formWidget.$touched = true;
        $scope.Widgets.DriverForm4.DriverForm4.RequestBody_occupation1_formWidget.$touched = true;

        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_firstname_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_lastName_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_dob_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_relation_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_maritalStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_driverLicenseStatus_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_ageLicensed_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_industry5_formWidget.$touched = true;
        $scope.Widgets.DriverForm5.DriverForm5.RequestBody_occupation1_formWidget.$touched = true;

    };

    $scope.TriggerVehiclesValidations = function($event, $isolateScope) {

        $scope.Widgets.VehicleForm0.VehicleForm0.RequestBody_RequestBody_vin_formWidget.$touched = true;
        $scope.Widgets.VehicleForm0.VehicleForm0.RequestBody_year_formWidget.$touched = true;
        $scope.Widgets.VehicleForm0.VehicleForm0.RequestBody_make_formWidget.$touched = true;
        $scope.Widgets.VehicleForm0.VehicleForm0.RequestBody_model_formWidget.$touched = true;
        $scope.Widgets.VehicleForm0.VehicleForm0.RequestBody_subModel_formWidget.$touched = true;
        $scope.Widgets.VehicleForm0.VehicleForm0.RequestBody_antiTheft_formWidget.$touched = true;

        $scope.Widgets.CreateImVehicleUseForm0.CreateImVehicleUseForm0.RequestBody_costNew_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm0.CreateImVehicleUseForm0.RequestBody_useage_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm0.CreateImVehicleUseForm0.RequestBody_oneWayMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm0.CreateImVehicleUseForm0.RequestBody_annualMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm0.CreateImVehicleUseForm0.RequestBody_ownership_formWidget.$touched = true;

        $scope.Widgets.CreateVehicleAddressForm0.CreateVehicleAddressForm0.RequestBody_address2_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm0.CreateVehicleAddressForm0.RequestBody_city_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm0.CreateVehicleAddressForm0.RequestBody_stateCode_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm0.CreateVehicleAddressForm0.RequestBody_county_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm0.CreateVehicleAddressForm0.RequestBody_zipCode5_formWidget.$touched = true;

        $scope.Widgets.VehicleForm1.VehicleForm1.RequestBody_RequestBody_vin_formWidget.$touched = true;
        $scope.Widgets.VehicleForm1.VehicleForm1.RequestBody_year_formWidget.$touched = true;
        $scope.Widgets.VehicleForm1.VehicleForm1.RequestBody_make_formWidget.$touched = true;
        $scope.Widgets.VehicleForm1.VehicleForm1.RequestBody_model_formWidget.$touched = true;
        $scope.Widgets.VehicleForm1.VehicleForm1.RequestBody_subModel_formWidget.$touched = true;
        $scope.Widgets.VehicleForm1.VehicleForm1.RequestBody_antiTheft_formWidget.$touched = true;

        $scope.Widgets.CreateImVehicleUseForm1.CreateImVehicleUseForm1.RequestBody_costNew_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm1.CreateImVehicleUseForm1.RequestBody_useage_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm1.CreateImVehicleUseForm1.RequestBody_oneWayMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm1.CreateImVehicleUseForm1.RequestBody_annualMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm1.CreateImVehicleUseForm1.RequestBody_ownership_formWidget.$touched = true;

        $scope.Widgets.CreateVehicleAddressForm1.CreateVehicleAddressForm1.RequestBody_address2_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm1.CreateVehicleAddressForm1.RequestBody_city_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm1.CreateVehicleAddressForm1.RequestBody_stateCode_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm1.CreateVehicleAddressForm1.RequestBody_county_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm1.CreateVehicleAddressForm1.RequestBody_zipCode5_formWidget.$touched = true;

        $scope.Widgets.VehicleForm2.VehicleForm2.RequestBody_RequestBody_vin_formWidget.$touched = true;
        $scope.Widgets.VehicleForm2.VehicleForm2.RequestBody_year_formWidget.$touched = true;
        $scope.Widgets.VehicleForm2.VehicleForm2.RequestBody_make_formWidget.$touched = true;
        $scope.Widgets.VehicleForm2.VehicleForm2.RequestBody_model_formWidget.$touched = true;
        $scope.Widgets.VehicleForm2.VehicleForm2.RequestBody_subModel_formWidget.$touched = true;
        $scope.Widgets.VehicleForm2.VehicleForm2.RequestBody_antiTheft_formWidget.$touched = true;

        $scope.Widgets.CreateImVehicleUseForm2.CreateImVehicleUseForm2.RequestBody_costNew_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm2.CreateImVehicleUseForm2.RequestBody_useage_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm2.CreateImVehicleUseForm2.RequestBody_oneWayMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm2.CreateImVehicleUseForm2.RequestBody_annualMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm2.CreateImVehicleUseForm2.RequestBody_ownership_formWidget.$touched = true;

        $scope.Widgets.CreateVehicleAddressForm2.CreateVehicleAddressForm2.RequestBody_address2_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm2.CreateVehicleAddressForm2.RequestBody_city_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm2.CreateVehicleAddressForm2.RequestBody_stateCode_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm2.CreateVehicleAddressForm2.RequestBody_county_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm2.CreateVehicleAddressForm2.RequestBody_zipCode5_formWidget.$touched = true;

        $scope.Widgets.VehicleForm3.VehicleForm3.RequestBody_RequestBody_vin_formWidget.$touched = true;
        $scope.Widgets.VehicleForm3.VehicleForm3.RequestBody_year_formWidget.$touched = true;
        $scope.Widgets.VehicleForm3.VehicleForm3.RequestBody_make_formWidget.$touched = true;
        $scope.Widgets.VehicleForm3.VehicleForm3.RequestBody_model_formWidget.$touched = true;
        $scope.Widgets.VehicleForm3.VehicleForm3.RequestBody_subModel_formWidget.$touched = true;
        $scope.Widgets.VehicleForm3.VehicleForm3.RequestBody_antiTheft_formWidget.$touched = true;

        $scope.Widgets.CreateImVehicleUseForm3.CreateImVehicleUseForm3.RequestBody_costNew_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm3.CreateImVehicleUseForm3.RequestBody_useage_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm3.CreateImVehicleUseForm3.RequestBody_oneWayMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm3.CreateImVehicleUseForm3.RequestBody_annualMiles_formWidget.$touched = true;
        $scope.Widgets.CreateImVehicleUseForm3.CreateImVehicleUseForm3.RequestBody_ownership_formWidget.$touched = true;

        $scope.Widgets.CreateVehicleAddressForm3.CreateVehicleAddressForm3.RequestBody_address2_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm3.CreateVehicleAddressForm3.RequestBody_city_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm3.CreateVehicleAddressForm3.RequestBody_stateCode_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm3.CreateVehicleAddressForm3.RequestBody_county_formWidget.$touched = true;
        $scope.Widgets.CreateVehicleAddressForm3.CreateVehicleAddressForm3.RequestBody_zipCode5_formWidget.$touched = true;
    };

    $scope.TriggerCoveragesValidations = function($event, $isolateScope) {

        $scope.Widgets.CreateImCoverageForm1.CreateImCoverageForm1.RequestBody_bi_formWidget.$touched = true;
        $scope.Widgets.CreateImCoverageForm1.CreateImCoverageForm1.RequestBody_pd_formWidget.$touched = true;
        $scope.Widgets.CreateImCoverageForm1.CreateImCoverageForm1.RequestBody_mp_formWidget.$touched = true;
        $scope.Widgets.CreateImCoverageForm1.CreateImCoverageForm1.RequestBody_um_formWidget.$touched = true;
        $scope.Widgets.CreateImCoverageForm1.CreateImCoverageForm1.RequestBody_uim_formWidget.$touched = true;

        var i = 0;
        while (i < 50) {
            i++;
            if (!$scope.Widgets['StateCoverageForm' + i]) {
                break;
            }
            $scope.Widgets['StateCoverageForm' + i]['StateCoverageForm' + i].RequestBody_coverageName_formWidget.$touched = true;
            $scope.Widgets['StateCoverageForm' + i]['StateCoverageForm' + i].RequestBody_coverageValue_formWidget.$touched = true;
            $scope.Widgets['StateCoverageForm' + i]['StateCoverageForm' + i].coverageValue.$touched = true;
        }

        var j = 0;
        while (j < 4) {;
            if (!$scope.Widgets['CreateImVehicleCoverageForm' + j]) {
                break;
            }

            if ($scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].Driver_assignment_formWidget) {
                $scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].Driver_assignment_formWidget.$touched = true;
            }
            if ($scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_comprehensive_formWidget) {
                $scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_comprehensive_formWidget.$touched = true;
            }
            if ($scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_collDeduct_formWidget) {
                $scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_collDeduct_formWidget.$touched = true;
            }
            if ($scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_rentalDeduct_formWidget) {
                $scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_rentalDeduct_formWidget.$touched = true;
            }
            if ($scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_towingDeduct_formWidget) {
                $scope.Widgets['CreateImVehicleCoverageForm' + j]['CreateImVehicleCoverageForm' + j].RequestBody_towingDeduct_formWidget.$touched = true;
            }
            j++
        }

    };

    $scope.UpdateImIncidentInvokeonSuccess = function(variable, data, options) {;
        var driverIdList = "";
        if ($scope.Variables.driversIds.dataSet.length > 0) {
            for (var id in $scope.Variables.driversIds.dataSet) {
                driverIdList = driverIdList + $scope.Variables.driversIds.dataSet[id] + ",";
            }
        }

        driverIdList = driverIdList.substring(0, driverIdList.length - 1);
        var driverId = driverIdList;
        driverIdList = '(' + driverIdList + ')';

        var query = "incidentType='Accident'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke.invoke();

        //$scope.Variables.GetAccidentList.setInput("driverIds", driverId);
        //$scope.Variables.GetAccidentList.invoke();
        $scope.Variables.GetAccidentListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetAccidentListByPolicyId.invoke();

        query = "incidentType='Violation'  and driverId in " + driverIdList;
        $scope.Variables.GetIncidentTypeListInvoke1.setInput("q", query);
        $scope.Variables.GetIncidentTypeListInvoke1.invoke();

        //query = "incidentType='Comp Loss'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke2.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke2.invoke();
        $scope.Variables.GetCompLossListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetCompLossListByPolicyId.invoke();

        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = "";

        $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setPristine();
        $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setUntouched();

        $scope.Widgets.RequestBody_incidentType.datavalue = 'Accident';

    };


    $scope.getVehicleMakeModelById = function(data, ID) {

        var makeModel = 'NoLongerOwned';

        var vehicleArray = data.content;
        if (vehicleArray.length > 0) {
            for (var id1 in vehicleArray) {
                if (vehicleArray[id1].vehicleId == ID) {
                    makeModel = vehicleArray[id1].make + ' ' + vehicleArray[id1].model;
                    return makeModel;
                }
            }
        } else {
            makeModel = 'NoLongerOwned';
            return makeModel;
        }
        return makeModel;

    };

    $scope.updateEligibilityTab = function() {
        if ($scope.Variables.eligibilityId.dataSet.dataValue != 0) {
            if ($scope.Variables.UpdateImEligibility.dataBinding.RequestBody.questionAnswer9) {
                $scope.Variables.UpdateImEligibility.dataBinding.RequestBody.questionAnswer9 = $scope.Variables.UpdateImEligibility.dataBinding.RequestBody.questionAnswer9.toString()
            }

            if ($scope.Variables.UpdateImEligibility.dataBinding.RequestBody.questionAnswer8) {
                $scope.Variables.UpdateImEligibility.dataBinding.RequestBody.questionAnswer8 = $scope.Variables.UpdateImEligibility.dataBinding.RequestBody.questionAnswer8.toString()
            }
            $scope.Variables.UpdateImEligibility.dataBinding.RequestBody.policyId = $scope.policyId
            $scope.Variables.UpdateImEligibility.invoke()

        } else {
            if ($scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer9) {
                $scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer9 = $scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer9.toString()
            }
            $scope.Variables.CreateImEligibilityInvoke.invoke()

        }
        $scope.checkInvokePage();
    };

    $scope.button20Click = function($event, $isolateScope) {
        debugger;
        $scope.eligibilityButtonClick = true
        var forms = ["CreateImEligibilityForm1"];
        var isPageDirty = $scope.checkIfPageDirty(forms);
        if ($scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer8) {

            $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer8.datavalue = $scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer8.toString()

            $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer8 = $scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer8.toString()
        }

        $timeout(function() {

            if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
                $scope.isEligibilityTab = true
                $scope.Widgets.newQuoteConfDialog.open();
            } else {
                $scope.updateEligibilityTab();
            }

        }, 500);

        var eligibilityTabIncomplete = false;
        var eligibilityTabRequestBody_questionAnswer1 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer1.datavalue;


        var eligibilityTabLastRequestBody_questionAnswer2 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer2.datavalue;


        var eligibilityTabRequestBody_questionAnswer3 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer3.datavalue;


        var eligibilityTabRequestBody_questionAnswer4 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer4.datavalue;


        var eligibilityTabRequestBody_questionAnswer5 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer5.datavalue;


        var eligibilityTabRequestBody_questionAnswer6 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer6.datavalue;


        var eligibilityTabRequestBody_questionAnswer11 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer11.datavalue;


        var eligibilityTabRequestBody_questionAnswer7 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer7.datavalue;


        if (eligibilityTabRequestBody_questionAnswer1 == "''" || eligibilityTabLastRequestBody_questionAnswer2 == "''" || eligibilityTabRequestBody_questionAnswer3 == "''" || eligibilityTabRequestBody_questionAnswer4 == "''" || eligibilityTabRequestBody_questionAnswer5 == "''" || !eligibilityTabRequestBody_questionAnswer6 || !eligibilityTabRequestBody_questionAnswer11 || eligibilityTabRequestBody_questionAnswer7 == "''" || $scope.Widgets.CreateImEligibilityForm1.CreateImEligibilityForm1.$invalid) {

            $scope.Widgets.tabpane2._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Eligibility</span><!-- ngIf: badgevalue --></div></a>';
            eligibilityTabIncomplete = true;
            $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue = true;
        } else {
            $scope.Widgets.tabpane2._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Eligibility</span><!-- ngIf: badgevalue --></div></a>';

            eligibilityTabIncomplete = false;
            $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue = false;
        }

        $scope.Widgets.tabs1.next();
        $window.scrollTo(0, 0);
    };

    $scope.UpdateImPolicyInfoInvokeonSuccess = function(variable, data, options) {
        // if ($scope.Variables.getPolicyInfo.dataSet.content[0].creditCheckAuth != "''") {
        //     $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_creditCheckAuth.datavalue = $scope.Variables.getPolicyInfo.dataSet.content[0].creditCheckAuth;
        // }
        debugger
        $scope.Variables.GetDefensiveDriverMinDate.setInput("policy_id", data.policyId);
        $scope.Variables.GetDefensiveDriverMinDate.invoke();
        var query = "policyId= " + data.policyId;
        $scope.Variables.getPolicyInfo.setInput("q", query);
        $scope.Variables.getPolicyInfo.invoke();
    };

    $scope.button19Click = function($event, $isolateScope) {
        $scope.eligibilityButtonClick = true

        var eligibilityTabIncomplete = false;
        var eligibilityTabRequestBody_questionAnswer1 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer1.datavalue;
        var eligibilityTabLastRequestBody_questionAnswer2 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer2.datavalue;
        var eligibilityTabRequestBody_questionAnswer3 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer3.datavalue;
        var eligibilityTabRequestBody_questionAnswer4 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer4.datavalue;
        var eligibilityTabRequestBody_questionAnswer5 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer5.datavalue;
        var eligibilityTabRequestBody_questionAnswer6 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer6.datavalue;
        var eligibilityTabRequestBody_questionAnswer11 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer11.datavalue;
        var eligibilityTabRequestBody_questionAnswer7 = $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer7.datavalue;

        if (!eligibilityTabRequestBody_questionAnswer1 || !eligibilityTabLastRequestBody_questionAnswer2 || !eligibilityTabRequestBody_questionAnswer3 || !eligibilityTabRequestBody_questionAnswer4 || !eligibilityTabRequestBody_questionAnswer5 || !eligibilityTabRequestBody_questionAnswer6 || !eligibilityTabRequestBody_questionAnswer11 || !eligibilityTabRequestBody_questionAnswer7 || $scope.Widgets.CreateImEligibilityForm1.CreateImEligibilityForm1.$invalid) {
            $scope.Widgets.tabpane2._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Eligibility</span><!-- ngIf: badgevalue --></div></a>';

            eligibilityTabIncomplete = true;
            $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue = true;
        } else {
            $scope.Widgets.tabpane2._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Eligibility</span><!-- ngIf: badgevalue --></div></a>';

            eligibilityTabIncomplete = false;
            $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue = false;
        }

        $scope.Widgets.tabs1.previous();
        $window.scrollTo(0, 0);

    };


    $scope.button5Click = function($event, $isolateScope) {
        debugger;
        $scope.policyButtonClick = true
        $scope.prevPolicyClick = true
        $scope.button6Click();
        // var policyTabIncomplete = false;
        // var policyTabRequestBody_priorCarrierType = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorCarrierType.datavalue;
        // var policyTabRequestBody_expiration = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue;
        // var policyTabRequestBody_yearsWithPriorCarrier = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithPriorCarrier.datavalue;
        // var policyTabRequestBody_priorLiabilityLimit = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorLiabilityLimit.datavalue;
        // var policyTabRequestBody_yearsWithContinuousCoverage = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue;

        // var policyTabRequestBody_creditCheckAuth = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_creditCheckAuth.datavalue;
        // var policyTabRequestBody_effective = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue;

        //if (policyTabRequestBody_creditCheckAuth == "''") {
        //  $scope.Widgets.RequestBody_creditCheckAuth_formWidget.class = 'error-field'
        //} else {
        //  $scope.Widgets.RequestBody_creditCheckAuth_formWidget.class = ''

        //}


        // if (!policyTabRequestBody_priorCarrierType || !policyTabRequestBody_expiration || !policyTabRequestBody_yearsWithPriorCarrier || !policyTabRequestBody_priorLiabilityLimit || !policyTabRequestBody_yearsWithContinuousCoverage || !policyTabRequestBody_creditCheckAuth || !policyTabRequestBody_effective) {

        //     $scope.Widgets.policy._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Policy</span><!-- ngIf: badgevalue --></div></a>';
        //     policyTabIncomplete = true;
        //     $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue = true;

        // } else {
        //     $scope.Widgets.policy._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Policy</span><!-- ngIf: badgevalue --></div></a>';
        //     policyTabIncomplete = false;
        //     $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue = false;
        // }

        //$scope.Widgets.tabs1.previous();
        $scope.prevPolicyClick = false
        $scope.Widgets.tabs1.goToTab(2);
        $window.scrollTo(0, 0);
    };

    $scope.updatePolicyTab = function() {


        if ($scope.Variables.priorPolicyId.dataSet.dataValue != 0) {
            debugger
            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.reasonForLapse = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.reasonNoLapse.datavalue;
            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.policyId = $scope.policyId

            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.expiration = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue;
            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.monthsWithContinuousCoverage = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithContinuousCoverage.datavalue;
            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.yearsWithContinuousCoverage = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue;
            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.yearsWithPriorCarrier = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithPriorCarrier.datavalue;
            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.monthsWithPriorCarrier = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithPriorCarrier.datavalue;
            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.priorCarrierType = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorCarrierType.datavalue;

            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.priorLiabilityLimit = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorLiabilityLimit.datavalue;

            // $scope.Variables.UpdateImPriorPolicyInfo.dataBinding.RequestBody.priorPolicyPremium = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorPolicyPremium.datavalue;
            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.policyId = $scope.policyId
            $scope.Variables.UpdateImPriorPolicyInfo.setInput($scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput);
            $scope.Variables.UpdateImPriorPolicyInfo.setInput("ID", $scope.Variables.priorPolicyId.dataSet.dataValue);
            $scope.Variables.UpdateImPriorPolicyInfo.invoke();
        } else {
            $scope.Variables.CreateImPriorPolicyInfoInvoke.dataBinding.RequestBody.reasonForLapse = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.reasonNoLapse.datavalue;
            $scope.Variables.CreateImPriorPolicyInfoInvoke.invoke()
        }
        $scope.checkInvokePage();
    };
    $scope.button6Click = function($event, $isolateScope) {
        $scope.policyButtonClick = true


        var forms = ["CreateImPriorPolicyInfoForm1", "UpdateImPolicyInfoForm1"];
        var isPageDirty = $scope.checkIfPageDirty(forms);
        var policyTabIncomplete = false;

        if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
            $scope.isPolicyTab = true;
            $scope.Widgets.newQuoteConfDialog.open();
        } else {
            $scope.updatePolicyTab();
        }

        var policyTabRequestBody_priorCarrierType = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorCarrierType.datavalue;
        var policyTabRequestBody_expiration = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue;
        var policyTabRequestBody_yearsWithPriorCarrier = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithPriorCarrier.datavalue != null ? $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithPriorCarrier.datavalue.toString() : null;


        var policyTabRequestBody_priorLiabilityLimit = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorLiabilityLimit.datavalue;

        var policyTabRequestBody_yearsWithContinuousCoverage = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue != null ? $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue.toString() : null;

        if (policyTabRequestBody_priorCarrierType == "No Prior Insurance") {
            policyTabRequestBody_priorLiabilityLimit = true;
            policyTabRequestBody_yearsWithPriorCarrier = true;
            policyTabRequestBody_expiration = true;
            policyTabRequestBody_yearsWithContinuousCoverage = true;
            //not required fields if no prior ins seleced
        }


        var policyTabRequestBody_creditCheckAuth = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_creditCheckAuth.datavalue;
        var multipolicy_discount = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.multipolicy_discount.datavalue;

        var policyTabRequestBody_effective = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue;
        if (policyTabRequestBody_creditCheckAuth == "''") {
            $scope.Widgets.RequestBody_creditCheckAuth_formWidget.class = 'error-field'
        } else {
            $scope.Widgets.RequestBody_creditCheckAuth_formWidget.class = ''

        }
        debugger
        if ((policyTabRequestBody_priorCarrierType != "No Prior Insurance") && (policyTabRequestBody_priorLiabilityLimit == undefined)) {
            $scope.Widgets.RequestBody_priorLiabilityLimit_formWidget.class = 'error-field'
        } else {
            $scope.Widgets.RequestBody_priorLiabilityLimit_formWidget.class = ''

        }

        if (!policyTabRequestBody_priorCarrierType || !policyTabRequestBody_expiration || !policyTabRequestBody_yearsWithPriorCarrier || !policyTabRequestBody_priorLiabilityLimit || !policyTabRequestBody_yearsWithContinuousCoverage || !policyTabRequestBody_creditCheckAuth || !policyTabRequestBody_effective || policyTabRequestBody_creditCheckAuth == "''" || multipolicy_discount == "''") {

            $scope.Widgets.policy._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Policy</span><!-- ngIf: badgevalue --></div></a>';
            policyTabIncomplete = true;
            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue = true;

        } else {
            $scope.Widgets.policy._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Policy</span><!-- ngIf: badgevalue --></div></a>';
            policyTabIncomplete = false;
            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue = false;
        }

        if (!$scope.prevPolicyClick) {
            $scope.Widgets.tabs1.next();
            $window.scrollTo(0, 0);
        }

    };


    $scope.button9Click = function($event, $isolateScope) {
        $scope.Widgets.tabs1.next();
        $window.scrollTo(0, 0);
    };


    $scope.button8Click = function($event, $isolateScope) {
        $scope.Widgets.tabs1.previous();
        $window.scrollTo(0, 0);
    };

    $scope.button11Click = function($event, $isolateScope) {
        $scope.vehicleButtonClick = true
        var vehicleTabIncomplete = false;

        var forms = ["VehicleForm0", "VehicleForm1", "VehicleForm2", "VehicleForm3"];
        var isPageDirty = $scope.checkIfPageDirty(forms);

        if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
            $scope.Widgets.newQuoteConfDialog.open();

        }
        var vehicleTabRequestBody_vin = $scope.Widgets.VehicleForm0.formWidgets.RequestBody_vin.datavalue;
        var vehicleTabRequestBody_year = $scope.Widgets.VehicleForm0.formWidgets.RequestBody_year.datavalue;
        var vehicleTabRequestBody_make = $scope.Widgets.VehicleForm0.formWidgets.RequestBody_make.datavalue;
        var vehicleTabRequestBody_model = $scope.Widgets.VehicleForm0.formWidgets.RequestBody_model.datavalue;
        var vehicleTabRequestBody_subModel = $scope.Widgets.VehicleForm0.formWidgets.RequestBody_subModel.datavalue;
        var vehicleTabRequestBody_antiTheft = $scope.Widgets.VehicleForm0.formWidgets.RequestBody_antiTheft.datavalue;

        var vehicleTabRequestBody_costNew = $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_costNew.datavalue;
        var vehicleTabRequestBody_RequestBody_useage = $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_useage.datavalue;
        var vehicleTabRequestBody_annualMiles = $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_annualMiles.datavalue;
        var vehicleTabRequestBody_ownership = $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_ownership.datavalue;
        var vehicleTabRequestBody_occupation = $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_newVehicle.datavalue;

        var i = 0;
        var numOfVehicles = $scope.Widgets.numVehicles.datavalue;
        numOfVehicles = (numOfVehicles == 0) ? 1 : numOfVehicles;
        if (!vehicleTabIncomplete) {
            for (i = 0; i < numOfVehicles; i++) {
                var dynamicFormName = 'CreateImVehicleUseForm' + i;
                if ($scope.Widgets[dynamicFormName][dynamicFormName].$invalid) {
                    $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Vehicles</span><!-- ngIf: badgevalue --></div></a>';
                    vehicleTabIncomplete = true;
                    $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue = true;
                    break;
                }
            }
        }

        if (!vehicleTabIncomplete) {
            $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Vehicles</span><!-- ngIf: badgevalue --></div></a>';

            $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue = false;
        }

        $scope.Widgets.tabs1.previous();
        $window.scrollTo(0, 0);
    };

    $scope.button14Click = function($event, $isolateScope) {;
        $scope.incidentsButtonClick = true
        //Check if the Incidents Form is not empty if it is not then show the popup.
        console.log('Incident Next button Click');
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue);

        var incidentDate = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue;
        var incidentDriver = $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue;
        var accidentDesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue;
        var incidentAmount = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue;
        var incidentVehicle = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue;
        var violationDesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue;
        var lossdesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue;
        var incidentsFormValid = $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$valid;

        var incidentTabFormFilledComplete = false;
        var incidentTabFormInComplete = false;
        var incidentType = $scope.Widgets.RequestBody_incidentType.datavalue;

        if ((incidentType == 'Accident' && !((!incidentDate || incidentDate == "") && (!incidentDriver || incidentDriver == "") && (!accidentDesc || accidentDesc == "") && (!incidentAmount || incidentAmount == "") &&
                (incidentVehicle == undefined || incidentVehicle == ""))) || (incidentType == 'Violation' && !((!incidentDate || incidentDate == "") && (!incidentDriver || incidentDriver == "") && (!violationDesc || violationDesc == ""))) || (incidentType == 'Comp Loss' && !((!incidentDate || incidentDate == "") && (!incidentDriver || incidentDriver == "") && (!lossdesc || lossdesc == "" && (!incidentAmount || incidentAmount == "") &&
                (incidentVehicle == undefined || incidentVehicle == ""))))) {


            incidentTabFormInComplete = true;
        } else if (((incidentDate && incidentDate != '') && incidentDriver && incidentAmount && (incidentVehicle || incidentVehicle >= 0) && accidentDesc) || ((incidentDate && incidentDate != '') && incidentDriver && violationDesc) || ((incidentDate && incidentDate != '') && incidentDriver && incidentAmount && (incidentVehicle || incidentVehicle >= 0) && lossdesc)) {
            incidentTabFormFilledComplete = true;
        }

        if (incidentTabFormFilledComplete) {
            $scope.Widgets.confirmIncidentsSave2.open();
        } else if (incidentTabFormInComplete) {
            $scope.Widgets.confirmIncidentsSave.open();
        } else {
            $scope.Widgets.tabs1.previous();
            $window.scrollTo(0, 0);
        }
    };


    $scope.button12Click = function($event, $isolateScope) {
        $scope.Widgets.tabs1.next();
        $window.scrollTo(0, 0);
    };


    $scope.gotToPrev = function($event, $isolateScope) {
        $scope.Widgets.tabs1.previous();
        $window.scrollTo(0, 0);
    };


    $scope.gotToNext = function($event, $isolateScope) {
        $scope.Widgets.tabs1.next();
        $window.scrollTo(0, 0);
    };

    $scope.updateVehiclesTab = function() {

        for (var k in $scope.Widgets) {
            if (k.indexOf('VehicleForm') > -1 && $scope.Widgets[k].$element.hasClass('ng-dirty')) {
                var index = k.charAt(k.length - 1);
                var j = "CreateImVehicleUseForm" + index;
                if ($scope.Widgets[j].formWidgets.altGarage.datavalue == "Yes") {
                    $scope.vehFormForAddress = k;
                    for (var k in $scope.Widgets) {
                        if (k.indexOf('CreateVehicleAddressForm') !== -1 && k.charAt(k.length - 1) == index) {
                            if (k.indexOf('CreateVehicleAddressForm') > -1) {
                                var index = k.charAt(k.length - 1);
                                if ($scope.Variables.vehicleAddressIds.dataSet[index] == undefined) {

                                    if ($scope.Widgets[k].$element.hasClass('ng-dirty')) {
                                        //$scope.Widgets[k].submit();
                                        $scope.Variables.CreateVehicleAddress.setInput($scope.Widgets[k].dataoutput);

                                        $scope.Variables.CreateVehicleAddress.invoke();
                                        $scope.Variables.numVehiclesList.dataSet.dataValue = index;
                                        $scope.setVehiclesButton(index);
                                        $scope.Widgets.numVehicles.datavalue = index;


                                    } else {
                                        $scope.Widgets[k].show = false;

                                    }

                                } else {
                                    var index = k.charAt(k.length - 1);
                                    var vehicleUseId = $scope.Variables.vehicleAddressIds.dataSet[index];
                                    $scope.Variables.UpdateVehicleAddress.setInput($scope.Widgets[k].dataoutput);
                                    $scope.Variables.UpdateVehicleAddress.setInput("ID", vehicleUseId);
                                    $scope.Variables.UpdateVehicleAddress.invoke();
                                    $scope.saveVehicle(vehicleUseId, k);
                                }
                            }
                        }
                    }
                } else {
                    $scope.saveVehicle(null, k);
                }
            }
        }
        $scope.checkInvokePage();
    };
    $scope.button21Click = function($event, $isolateScope) {
        $scope.vehicleButtonClick = true
        var forms = ["VehicleForm0", "VehicleForm1", "VehicleForm2", "VehicleForm3", "CreateImVehicleUseForm0", "CreateImVehicleUseForm1", "CreateImVehicleUseForm2", "CreateImVehicleUseForm3"]
        var isPageDirty = $scope.checkIfPageDirty(forms);

        var vehicleTabIncomplete = false;

        if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
            $scope.isVehiclesTab = true
            $scope.Widgets.newQuoteConfDialog.open();
        } else {
            $scope.updateVehiclesTab();
        }

        var i = 0;
        var numOfVehicles = $scope.Widgets.numVehicles.datavalue;
        numOfVehicles = (numOfVehicles == 0) ? 1 : numOfVehicles;
        for (i = 0; i < numOfVehicles; i++) {
            var dynamicFormName = 'VehicleForm' + i;
            if ($scope.Widgets[dynamicFormName][dynamicFormName].$invalid) {
                $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Vehicles</span><!-- ngIf: badgevalue --></div></a>';
                vehicleTabIncomplete = true;
                $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue = true;
                break;
            }
        }

        var i = 0;
        var numOfVehicles = $scope.Widgets.numVehicles.datavalue;
        numOfVehicles = (numOfVehicles == 0) ? 1 : numOfVehicles;
        if (!vehicleTabIncomplete) {
            for (i = 0; i < numOfVehicles; i++) {
                var dynamicFormName = 'CreateImVehicleUseForm' + i;
                if ($scope.Widgets[dynamicFormName][dynamicFormName].$invalid || $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_newVehicle.datavalue == "''") {
                    $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Vehicles</span><!-- ngIf: badgevalue --></div></a>';

                    vehicleTabIncomplete = true;
                    $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue = true;
                    break;
                }
            }
        }

        if (!vehicleTabIncomplete) {
            $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Vehicles</span><!-- ngIf: badgevalue --></div></a>';

            $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue = false;
        }

        if ($event != null) {
            //button click called through script for tab save functionality
            $scope.Widgets.confirmIncidents.open();
        }





        $window.scrollTo(0, 0);
    };

    $scope.saveVehicle = function(addressId, k) {
        $timeout(function() {

            if (k.indexOf('VehicleForm') > -1 && ($scope.Widgets[k].show || $scope.Widgets[k].$element.hasClass('ng-dirty'))) {
                var index = k.charAt(k.length - 1);
                $scope.Widgets[k].formWidgets.RequestBody_garageAddressId.datavalue = addressId;

                if ($scope.Variables.vehicleIds.dataSet[index] == undefined) {
                    if ($scope.Widgets[k].$element.hasClass('ng-dirty')) {
                        //$scope.Widgets[k].submit();
                        $scope.Variables.CreateImVehicleInvoke.setInput($scope.Widgets[k].dataoutput);
                        $scope.Variables.CreateImVehicleInvoke.dataBinding.RequestBody.garageAddressId = addressId
                        $scope.Variables.CreateImVehicleInvoke.invoke()
                    } else {
                        $scope.Widgets[k].show = false;
                        $scope.Variables.numVehiclesList.dataSet.dataValue = index;
                        $scope.setVehiclesButton(index);
                        $scope.Widgets.numVehicles.datavalue = index;
                    }

                } else {

                    var index = k.charAt(k.length - 1);
                    var vehicleId = $scope.Variables.vehicleIds.dataSet[index];
                    $scope.Widgets[k].dataoutput.RequestBody.garageAddressId =
                        addressId == null ? null : addressId;
                    $scope.Widgets[k].formWidgets.RequestBody_garageAddressId.datavalue = addressId;
                    // $scope.Widgets[k].dataoutput.RequestBody.garageAddressId =
                    //     addressId == null ? null : addressId;
                    $scope.Variables.updateImVehicle.setInput($scope.Widgets[k].dataoutput);
                    $scope.Variables.updateImVehicle.dataBinding.RequestBody.garageAddressId = addressId
                    $scope.Variables.updateImVehicle.setInput("ID", vehicleId);
                    $scope.Variables.updateImVehicle.setInput("formName", k);
                    $scope.Variables.updateImVehicle.invoke({}, function(data) {

                        //var formName = variable.dataBinding.formName
                        //var count = formName.charAt(formName.length - 1);
                        var vehicleUseId = $scope.Variables.vehicleUseIds.dataSet[index];
                        $scope.Variables.updateImVehicleUse.setInput($scope.Widgets["CreateImVehicleUseForm" + index].dataoutput);
                        $scope.Variables.updateImVehicleUse.setInput("ID", vehicleUseId);
                        $scope.Variables.updateImVehicleUse.invoke();

                        // Success Callback
                        console.log("success", data);
                    }, function(error) {
                        // Error Callback
                        console.log("error", error)
                    });
                }

            } else {
                if (k.indexOf('VehicleForm') > -1) {
                    console.log("form name is " + k);
                    console.log("form show is " + $scope.Widgets[k].show);
                    console.log("form dirty is " + $scope.Widgets[k].$element.hasClass('ng-dirty'));
                }

            }
        }, 2000);


    };
    $scope.updateDriverTab = function() {;
        debugger;
        for (var k in $scope.Widgets) {
            if (k.indexOf('DriverForm') > -1) {
                var index = k.charAt(k.length - 1);
                //if ($scope.Widgets[k].show) {
                if ($scope.Variables.driversIds.dataSet[index] == undefined) {
                    if ($scope.Widgets[k].$element.hasClass('ng-dirty')) {
                        // $scope.Widgets[k].submit();

                        $scope.Variables.CreateImDriverInvoke.setInput($scope.Widgets[k].dataoutput);
                        $scope.Variables.CreateImDriverInvoke.invoke();
                        $scope.Variables.numDriversList.dataSet.dataValue = index;
                        $scope.setDriversButton(index);
                        $scope.Widgets.numDrivers.datavalue = index;
                    } else {
                        //$scope.Widgets[k].submit();
                        $scope.Widgets[k].show = false;
                        //$scope.Variables.numDriversList.dataSet.dataValue = index;
                        // $scope.setDriversButton(index);
                        // $scope.Widgets.numDrivers.datavalue = index;
                    }

                } else {
                    var index = k.charAt(k.length - 1);
                    var driverId = $scope.Variables.driversIds.dataSet[index];
                    $scope.Variables.updateImDriver.setInput($scope.Widgets[k].dataoutput);
                    $scope.Variables.updateImDriver.setInput("ID", driverId);
                    $scope.Variables.updateImDriver.invoke();
                }
                // }
            }
        }
        $scope.checkInvokePage();
    };

    $scope.button31Click = function($event, $isolateScope) {;
        $scope.driverButtonClick = true

        var forms = ["DriverForm0", "DriverForm1", "DriverForm2", "DriverForm3", "DriverForm4", "DriverForm5"]
        var isPageDirty = $scope.checkIfPageDirty(forms);

        if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
            $scope.isDriverTab = true
            $scope.Widgets.newQuoteConfDialog.open();
        } else {
            $scope.updateDriverTab();
        }

        var driverTabIncomplete = false;
        var i = 0;
        var numOfDrivers = $scope.Widgets.numDrivers.datavalue;
        numOfDrivers = (numOfDrivers == 0) ? 1 : numOfDrivers;
        for (i = 0; i < numOfDrivers; i++) {

            var dynamicFormName = 'DriverForm' + i;
            if ($scope.Widgets[dynamicFormName][dynamicFormName].$invalid) {
                $scope.Widgets.driver._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Driver</span><!-- ngIf: badgevalue --></div></a>';

                $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue = true;
                driverTabIncomplete = true;
                break;
            }
            var cq = $scope.Variables.GetCarrierQuestions
            cq.setInput("policyId", $scope.policyId)
            cq.invoke();

        }

        if (!driverTabIncomplete) {
            $scope.Widgets.driver._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Driver</span><!-- ngIf: badgevalue --></div></a>';
            $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue = false;
        }

        $scope.Widgets.tabs1.next();
        $window.scrollTo(0, 0);
    };

    $scope.checkIterationProcess = function() {
        $scope.newIterationQuote = true
        var sv = $scope.Variables.CopyQuoteProc
        sv.setInput("policy_id", $scope.Variables.selectedPolicyId.dataSet.dataValue)
        sv.setInput("new_policy_id", 1)
        sv.invoke();
    };

    $scope.checkIfPageDirty = function(formNames) {


        var currentForms = [];
        var isDirty = false;
        for (var index in formNames) {
            var formName = formNames[index];
            for (var k in $scope.Widgets) {

                if (k.indexOf(formName) != -1) {

                    currentForms.push($scope.Widgets[k]);
                }
            }
        }

        for (var j in currentForms) {
            var currentForm = currentForms[j];
            if (currentForm.$element.hasClass('ng-dirty')) {
                return true;
            }

        }
        return isDirty;
    };

    $scope.updateClientTab = function() {

        console.log($scope)

        ;
        if ($scope.Variables.applicantId.dataSet.dataValue != 0) {
            $scope.Variables.UpdateImApplicant.dataBinding.RequestBody.insertTimestamp = new Date().toISOString().slice(0, 19);
            $scope.Variables.UpdateImApplicant.invoke();
        } else {
            $scope.Variables.CreateImApplicantInvoke.dataBinding.RequestBody.insertTimestamp = new Date().toISOString().slice(0, 19);
            $scope.Variables.CreateImApplicantInvoke.invoke();

        }
        $timeout(function() {
            $scope.CheckAddressAndCoApp();
            $scope.checkInvokePage();
        }, 500);


    };

    $scope.checkInvokePage = function() {
        $timeout(function() {
            if ($scope.requote) {
                $scope.Variables.requestForQuotesTab.dataSet.dataValue = true;
                $scope.Variables.RequestFromClientPage.dataSet.dataValue = false;
                $scope.Variables.goToPage_Main.invoke()
                $scope.Variables.goToPage_AutoQuote.invoke()
            } else if ($scope.newIterationQuote) {
                $scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
                $scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
                $scope.Variables.completedNewIteration.dataSet.dataValue = false;
                $scope.Variables.goToPage_Main.invoke()
                $scope.Variables.goToPage_AutoQuote.invoke()
            }

        }, 1000)
    };

    $scope.button3Click = function($event, $isolateScope) {
        $scope.clientButtonClick = true
        $scope.ShowAutoTabs()
        var clientTabIncomplete = false;
        var forms = ["CreateImApplicantForm1", "CreateImAddressForm1", "CoApplicant"];
        var isPageDirty = $scope.checkIfPageDirty(forms);

        if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
            $scope.isClientTab = true
            $scope.Widgets.newQuoteConfDialog.open();
            return
        } else {
            $scope.updateClientTab();
        }
        $scope.Widgets.tabs1.next();

        var clientTabFirstName = $scope.Widgets.CreateImApplicantForm1.formWidgets.firstName.datavalue;
        var clientTabLastName = $scope.Widgets.CreateImApplicantForm1.formWidgets.lastName.datavalue;
        var clientTabGender = $scope.Widgets.CreateImApplicantForm1.formWidgets.gender.datavalue;
        $scope.clientTabGender = $scope.Widgets.CreateImApplicantForm1.formWidgets.gender.datavalue;
        var clientTabDOB = $scope.Widgets.CreateImApplicantForm1.formWidgets.dob.datavalue;
        var clientTabEducation = $scope.Widgets.CreateImApplicantForm1.formWidgets.RequestBody_education.datavalue;
        var clientTabIndustry = $scope.Widgets.CreateImApplicantForm1.formWidgets.RequestBody_industry.datavalue;
        var clientTabOccupation = $scope.Widgets.CreateImApplicantForm1.formWidgets.occupation.datavalue;
        var clientTabMaritalStatus = $scope.Widgets.CreateImApplicantForm1.formWidgets.maritalStatus.datavalue;

        var clientTabAddress1 = $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_address1.datavalue;
        var clientTabCity = $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_city.datavalue;
        var clientTabCounty = $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_county.datavalue;
        var clientTabState = $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_stateCode.datavalue;
        var clientTabZipCode = $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_zipCode5.datavalue;

        var yearsAtCurrent = $scope.Widgets.createResidenceInfoForm1.formWidgets.years_at_current.datavalue != null ? $scope.Widgets.createResidenceInfoForm1.formWidgets.years_at_current.datavalue.toString() : null;
        var monthsAtCurrent = $scope.Widgets.createResidenceInfoForm1.formWidgets.months_at_current.datavalue != null ? $scope.Widgets.createResidenceInfoForm1.formWidgets.months_at_current.datavalue.toString() : null;


        var clientTabCoApplicantFirstName = $scope.Widgets.CoApplicant.formWidgets.RequestBody_firstName.datavalue;
        var clientTabCoApplicantLastName = $scope.Widgets.CoApplicant.formWidgets.form_field121.datavalue;
        var clientTabCoApplicantGender = $scope.Widgets.CoApplicant.formWidgets.RequestBody_gender.datavalue;

        var clientTabCoApplicantDOB = $scope.Widgets.CoApplicant.formWidgets.RequestBody_dob.datavalue;
        var clientTabCoApplicantMaritalStatus = $scope.Widgets.CoApplicant.formWidgets.RequestBody_maritalStatus.datavalue;
        var clientTabCoApplicantRelation = $scope.Widgets.CoApplicant.formWidgets.RequestBody_relation.datavalue;
        var clientTabCoApplicantIndustry = $scope.Widgets.CoApplicant.formWidgets.RequestBody_industry.datavalue;
        var clientTabCoApplicantOccupation = $scope.Widgets.CoApplicant.formWidgets.RequestBody_occupation.datavalue;

        var phoneNumber = $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_phoneNumber.datavalue;




        var coApplicantAdded = $scope.Widgets.CreateImApplicantForm1.formWidgets.coapplicant.datavalue;

        if (!clientTabFirstName || !clientTabLastName || (clientTabGender == "''") || !clientTabDOB || !clientTabEducation || !clientTabIndustry || !clientTabOccupation || !clientTabMaritalStatus || !clientTabAddress1 || !clientTabCity || !clientTabCounty || !clientTabState || !clientTabZipCode || !yearsAtCurrent || !monthsAtCurrent || !phoneNumber || (coApplicantAdded == 1 && (!clientTabCoApplicantFirstName || !clientTabCoApplicantLastName || (clientTabCoApplicantGender == "''") || !clientTabCoApplicantDOB && !clientTabCoApplicantMaritalStatus && !clientTabCoApplicantRelation && !clientTabCoApplicantIndustry && !clientTabCoApplicantOccupation))) {

            $scope.Widgets.tabpane1._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Client</span><!-- ngIf: badgevalue --></div></a>';
            clientTabIncomplete = true;
            $scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue = true;
        } else {
            clientTabIncomplete = false;
            $scope.Widgets.tabpane1._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Client</span><!-- ngIf: badgevalue --></div></a>';

            $scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue = false;
        }



        window.scrollTo(0, 0);
    };



    $scope.numVehiclesChange = function($event, $isolateScope, newVal, oldVal) {;
        $scope.numVehiclesValue = newVal;
        if (newVal < oldVal) {
            for (var i = newVal; i < oldVal; i++) {

                if ($scope.Variables.getPolicyVehicles.dataSet.content.length > i) {
                    var vehId = $scope.Variables.getPolicyVehicles.dataSet.content[i].vehicleId;
                    $scope.Variables.deleteVehicle.setInput("ID", vehId);
                    $scope.Variables.deleteVehicle.invoke();
                    var currentForm = $scope.Widgets["VehicleForm" + i];


                } else {
                    var currentForm = $scope.Widgets["VehicleForm" + i];
                    currentForm.show = false;
                    var currentForm1 = $scope.Widgets["CreateImVehicleUseForm" + i];
                    $scope.clearVehicleForm(currentForm);
                    $scope.clearVehicleUseForm(currentForm1);
                }
            }
        }
        $scope.setVehiclesButton(newVal);


    };



    $scope.clearVehicleForm = function(currentForm) {

        currentForm.formWidgets.RequestBody_vin.datavalue = '';
        currentForm.formWidgets.RequestBody_year.datavalue = "";
        currentForm.formWidgets.RequestBody_make.datavalue = "";
        currentForm.formWidgets.RequestBody_model.datavalue = "";
        currentForm.formWidgets.RequestBody_subModel.datavalue = "";
        currentForm.formWidgets.RequestBody_antiTheft.datavalue = "";
        // //currentForm.$setUntouched();
        // currentForm.$element.addClass('ng-untouched');
        currentForm.$element.removeClass('ng-dirty');
        // currentForm.$element.addClass('ng-pristine');

        currentForm[currentForm.name].RequestBody_vin_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_year_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_make_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_model_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_subModel_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_antiTheft_formWidget.$setUntouched();
    }

    $scope.clearVehicleUseForm = function(currentForm) {
        currentForm.formWidgets.RequestBody_costNew.datavalue = "";
        currentForm.formWidgets.RequestBody_useage.datavalue = "";
        currentForm.formWidgets.RequestBody_oneWayMiles.datavalue = "";

        currentForm[currentForm.name].RequestBody_costNew_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_useage_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_oneWayMiles_formWidget.$setUntouched();

        var index = currentForm.name.charAt(currentForm.name.length - 1)

        if (index == 1) {
            currentForm.formWidgets.RequestBody_annualMiles1.datavalue = "";
            currentForm[currentForm.name].RequestBody_annualMiles1_formWidget.$setUntouched();
        } else if (index == 2) {
            currentForm.formWidgets.RequestBody_annualMiles2.datavalue = "";
            currentForm[currentForm.name].RequestBody_annualMiles2_formWidget.$setUntouched();
        } else if (index == 3) {
            currentForm.formWidgets.RequestBody_annualMiles3.datavalue = "";
            currentForm[currentForm.name].RequestBody_annualMiles3_formWidget.$setUntouched();
        }



        currentForm.formWidgets.RequestBody_daysPerWk.datavalue = "5";
        currentForm.formWidgets.RequestBody_weeksPerMnth.datavalue = "4";
        currentForm.formWidgets.RequestBody_ownership.datavalue = "";
        currentForm.formWidgets.RequestBody_newVehicle.datavalue = "''";
        currentForm.formWidgets.RequestBody_newVehicle.defaultvalue = "''";
        currentForm.dataoutput.RequestBody.newVehicle = '';

        //currentForm.formWidgets.RequestBody_vehicleUse.datavalue = "";
        //currentForm.$setUntouched();
    }


    $scope.clearVehicleAddressForm = function(currentForm) {
        currentForm.formWidgets.RequestBody_address2.datavalue = "";
        currentForm.formWidgets.RequestBody_address1.datavalue = "";
        currentForm.formWidgets.RequestBody_city.datavalue = "";
        currentForm.formWidgets.RequestBody_stateCode.datavalue = "";
        currentForm.formWidgets.RequestBody_county.datavalue = "";
        currentForm.formWidgets.RequestBody_zipCode5.datavalue = "";

    }


    $scope.clearDriverForm = function(currentForm) {


        // currentForm.reset();
        //currentForm.$setPristine();


        //  currentForm.formData = "";

        currentForm.formWidgets.RequestBody_firstname.datavalue = ""
        currentForm.formWidgets.RequestBody_lastName.datavalue = ""
        currentForm.formWidgets.RequestBody_dob.datavalue = ""
        currentForm.formWidgets.RequestBody_relation.datavalue = ""
        currentForm.formWidgets.RequestBody_maritalStatus.datavalue = ""
        currentForm.formWidgets.RequestBody_driverLicenseStatus.datavalue = ""
        currentForm.formWidgets.RequestBody_ageLicensed.datavalue = ""
        currentForm.formWidgets.RequestBody_dateLicensed.datavalue = ""

        currentForm.formWidgets.RequestBody_gender.datavalue = "''";
        currentForm.formWidgets.RequestBody_goodDriver.datavalue = "''";
        currentForm.formWidgets.RequestBody_matDriver.datavalue = "''";
        currentForm.formWidgets.RequestBody_driverTraining.datavalue = "''";
        currentForm.formWidgets.RequestBody_student100.datavalue = "''";
        currentForm.formWidgets.RequestBody_goodStudent.datavalue = "''";


        currentForm.formWidgets.RequestBody_gender.defaultvalue = "''";
        currentForm.formWidgets.RequestBody_goodDriver.defaultvalue = "''";
        currentForm.formWidgets.RequestBody_matDriver.defaultvalue = "''";
        currentForm.formWidgets.RequestBody_driverTraining.defaultvalue = "''";
        currentForm.formWidgets.RequestBody_student100.defaultvalue = "''";
        currentForm.formWidgets.RequestBody_goodStudent.defaultvalue = "''";


        currentForm[currentForm.name].RequestBody_firstname_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_lastName_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_dob_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_relation_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_maritalStatus_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_driverLicenseStatus_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_ageLicensed_formWidget.$setUntouched();
        currentForm[currentForm.name].RequestBody_dateLicensed_formWidget.$setUntouched();

        currentForm.$element.removeClass('ng-dirty');



    }


    $scope.setVehiclesButton = function(newVal) {

        if ($scope.Widgets.VehicleForm0 == null) {
            return false;
        }

        ;

        for (var k in $scope.Widgets) {
            if (k.indexOf('VehicleForm') > -1 || (k.indexOf('CreateImVehicleUseForm') > -1)) {
                var index = k.charAt(k.length - 1);
                if (index < newVal) {
                    $scope.Widgets[k].show = true;
                } else {
                    $scope.Widgets[k].show = false;
                    //$scope.Widgets[k].resetForm()
                }
            }
        }
        if (1 <= newVal && newVal <= 2 && angular.element($('[name="vehButtonPrev"]'))[0] != null) {
            if (newVal == '1') {
                angular.element($('[name="vehButtonPrev"]'))[0].innerText = '1';
                $scope.Widgets.vehButtonPrev.show = false;
            } else {
                angular.element($('[name="vehButtonPrev"]'))[0].innerText = '1-2';
                $scope.Widgets.vehButtonPrev.show = true;
            }
        } else if (3 <= newVal && newVal <= 4 && angular.element($('[name="vehButtonPrev"]'))[0] != null) {
            $scope.Widgets.vehButtonPrev.show = true;
            angular.element($('[name="vehButtonPrev"]'))[0].innerText = '1-2';
            if (newVal == '3') {
                angular.element($('[name="vehButtonNext"]'))[0].innerText = '3';
            } else {
                angular.element($('[name="vehButtonNext"]'))[0].innerText = '3-4';
            }
        }
        if (newVal == 1 || newVal == 2) {
            $scope.Widgets.tabs5.goToTab(1, {});
            $scope.vehicleActivePage = 1;
        }
        if (newVal == 3 || newVal == 4) {
            $scope.Widgets.tabs5.goToTab(2, {});
            $scope.vehicleActivePage = 2;
        }
        if (newVal == 0) {
            newVal = 1;
        }
        $scope.Widgets.numVehicles.datavalue = newVal;
        $scope.numVehiclesValue = newVal;

        $scope.Widgets.VehicleForm0.show = true;
        $scope.Widgets.CreateImVehicleUseForm0.show = true;
    }

    $scope.setDriversButton = function(newVal) {
        debugger;
        for (var k in $scope.Widgets) {
            if (k.indexOf('DriverForm') > -1) {
                var index = k.charAt(k.length - 1);
                if (index < newVal) {
                    $scope.Widgets[k].show = true;
                } else {
                    $scope.Widgets[k].show = false;
                }
            }

        }
        if (1 <= newVal && newVal <= 2) {

            if (newVal == '1') {
                $scope.Widgets.driveButton1.$element[0].innerText = '1'
                // angular.element($('[name="driveButton1"]'))[0].innerText = '1';
                $scope.Widgets.driveButton1.show = false;


            } else {
                $scope.Widgets.driveButton1.$element[0].innerText = '1-2'
                //angular.element($('[name="driveButton1"]'))[0].innerText = '1-2';
                $scope.Widgets.driveButton1.show = true;
            }
        }
        if (3 <= newVal && newVal <= 4) {
            $scope.Widgets.driveButton1.show = true;

            angular.element($('[name="driveButton1"]'))[0].innerText = '1-2';
            if (newVal == '3') {
                angular.element($('[name="driveButton2"]'))[0].innerText = '3';
                angular.element($('[name="driveButton1"]'))[0].innerText = '1-2';
            } else {
                angular.element($('[name="driveButton2"]'))[0].innerText = '3-4';
            }
        }
        if (5 <= newVal && newVal <= 6) {
            $scope.Widgets.driveButton1.show = true;

            angular.element($('[name="driveButton1"]'))[0].innerText = '1-2';
            angular.element($('[name="driveButton2"]'))[0].innerText = '3-4';
            if (newVal == '5') {
                angular.element($('[name="driveButton3"]'))[0].innerText = '5';
            } else {
                angular.element($('[name="driveButton3"]'))[0].innerText = '5-6';
            }
        }

        if (newVal == 1 || newVal == 2) {
            $scope.Widgets.tabs4.goToTab(1, {});
            $scope.driverActivePage = 1;
        }
        if (newVal == 3 || newVal == 4) {
            $scope.Widgets.tabs4.goToTab(2, {});
            $scope.driverActivePage = 2;
        }
        if (newVal == 5 || newVal == 6) {
            $scope.Widgets.tabs4.goToTab(3, {});
            $scope.driverActivePage = 3;
        }
        if (newVal == 0) {
            newVal = 1;
        }
        $scope.Widgets.numDrivers.datavalue = newVal;

        $scope.numDriversValue = newVal;
        $scope.Widgets.DriverForm0.show = true;



    }

    $scope.numDriversChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.numDriversValue = newVal;
        $scope.setDriversButton(newVal);


    };


    $scope.GetCarrierQuestionsInvokeonSuccess = function(variable, data, options) {
        for (var i = 0; i <= data.length; i++) {
            var carrData = data[i]
            if (carrData) {

                if (carrData.answerDataType == "DROPDOWN") {
                    var gaq = $scope.Variables.GetAnswersByQuestion123;
                    gaq.setInput("qmi", carrData.questionMasterId)
                    gaq.invoke()
                }
            }
        }
        if ($scope.Variables.RequestFromClientPage.dataSet.dataValue) {
            $scope.policyId = $scope.Variables.selectedPolicyId.dataSet.dataValue;
            var query = "policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;
        }
    };

    $scope.updateCarrierTab = function() {;
        $scope.Variables.savePolicyQuestions.setInput("RequestBody", $scope.carrierTabData);
        $scope.Variables.savePolicyQuestions.invoke();
        $scope.checkInvokePage();
    };

    // $scope.button19_2Click = function($event, $isolateScope) {
    //      ;
    //     $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.ID = $scope.policyId;

    //     $scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime = new Date().toISOString().slice(0, 19);
    //     $scope.Variables.getPolicyInfo.dataSet.content[0].submittedStatus = 'Y';

    //     $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.RequestBody = $scope.Variables.getPolicyInfo.dataSet.content[0]
    //     $scope.Variables.UpdateImPolicyInfoInvoke.invoke();

    //     var tabIssues = false;

    //     var carrierTabIncomplete = false;

    //     if ($scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == true) {
    //         $scope.Widgets.tabpane1._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Client</span><!-- ngIf: badgevalue --></div></a>';

    //     }

    //     if ($scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == true) {
    //         $scope.Widgets.tabpane2._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Eligibility</span><!-- ngIf: badgevalue --></div></a>';
    //     }

    //     if ($scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == true) {
    //         $scope.Widgets.policy._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Policy</span><!-- ngIf: badgevalue --></div></a>';
    //     }

    //     if ($scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == "true" || $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == true) {
    //         $scope.Widgets.driver._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Driver</span><!-- ngIf: badgevalue --></div></a>';

    //     }

    //     if ($scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == "true" || $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == true) {
    //         $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Vehicles</span><!-- ngIf: badgevalue --></div></a>';

    //     }

    //     if ($scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == true) {
    //         $scope.Widgets.coverages._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Coverages</span><!-- ngIf: badgevalue --></div></a>';
    //     }

    //     // for (var k in $scope.Widgets.GetCarrierQuestionsList1.Widgets) {
    //     //     var formName = k.substring(28, k.length - 1);
    //     //     if (k.indexOf('CreateImPolicyQuestionsForm') > -1) {

    //     //         if (($scope.Widgets.GetCarrierQuestionsList1.Widgets[k].formWidgets.Answercontainer.pageParams.answerDataType == 'TEXT' && $scope.Widgets.GetCarrierQuestionsList1.Widgets[k].$element[0][3].required && $scope.Widgets.GetCarrierQuestionsList1.Widgets[k].$element[0][3].validity.valueMissing) || ($scope.Widgets.GetCarrierQuestionsList1.Widgets[k].formWidgets.Answercontainer.pageParams.answerDataType == 'DROPDOWN' && $scope.Widgets.GetCarrierQuestionsList1.Widgets[k].$element[0][2].required && $scope.Widgets.GetCarrierQuestionsList1.Widgets[k].$element[0][2].validity.valueMissing)) {
    //     //             carrierTabIncomplete = true;
    //     //             $scope.Widgets.carrier._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Carrier</span><!-- ngIf: badgevalue --></div></a>';

    //     //             break;
    //     //         } else {
    //     //             carrierTabIncomplete = false;
    //     //             $scope.Widgets.carrier._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Carrier</span><!-- ngIf: badgevalue --></div></a>';

    //     //         }
    //     //     }
    //     // }
    //     if ($scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == "true" || $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == "true" || $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == "true" || carrierTabIncomplete || $scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == true || $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == true || $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == true || $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == true || $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == true || $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == true) {

    //         $scope.Widgets.requiredInfoMissingDialog.open();
    //         return false;
    //     }


    //     var forms = [];


    //     // for (var k in $scope.Widgets.GetCarrierQuestionsList1.Widgets) {
    //     //     if (k.indexOf('CreateImPolicyQuestionsForm') > -1) {
    //     //         forms.push(k);
    //     //     }
    //     // }
    //     var isPageDirty = $scope.checkIfPageDirty(forms);
    //     if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
    //         $scope.isCarrierTab = true
    //         $scope.Widgets.newQuoteConfDialog.open();
    //     } else {
    //         $scope.updateCarrierTab();
    //     }
    //     $scope.Widgets.tabs1.next();

    // };

    $scope.RequestBody_stateCodeChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.carriersMap = new Map();
        $scope.Variables.getAnswersForStateQuestions.setInput('stateName', newVal);
    };
    $scope.updateCoveragesTab = function() {
        if ($scope.requote) {
            for (var k in $scope.Widgets.GetImVehicleCoverageList1.Widgets) {
                if (k.indexOf('QuoteVehCovForm') > -1) {
                    var index = k.charAt(k.length - 1);
                    var vehicleCoverageId = $scope.Variables.VehicleCoverageIds.dataSet[index];
                    $scope.Variables.UpdateImVehicleCoverage.setInput($scope.Widgets.GetImVehicleCoverageList1.Widgets[k].dataoutput);
                    $scope.Variables.UpdateImVehicleCoverage.setInput("ID", vehicleCoverageId);
                    $scope.Variables.UpdateImVehicleCoverage.invoke();
                }
            }
            $scope.Variables.UpdateImCoverageInvoke.setInput("RequestBody", $scope.Widgets.UpdateImCoverageForm1.dataoutput.RequestBody);
            $scope.Variables.UpdateImCoverageInvoke.setInput("ID", $scope.Variables.coverageId.dataSet.dataValue);
            $scope.Variables.UpdateImCoverageInvoke.invoke();
        } else {
            if ($scope.Variables.coverageId.dataSet.dataValue != 0) {
                $scope.Variables.UpdateImCoverageInvoke.invoke();
            } else {
                $scope.Variables.CreateImCoverageInvoke.invoke();

            }

            for (var k in $scope.Widgets.GetStateDropDownValuesList2.Widgets) {

                if (k.indexOf('StateCoverageForm') > -1) {
                    var index = k.charAt(k.length - 1);
                    if ($scope.Variables.StateCoverageIds.dataSet[index] == undefined) {
                        //add request

                        $scope.Widgets.GetStateDropDownValuesList2.Widgets[k].submit();
                    } else {
                        var index = k.charAt(k.length - 1);
                        var stateCoverageId = $scope.Variables.StateCoverageIds.dataSet[index];
                        $scope.Variables.UpdateImStateSpecificCoverage.setInput($scope.Widgets.GetStateDropDownValuesList2.Widgets[k].dataoutput);
                        $scope.Variables.UpdateImStateSpecificCoverage.setInput("ID", stateCoverageId);
                        $scope.Variables.UpdateImStateSpecificCoverage.invoke();

                    }
                }
            }

            for (var k in $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets) {
                if (k.indexOf('CreateImVehicleCoverageForm') > -1) {
                    var index = k.charAt(k.length - 1);
                    if ($scope.Variables.VehicleCoverageIds.dataSet[index] == undefined) {
                        if ($scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].dataoutput.RequestBody.collDeduct != null && $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].dataoutput.RequestBody.comprehensive != null && $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].dataoutput.RequestBody.rentalDeduct != null && $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].dataoutput.RequestBody.towingDeduct != null && $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].dataoutput.RequestBody.policyId != null) {
                            $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].submit();
                        }

                    } else {
                        var index = k.charAt(k.length - 1);
                        var vehicleCoverageId = $scope.Variables.VehicleCoverageIds.dataSet[index];
                        if ($scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].$element.hasClass('ng-dirty')) {
                            $scope.Variables.UpdateImVehicleCoverage.setInput($scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].dataoutput);
                            $scope.Variables.UpdateImVehicleCoverage.setInput("ID", vehicleCoverageId);
                            $scope.Variables.UpdateImVehicleCoverage.invoke();
                        }



                    }

                    if ($scope.Variables.VehicleAssignmentIds.dataSet[index] == undefined) {
                        var requestBody = {
                            "policyId": $scope.policyId,
                            "vehicleId": $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].formWidgets.RequestBody_vehicleId.datavalue,
                            "driverId": $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].formWidgets.Driver_assignment.datavalue

                        }
                        $scope.Variables.CreateImVehicleAssignment.setInput("RequestBody", requestBody);
                        $scope.Variables.CreateImVehicleAssignment.invoke();
                    } else {
                        var requestBody = {
                            "policyId": $scope.policyId,
                            "vehicleId": $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].formWidgets.RequestBody_vehicleId.datavalue,
                            "driverId": $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k].formWidgets.Driver_assignment.datavalue
                        }
                        $scope.Variables.UpdateImVehicleAssignment.setInput("ID", $scope.Variables.VehicleAssignmentIds.dataSet[index]);
                        $scope.Variables.UpdateImVehicleAssignment.setInput("RequestBody", requestBody);
                        $scope.Variables.UpdateImVehicleAssignment.invoke();

                    }




                }
            }
        }
        $scope.checkInvokePage();

    }



    $scope.button29Click = function($event, $isolateScope) {
        $scope.coveragesButtonClick = true
        var coverageTabDriver_assignment = " ";
        var coverageTabRequestBody_comprehensive = " ";
        var coverageTabRequestBody_collDeduct = " ";
        var coverageTabRequestBody_rentalDeduct = " ";
        var coverageTabRequestBody_towingDeduct = " ";
        var coverageTabcoverageValue = " ";
        var coverageTabRequestBody_coverageName = " ";

        $scope.policyId = $scope.Variables.selectedPolicyId.dataSet.dataValue;
        var coveragesTabIncomplete = false;
        var forms = ["CreateImCoverageForm1"];
        for (var k in $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets) {
            if (k.indexOf('CreateImVehicleCoverageForm') > -1) {
                forms.push(k)
            }
        }

        for (var k in $scope.Widgets.GetStateDropDownValuesList2.Widgets) {
            if (k.indexOf('StateCoverageForm') > -1) {
                forms.push(k)
            }
        }
        var isPageDirty = $scope.checkIfPageDirty(forms);
        if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
            $scope.isCoveragesTab = true;
            $scope.Widgets.newQuoteConfDialog.open();
        } else {
            $scope.updateCoveragesTab();
        }

        var coverageTabRequestBody_bi = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_bi.datavalue;
        var coverageTabRequestBody_pd = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.datavalue;
        var coverageTabRequestBody_mp = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_mp.datavalue;
        var coverageTabRequestBody_uim = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue;
        var coverageTabRequestBody_um = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue;
        var coverageTabRequestBody_umuim = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uiuim.datavalue;

        var state_cd = $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_stateCode.datavalue;
        if (state_cd == 'DC' || state_cd == 'DE' || state_cd == 'OR' || state_cd == 'PA' || state_cd == 'MI') {
            //UR-121 medical payments field not shown for DC/DE/OR/PA/MI
            coverageTabRequestBody_mp = true;
        }


        if (state_cd == 'CA' || state_cd == 'FL' || state_cd == 'MD') {
            //UR-121 medical payments field not shown for DC/DE/OR/PA/MI
            coverageTabRequestBody_um = true;
            coverageTabRequestBody_uim = true;
        }

        if (state_cd != 'CA' && state_cd != 'FL' && state_cd != 'MD') {
            //UR-121 medical payments field not shown for DC/DE/OR/PA/MI
            coverageTabRequestBody_umuim = true;
        }


        // If no vechile is created skip form validation
        if ($scope.Widgets.CreateImVehicleCoverageForm0 != undefined) {
            coverageTabDriver_assignment = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.Driver_assignment.datavalue;
            coverageTabRequestBody_comprehensive = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_comprehensive.datavalue;
            coverageTabRequestBody_collDeduct = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_collDeduct.datavalue;
            coverageTabRequestBody_rentalDeduct = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_rentalDeduct.datavalue;
            coverageTabRequestBody_towingDeduct = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_towingDeduct.datavalue;
        }

        // If no state coverage is created skip form validation
        if ($scope.Widgets.StateCoverageForm0 != undefined) {

            coverageTabcoverageValue = $scope.Widgets.StateCoverageForm0.formWidgets.coverageValue.datavalue;
            coverageTabRequestBody_coverageName = $scope.Widgets.StateCoverageForm0.formWidgets.RequestBody_coverageName.datavalue;

        }

        debugger
        if (!coverageTabRequestBody_bi || !coverageTabRequestBody_pd || !coverageTabRequestBody_mp || !coverageTabRequestBody_uim || !coverageTabRequestBody_um || !coverageTabDriver_assignment || !coverageTabRequestBody_comprehensive || !coverageTabRequestBody_collDeduct || !coverageTabRequestBody_rentalDeduct || !coverageTabRequestBody_towingDeduct || !coverageTabcoverageValue ||
            !coverageTabRequestBody_coverageName || !coverageTabRequestBody_umuim
        ) {

            $scope.Widgets.coverages._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Coverages</span><!-- ngIf: badgevalue --></div></a>';
            coveragesTabIncomplete = true;
            $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue = true;
        } else {
            $scope.Widgets.coverages._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Coverages</span><!-- ngIf: badgevalue --></div></a>';
            coveragesTabIncomplete = false;
            $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue = false;
        }
        $scope.Widgets.tabs1.next();
        $window.scrollTo(0, 0);
    };

    $scope.coverageValueChange = function($event, $isolateScope, item, currentItemWidgets, newVal, oldVal) {
        currentItemWidgets['StateCoverageForm' + (Number(_.findKey($isolateScope.$parent.Variables.StateDefaultOrder.dataSet.content, item)))].formWidgets.RequestBody_coverageValue.datavalue = newVal;
    };

    $scope.RequestBody_vinBlur = function($event, $isolateScope, item, currentItemWidgets) {

        var currentForm = "";

        var formName = $isolateScope.$parent.ngform.$name;
        var vechileUseFormName = "CreateImVehicleUseForm" + formName.charAt(formName.length - 1);
        for (var k in $scope.Widgets) {
            if (k == formName) {
                currentForm = $scope.Widgets[k];
                break;
            }
        }
        $scope.currentForm = currentForm;
        var sv = $scope.Variables.CheckVin;
        sv.setInput("VIN", currentForm.formWidgets.RequestBody_vin.datavalue)
        sv.setInput("username", "stg")
        sv.setInput("password", "stg")
        sv.invoke({}, function(data) {

            var flag1 = true,
                flag2 = true;
            var vinRequestDetails = [];
            for (var keyName in data.Vehicle.Data) {

                var value = data.Vehicle.Data[keyName];
                if (value.Key == "Error_Status" && value.Value == 10000000000000000) {
                    $scope.currentForm.formWidgets.RequestBody_vin.focus()
                    $scope.currentForm.formWidgets.RequestBody_vin.datavalue = null
                    $scope.Variables.VinNotify.notify()
                } else if (value.Key == "Error_Status" && value.Value == '00000000000000000') {

                    for (var keyName1 in data.Vehicle.Data) {

                        var value1 = data.Vehicle.Data[keyName1];
                        if (value1.Key == "NCICMake") {
                            var yearBody = {
                                "make": value1.Value
                            }
                            vinRequestDetails.push(yearBody)
                        }
                        if (value1.Key == "Model") {
                            var yearBody = {
                                "model": value1.Value
                            }
                            vinRequestDetails.push(yearBody)
                        }
                        if (value1.Key == "Year") {
                            $scope.currentForm.formWidgets.RequestBody_year.datavalue = value1.Value
                            var yearBody = {
                                "year": value1.Value
                            }
                            vinRequestDetails.push(yearBody)
                        }
                        if (value1.Key == "Body_Type") {


                            $scope.currentForm.formWidgets.RequestBody_subModel.datavalue = value1.Value
                        }

                        //imaginea

                        if (value1.Key == "ANTI_LOCK") {
                            flag1 = false;
                            $scope.currentForm.formWidgets.RequestBody_antiLockBrake.datavalue = value1.Value
                        }
                        if (value1.Key == "DAY_RUN_LAMPS") {
                            flag2 = false;
                            $scope.currentForm.formWidgets.RequestBody_daytimeRunningLight.datavalue = value1.Value
                        }
                        if (value1.Key == "List_Price") {

                            $scope.Widgets[vechileUseFormName].formWidgets.RequestBody_costNew.datavalue = value1.Value
                        }
                    }

                }
            }
            if (vinRequestDetails.length > 0) {
                debugger
                $scope.RequestBody_yearChange($event, $isolateScope, "", "", vinRequestDetails[2].make, vinRequestDetails[0].model)
            }
            if (flag1 == true) {
                $scope.currentForm.formWidgets.RequestBody_antiLockBrake.datavalue = "Yes"
            }
            if (flag2 == true) {
                $scope.currentForm.formWidgets.RequestBody_daytimeRunningLight.datavalue = "Yes"
            }
        }, function(error) {
            // Error Callback

        });
    };


    $scope.vehicleSelect = function($event, $isolateScope) {


        $scope.vehicleButtonClick = false
        if ($scope.viewQuery) {
            $scope.Variables.getVehicleUseByPolicyId.setInput("q", $scope.viewQuery);
            $scope.Variables.getVehicleUseByPolicyId.invoke();

            $scope.Variables.getPolicyVehicles.setInput("q", $scope.viewQuery);
            $scope.Variables.getPolicyVehicles.invoke();
        }




        //$scope.controlFlowTab($event, $isolateScope, "vehicle");


        // $timeout(function() {
        //     var formScope = $scope.Widgets.ListDataGroup1.getWidgets('VehicleForm0')[0];
        //     formScope.elScope.ngform = formScope[formScope.name];
        //     $scope.Variables.GetAutoOwnershipType.invoke();
        //     $scope.Variables.GetUseageType.invoke();
        $timeout(function() {
            $scope.Widgets.VehicleForm0.show = true;
            $scope.Widgets.CreateImVehicleUseForm0.show = true;
            var numVehicles = $scope.Variables.getPolicyVehicles.dataSet.numberOfElements;

            //$scope.Widgets.numVehicles.datavalue = numVehicles;
            // $scope.setVehiclesButton(numVehicles);

            // }, 500)
            if (numVehicles == 0 || numVehicles == undefined) {
                numVehicles = 1;
            }

            $scope.Variables.numVehiclesList.dataSet.dataValue = numVehicles;
            $scope.setVehiclesButton(numVehicles);
            $scope.Widgets.numVehicles.datavalue = numVehicles;
            //$window.scrollTo(0, 0);

        }, 1000);
    };

    $scope.driverSelect = function($event, $isolateScope) {
        $scope.driverButtonClick = false

        if ($scope.viewQuery) {
            $scope.Variables.getDriversByPolicyId.setInput("q", $scope.viewQuery);
            $scope.Variables.getDriversByPolicyId.invoke();
        }






        //$scope.controlFlowTab($event, $isolateScope, "driver");

        // var formScope = $scope.Widgets.ListDataGroup2.getWidgets('DriverForm0')[0];
        // formScope.elScope.ngform = formScope[formScope.name];
        // $scope.Variables.getImApplicant.invoke();

        $scope.loadDriverData();






    };

    $scope.loadDriverData = function() {
        $timeout(function() {
            $scope.Widgets.DriverForm0.show = true;
            $scope.Widgets.DriverForm0.formWidgets.RequestBody_gender.datavalue = $scope.clientTabGender;
            $scope.Widgets.DriverForm0.dataoutput.RequestBody.gender = $scope.clientTabGender;
            $scope.Variables.GetDriverOccupationType.invoke();
            $scope.Variables.GetDriverOccupationType1.invoke();
            $scope.Variables.GetDriverOccupationType2.invoke();
            $scope.Variables.GetDriverOccupationType3.invoke();
            $scope.Variables.GetDriverOccupationType4.invoke();
            $scope.Variables.GetDriverOccupationType5.invoke();
            var numDriver = $scope.Variables.getDriversByPolicyId.dataSet.numberOfElements;


            //$scope.Widgets.numDrivers.datavalue = numDriver;
            if (numDriver == 0 || numDriver == undefined) {
                numDriver = 1;
            }
            $scope.Variables.numDriversList.dataSet.dataValue = numDriver;
            $scope.setDriversButton(numDriver);
            $scope.Widgets.numDrivers.datavalue = numDriver;
        }, 1500);



    }



    $scope.RequestBody_useageChange = function($event, $isolateScope, item, currentItemWidgets, newVal, oldVal) {
        debugger;


        var formName = $isolateScope.$parent.ngform.$name;
        var currentForm = $scope.Widgets[formName];


        if (newVal == "To/From Work" || newVal == "To/From School") {
            $scope.calculateAnnualMiles($event, $isolateScope);
        } else {
            //   currentForm.formWidgets.RequestBody_annualMiles.datavalue = "";
            if (formName.indexOf('1') != -1) {
                currentForm.formWidgets.RequestBody_annualMiles1.datavalue = "";
            } else if (formName.indexOf('2') != -1) {
                currentForm.formWidgets.RequestBody_annualMiles2.datavalue = "";
            } else if (formName.indexOf('3') != -1) {
                currentForm.formWidgets.RequestBody_annualMiles3.datavalue = "";
            } else {
                currentForm.formWidgets.RequestBody_annualMiles.datavalue = "";
            }

        }
    };

    $scope.RequestBody_modelChange = function($event, $isolateScope, item, currentItemWidgets) {

        var modelForm = "";
        var modelCall = "";
        var modelFormName = $isolateScope.$parent.ngform.$name;
        for (var k in $scope.Widgets) {
            if (k == modelFormName) {
                modelForm = $scope.Widgets[k];
                break;
            }
        }
        $scope.modelForm = modelForm;
        modelCall = $scope.Variables.CheckMakeModel;

        modelCall.setInput("MAKE", $scope.modelForm.formWidgets.RequestBody_make.datavalue)
        modelCall.setInput("YEAR", $scope.modelForm.formWidgets.RequestBody_year.datavalue)
        modelCall.setInput("MODEL", $scope.modelForm.formWidgets.RequestBody_model.datavalue)
        modelCall.setInput("username", "stg")
        modelCall.setInput("password", "stg")
        modelCall.invoke({}, function(data) {
            if (data.VinPrefixDetail != undefined) {
                if (data.VinPrefixDetail[0] == undefined) {
                    $scope.modelForm.formWidgets.RequestBody_vin.datavalue = data.VinPrefixDetail.VinPrefix
                    $scope.modelForm.formWidgets.RequestBody_subModel.datavalue = data.VinPrefixDetail.BodyStyle
                } else {
                    $scope.modelForm.formWidgets.RequestBody_vin.datavalue = data.VinPrefixDetail[0].VinPrefix
                    $scope.modelForm.formWidgets.RequestBody_subModel.datavalue = data.VinPrefixDetail[0].BodyStyle
                }
            }
        }, function(error) {
            // Error Callback
        });
    };

    $scope.AccidentDescChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_description.datavalue = newVal
    };


    $scope.ViolationDescChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_description.datavalue = newVal
    };


    $scope.LossDescChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_description.datavalue = newVal
    };


    $scope.carrierSelect = function($event, $isolateScope) {
        //  ;


    }

    $scope.maritalStatusChange = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "Married" || newVal == "Domestic Partner") {
            $scope.Widgets.CreateImApplicantForm1.formWidgets.coapplicant.datavalue = 1
        } else {
            $scope.Widgets.CreateImApplicantForm1.formWidgets.coapplicant.datavalue = 0
        }
    };


    $scope.button17_1Click = function($event, $isolateScope) {;
        $scope.incidentsButtonClick = true
        //Check if the Incidents Form is not empty if it is not then show the popup.
        console.log('Incident Next button Click');
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue);
        console.log($scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue);

        var incidentDate = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue;
        var incidentDriver = $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue;
        var accidentDesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue;
        var incidentAmount = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue;
        var incidentVehicle = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue;
        var violationDesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue;
        var lossdesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue;
        var incidentsFormValid = $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$valid;

        var incidentTabFormFilledComplete = false;
        var incidentTabFormInComplete = false;
        var incidentType = $scope.Widgets.RequestBody_incidentType.datavalue;
        /*if (!incidentsFormValid && !((!incidentDate || incidentDate == "") && !incidentDriver && !accidentDesc && !incidentAmount &&
                incidentVehicle == undefined && !violationDesc && !lossdesc)) {
            incidentTabFormInComplete = true;*/
        if ((incidentType == 'Accident' && !((!incidentDate || incidentDate == "") && (!incidentDriver || incidentDriver == "") && (!accidentDesc || accidentDesc == "") && (!incidentAmount || incidentAmount == "") &&
                (incidentVehicle == undefined || incidentVehicle == ""))) || (incidentType == 'Violation' && !((!incidentDate || incidentDate == "") && (!incidentDriver || incidentDriver == "") && (!violationDesc || violationDesc == ""))) || (incidentType == 'Comp Loss' && !((!incidentDate || incidentDate == "") && (!incidentDriver || incidentDriver == "") && (!lossdesc || lossdesc == "" && (!incidentAmount || incidentAmount == "") &&
                (incidentVehicle == undefined || incidentVehicle == ""))))) {


            incidentTabFormInComplete = true;
        } else if (((incidentDate && incidentDate != '') && incidentDriver && incidentAmount && (incidentVehicle || incidentVehicle >= 0) && accidentDesc) || ((incidentDate && incidentDate != '') && incidentDriver && violationDesc) || ((incidentDate && incidentDate != '') && incidentDriver && incidentAmount && (incidentVehicle || incidentVehicle >= 0) && lossdesc)) {
            incidentTabFormFilledComplete = true;
        }

        if (incidentTabFormFilledComplete) {
            $scope.Widgets.confirmIncidentsSave3.open();
        } else if (incidentTabFormInComplete) {
            $scope.Widgets.confirmIncidentsSave1.open();
        } else {
            $window.scrollTo(0, 0);
            $scope.Widgets.tabs1.next();
        }
    };

    $scope.form_field132Blur = function($event, $isolateScope, item, currentItemWidgets) {;
        $isolateScope.datavalue = $scope.formatDate($event.currentTarget.value);

        var formName = $isolateScope.$parent.ngform.$name
        var incidentDate = new Date($scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue);

        $scope.todaysDate = new Date();
        if (incidentDate > $scope.todaysDate) {
            $scope.Widgets.incidentDateAlert.open();
        }
    };


    $scope.GetCarrierXmlonSuccess = function(variable, data, options) {

        data.forEach(function(obj) {
            var channel = $scope.pusher.subscribe(obj.insuraMatchID);
            channel.bind(obj.id, function(data) {

                var parser = new DOMParser();
                var xmlDoc = parser.parseFromString(data.message, "text/xml");
                var carrierTag = xmlDoc.getElementsByTagName("Carrier")
                var quoteExecutionID = xmlDoc.getElementsByTagName("QuoteExecutionID")
                var insuraMatchID = xmlDoc.getElementsByTagName("InsuraMatchID")
                var totalPremium = xmlDoc.getElementsByTagName("ns2:TotalPrem")
                var paidInFull = xmlDoc.getElementsByTagName("ns2:Amt")
                var term = xmlDoc.getElementsByTagName("ns2:Term")
                var discounts = xmlDoc.getElementsByTagName("ns2:Discount")
                var discountDetails = [];
                var underwritingMessages = [];
                var messages = xmlDoc.getElementsByTagName("ns2:Restriction")
                var pay = xmlDoc.getElementsByTagName("ns2:PPs")
                var status = xmlDoc.getElementsByTagName("Status")
                var statusText = xmlDoc.getElementsByTagName("StatusText")
                var paymentOptions = [];
                var paymentOptions = [];
                var paymentObject = {};

                for (var i = 0; i < discounts.length; i++) {
                    discountDetails.push(discounts[i].getAttribute("name"));
                }
                for (var i = 0; i < messages.length; i++) {
                    underwritingMessages.push(messages[i].children[0].textContent);
                }
                for (var i = 0; i < pay.length; i++) {
                    paymentObject.plan = pay[i].getAttribute("Name")
                    for (var j = 0; j < pay[i].children.length; j++) {
                        if (pay[i].children[j] != undefined) {
                            if (pay[i].children[j].getAttribute("name") == "TotalAmt") {

                                paymentObject.amount = "$" + pay[i].children[j].textContent
                            }
                            if (pay[i].children[j].getAttribute("name") == "Desc") {


                                paymentObject.desc = pay[i].children[j].textContent
                            }
                            if (pay[i].children[j].getAttribute("name") == "DownPayment") {


                                paymentObject.downPay = "$" + pay[i].children[j].textContent
                            }
                            if (pay[i].children[j].getAttribute("name") == "InstallmentPayment") {


                                paymentObject.installPay = "$" + pay[i].children[j].textContent
                            }
                        }
                    }
                    paymentOptions.push(paymentObject);
                    var paymentObject = {};
                }

                var carrImage = null;
                if (carrierTag[0] != null && carrierTag[0].getAttribute("Name") != null) {
                    if (carrierTag[0].getAttribute("Name") == "Bristol West") {
                        carrImage = "resources/images/imagelists/logo-bristol-west.png"
                    }
                    if (carrierTag[0].getAttribute("Name").includes("MetLife Auto")) {
                        carrImage = "resources/images/imagelists/Metlife.png"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Plymouth Rock Assurance NJ") {
                        carrImage = "resources/images/imagelists/Plymouthrock.png"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Progressive Insurance") {
                        carrImage = "resources/images/imagelists/Progressive.png"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Safeco Insurance") {
                        carrImage = "resources/images/imagelists/safeco_logo_staticNav.jpg"
                    }
                    if (carrierTag[0].getAttribute("Name") == "National General") {
                        carrImage = "resources/images/imagelists/national_general.png"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Stillwater Property & Casualty") {

                        carrImage = "resources/images/imagelists/StillWater.png"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Mercury Auto Insurance") {
                        carrImage = "resources/images/imagelists/Mercury.jpg"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Dairyland Insurance") {
                        carrImage = "resources/images/imagelists/Dairyland.png"
                    }
                    if (carrierTag[0].getAttribute("Name") == "The General") {
                        carrImage = "resources/images/imagelists/The_General.jpg"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Omni Insurance") {
                        carrImage = "resources/images/imagelists/Good2Go_Omni.png"
                    }
                    if (carrierTag[0].getAttribute("Name") == "Liberty Mutual") {
                        carrImage = "resources/images/imagelists/logo-lm.png"
                    }
                }
                if (carrierTag[0].getAttribute("Name") == "The General") {
                    if (totalPremium.length == 0) {
                        totalPremium = paidInFull;
                        //UR-162 National General Premium fix    
                    }

                }
                var carrierData = {
                    "image": carrImage,
                    "carrierName": carrierTag[0].getAttribute("Name"),
                    "carrierId": carrierTag[0].getAttribute("ID"),
                    "totalPremium": totalPremium.length > 0 ? "$" + totalPremium[0].textContent + " /" + term[0].textContent + " months" : (paidInFull.length > 0 ? paidInFull[0].textContent + " /" + term[0].textContent + " months" : ""),
                    "paidInFull": paidInFull.length > 0 ? "$" + paidInFull[0].textContent : "",
                    "payDetails": paymentOptions.length > 0 ? paymentOptions : "",
                    "discountCount": discounts.length > 0 ? discounts.length + " discounts" : "",
                    "discountDetails": discountDetails.length > 0 ? discountDetails : "",
                    "underwritingMessages": underwritingMessages.length > 0 ? underwritingMessages : "",
                    "status": status[0] != undefined ? status[0].textContent : "",
                    "statusText": statusText[0] != undefined ? statusText[0].textContent : ""


                }
                if ($scope.Variables.PusherCarrier.dataSet.filter(e => e.carrierName === carrierData.carrierName).length == 0) {

                    $scope.Variables.PusherCarrier.dataSet.push(carrierData);
                    $scope.Widgets.PusherCarrierList1.binddataset;
                    $scope.$apply();
                }
            });

        });
    };


    $scope.quoteSelect = function($event, $isolateScope) {
        //reload issue
        sessionStorage.setItem('currentPolicyId', $scope.policyId);
        $window.scrollTo(0, 0);
        $timeout(function() {
            debugger;

            $scope.Variables.GetBIType.invoke();
            $scope.Variables.GetUIMType.invoke();
            $scope.Variables.GetPDType.invoke();
            $scope.Variables.GetCompLossDescType.invoke();
            $scope.Variables.GetCollisionDeductibleType.invoke();
            $scope.Variables.GetStateCarriers.invoke();
        }, 3000)
        debugger;
        if ($scope.viewQuery) {
            $scope.Variables.getPolicyVehicles.setInput("q", $scope.viewQuery);
            $scope.Variables.getPolicyVehicles.invoke();

        }

        /*var query = "policyId= " + $scope.policyId;
        $scope.Variables.GetImCoverageByPolicyId.setInput("q", query);
        $scope.Variables.GetImCoverageByPolicyId.invoke();
        $scope.Variables.GetImVehicleCoverageInvoke.setInput("q", query);
        $scope.Variables.GetImVehicleCoverageInvoke.invoke();*/
        //$scope.Variables.PusherCarrier.dataSet = [];
    };

    $scope.GetStateCarriersonSuccess = function(variable, data, options) {

        $scope.Widgets.checkboxset1.dataset = [];
        $scope.Widgets.checkboxset1.dataset = data.content
        $scope.Widgets.checkboxset1.binddataset;
        $scope.$apply();
    };



    $scope.CreateImApplicantInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.applicantId.dataSet.dataValue = data.applicantId;
        $scope.Variables.getImApplicant.invoke();

        //imaginea
        var requestBody = {
            driverLicenseNumber: data.licenseToDrive,
            dob: data.dob,
            firstName: data.firstName,
            gender: data.gender,
            industry: data.industry,
            lastName: data.lastName,
            maritalStatus: data.maritalStatus,
            occupation: data.occupation,
            policyId: data.policyId,
            goodDriver: "''",
            matDriver: "''",
            driverTraining: "''",
            student100: "''",
            goodStudent: "''"
        }

        $scope.Variables.CreateImDriverInvoke.setInput("RequestBody", requestBody);
        $scope.Variables.CreateImDriverInvoke.invoke();


    };

    $scope.CheckAddressAndCoApp = function() {

        if ($scope.Widgets.coapplicant.datavalue == 1) {
            if ($scope.Variables.coApplicantId.dataSet.dataValue != 0) {
                $scope.Variables.UpdateCoApplicant.invoke()
            } else {
                $scope.Variables.CreateImApplicantInvoke1.invoke()
            }

        }
        if ($scope.Variables.addressId.dataSet.dataValue != 0) {
            $scope.Variables.UpdateImAddress.invoke()
        } else {
            $scope.Variables.CreateImAddressInvoke.invoke()
        }
        if ($scope.Variables.residenceInfoId.dataSet.dataValue != 0) {

            $scope.Variables.updateResidenceInfo.setInput("ID", $scope.Variables.residenceInfoId.dataSet.dataValue);
            $scope.Variables.updateResidenceInfo.invoke();
        } else {

            $scope.Variables.createResidenceInfo.invoke();
        }


    };


    $scope.CreateImDriverInvokeonSuccess = function(variable, data, options) {

        $scope.Variables.driversIds.addItem(data.driverId);
        var q = "policyId = " + data.policyId;
        $scope.Variables.getDriversByPolicyId.setInput("q", q);
        $scope.Variables.getDriversByPolicyId.invoke();
    };


    $scope.CreateImVehicleInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.vehicleIds.addItem(data.vehicleId);
        var q = "policyId = " + data.policyId;
        $scope.Variables.getPolicyVehicles.setInput("q", q);
        $scope.Variables.getPolicyVehicles.invoke();
        // $scope.Variables.GetImVehicleList.setInput("q", q);
        // $scope.Variables.GetImVehicleList.invoke();
        $scope.Variables.getVehicleByPolicyId.setInput("q", q);
        $scope.Variables.getVehicleByPolicyId.invoke();

        var index = $scope.Variables.vehicleIds.dataSet.length - 1;
        var formName = "CreateImVehicleUseForm" + index;
        var vehUseForm = $scope.Widgets[formName];


        vehUseForm.formWidgets.RequestBody_vehicleId.datavalue = data.vehicleId;
        vehUseForm.dataoutput.RequestBody.vehicleId = data.vehicleId;
        $scope.Variables.CreateImVehicleUseInvoke.setInput(vehUseForm.dataoutput);
        $scope.Variables.CreateImVehicleUseInvoke.invoke();


    };


    $scope.CreateImVehicleUseInvokeonSuccess = function(variable, data, options) {

        $scope.Variables.vehicleUseIds.addItem(data.vehicleUseId);
    };


    $scope.CreateImEligibilityInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.eligibilityId.dataSet.dataValue = data.eligibilityId
    };


    $scope.CreateImCoverageInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.coverageId.dataSet.dataValue = data.coverageId;
    };




    $scope.CreateImPriorPolicyInfoInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.priorPolicyId.dataSet.dataValue = data.priorPolicyInfoId
    };


    $scope.computeIndexOfIncidents = function($event, $isolateScope, item, currentItemWidgets) {


        var data = [];
        if (item.incidentType == 'Accident') {
            data = $scope.Variables.GetAccidentListByPolicyId.dataSet.content;
        } else if (item.incidentType == 'Violation') {
            data = $scope.Variables.GetIncidentTypeListInvoke1.dataSet.content;
        } else if (item.incidentType == 'Comp Loss') {
            data = $scope.Variables.GetCompLossListByPolicyId.dataSet.content;
        }


        for (var i = 0; i < data.length; i++) {
            if (data[i].incidentId == item.incidentId) {
                $scope.Variables.getIncidentIndex.dataSet.dataValue = i;
                return;
            }
        }

    }

    $scope.button24Click = function($event, $isolateScope, item, currentItemWidgets) {;

        $scope.computeIndexOfIncidents($event, $isolateScope, item, currentItemWidgets);

        $scope.Widgets.RequestBody_incidentType.datavalue = item.incidentType;
        $scope.Variables.selectedIncidentId.dataSet.dataValue = item.incidentId;
        $scope.Variables.isIncidentUpdateRequest.dataSet.dataValue = true;

        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = item.date;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = item.driverId;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = item.description;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = item.value;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = item.vehicleInvolved;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.incidentID.dataValue = item.incidentId;
        //veh_id

    };


    $scope.button25Click = function($event, $isolateScope, item, currentItemWidgets) {;
        $scope.computeIndexOfIncidents($event, $isolateScope, item, currentItemWidgets);
        $scope.Widgets.RequestBody_incidentType.datavalue = item.incidentType;
        $scope.Variables.selectedIncidentId.dataSet.dataValue = item.incidentId;
        $scope.Variables.isIncidentUpdateRequest.dataSet.dataValue = true;

        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = item.date;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = item.driverId;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = item.description;
        //  $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = item.value;



    };


    $scope.button26Click = function($event, $isolateScope, item, currentItemWidgets) {;
        $scope.computeIndexOfIncidents($event, $isolateScope, item, currentItemWidgets);
        $scope.Widgets.RequestBody_incidentType.datavalue = item.incidentType;
        $scope.Variables.selectedIncidentId.dataSet.dataValue = item.incidentId;
        $scope.Variables.isIncidentUpdateRequest.dataSet.dataValue = true;

        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = item.date;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = item.driverId;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = item.description;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = item.value;
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = item.vehicleInvolved;
    };


    $scope.button25_1Click = function($event, $isolateScope, item, currentItemWidgets) {

        $scope.Variables.deleteIMIncident.setInput("ID", item.incidentId);
        $scope.Variables.deleteIMIncident.invoke();
    };


    $scope.button26_1Click = function($event, $isolateScope, item, currentItemWidgets) {

        $scope.Variables.deleteIMIncident.setInput("ID", item.incidentId);
        $scope.Variables.deleteIMIncident.invoke();
    };

    $scope.button27_1Click = function($event, $isolateScope, item, currentItemWidgets) {

        $scope.Variables.deleteIMIncident.setInput("ID", item.incidentId);
        $scope.Variables.deleteIMIncident.invoke();

    };


    $scope.deleteIMIncidentonSuccess = function(variable, data, options) {;
        var driverIdList = "";
        if ($scope.Variables.driversIds.dataSet.length > 0) {
            for (var id in $scope.Variables.driversIds.dataSet) {
                driverIdList = driverIdList + $scope.Variables.driversIds.dataSet[id] + ",";
            }
        }
        driverIdList = driverIdList.substring(0, driverIdList.length - 1);
        var driverId = driverIdList;
        driverIdList = '(' + driverIdList + ')';
        var query = "incidentType='Accident'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke.invoke();
        $scope.Variables.GetAccidentListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetAccidentListByPolicyId.invoke();

        query = "incidentType='Violation'  and driverId in " + driverIdList;
        $scope.Variables.GetIncidentTypeListInvoke1.setInput("q", query);
        $scope.Variables.GetIncidentTypeListInvoke1.invoke();

        //query = "incidentType='Comp Loss'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke2.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke2.invoke();
        $scope.Variables.GetCompLossListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetCompLossListByPolicyId.invoke();

        //$scope.Variables.GetAccidentList.setInput("driverIds", driverId);
        //$scope.Variables.GetAccidentList.invoke();


    };

    $scope.GetIncidentOnSuccess = function(data, requestBodyUpdateIncident) {

        //imaginea
        var selectedId = "";
        var index = $scope.Variables.getIncidentIndex.dataSet.dataValue;
        selectedId = data.content[index].incidentId;
        requestBodyUpdateIncident.RequestBody.driverId = data.content[index].driverId;
        $scope.Variables.updateIMIncident.setInput("ID", selectedId);
        $scope.Variables.updateIMIncident.setInput(requestBodyUpdateIncident);
        $scope.Variables.updateIMIncident.invoke();
    }

    $scope.updateIncidentsTab = function() {;

        //imaginea
        if ($scope.Variables.isIncidentUpdateRequest.dataSet.dataValue) {


            var requestBodyUpdateIncident = $scope.Widgets.CreateImIncidentForm1.dataoutput;
            var incType = requestBodyUpdateIncident.RequestBody.incidentType;

            if (incType == 'Comp Loss') {
                requestBodyUpdateIncident.RequestBody.description = requestBodyUpdateIncident.LossDesc;
                //    selectedId = $scope.Variables.GetCompLossListByPolicyId.dataSet.content[index].incidentId;
                if (!$scope.Variables.RequestFromClientPage.dataSet.dataValue && !$scope.Variables.requestForQuotesTab.dataSet.dataValue) {
                    $scope.isIncidentsTab = false;
                    $scope.Variables.updateIMIncident.setInput("ID", $scope.Variables.selectedIncidentId.dataSet.dataValue);
                    $scope.Variables.updateIMIncident.setInput(requestBodyUpdateIncident);
                    $scope.Variables.updateIMIncident.invoke();

                } else {
                    $scope.Variables.GetCompLossListByPolicyId.invoke({}, function(data) {

                        $scope.GetIncidentOnSuccess(data, requestBodyUpdateIncident);
                    }, function(error) {
                        // Error Callback
                        console.log("error", error)
                    });

                }
            } else if (incType == 'Violation') {
                requestBodyUpdateIncident.RequestBody.description = requestBodyUpdateIncident.ViolationDesc;
                //$scope.Variables.GetIncidentTypeListInvoke1.dataSet.content[index].incidentId;
                if (!$scope.Variables.RequestFromClientPage.dataSet.dataValue && !$scope.Variables.requestForQuotesTab.dataSet.dataValue) {
                    $scope.isIncidentsTab = false;
                    $scope.Variables.updateIMIncident.setInput("ID", $scope.Variables.selectedIncidentId.dataSet.dataValue);
                    $scope.Variables.updateIMIncident.setInput(requestBodyUpdateIncident);
                    $scope.Variables.updateIMIncident.invoke();

                } else {
                    $scope.Variables.GetIncidentTypeListInvoke1.invoke({}, function(data) {


                        $scope.GetIncidentOnSuccess(data, requestBodyUpdateIncident);
                    }, function(error) {
                        // Error Callback
                        console.log("error", error)
                    });

                }

            } else if (incType == 'Accident') {
                requestBodyUpdateIncident.RequestBody.description = requestBodyUpdateIncident.AccidentDesc;
                if (!$scope.Variables.RequestFromClientPage.dataSet.dataValue && !$scope.Variables.requestForQuotesTab.dataSet.dataValue) {
                    $scope.isIncidentsTab = false;
                    $scope.Variables.updateIMIncident.setInput("ID", $scope.Variables.selectedIncidentId.dataSet.dataValue);
                    $scope.Variables.updateIMIncident.setInput(requestBodyUpdateIncident);
                    $scope.Variables.updateIMIncident.invoke();

                } else {
                    $scope.Variables.GetAccidentListByPolicyId.invoke({}, function(data) {

                        $scope.GetIncidentOnSuccess(data, requestBodyUpdateIncident);
                    }, function(error) {
                        // Error Callback
                        console.log("error", error)
                    });

                }

            }
        } else {
            var requestBodyCreateIncident = $scope.Widgets.CreateImIncidentForm1.dataoutput;

            var incType = requestBodyCreateIncident.RequestBody.incidentType;
            if (incType == 'Comp Loss') {
                requestBodyCreateIncident.RequestBody.description = requestBodyCreateIncident.LossDesc;
            } else if (incType == 'Violation') {
                requestBodyCreateIncident.RequestBody.description = requestBodyCreateIncident.ViolationDesc;
            } else if (incType == 'Accident') {
                requestBodyCreateIncident.RequestBody.description = requestBodyCreateIncident.AccidentDesc;
            }

            $scope.Variables.CreateImIncidentInvoke.setInput(requestBodyCreateIncident);
            $scope.Variables.CreateImIncidentInvoke.invoke();
        }
        $scope.Variables.isIncidentUpdateRequest.dataSet.dataValue = false;
        $scope.checkInvokePage();
    };

    // $scope.updateIncidentsTab = function() {;
    //      
    //     if ($scope.Variables.isIncidentUpdateRequest.dataSet.dataValue) {
    //         $scope.Variables.updateIMIncident.setInput("ID", $scope.Variables.selectedIncidentId.dataSet.dataValue);
    //         var requestBodyUpdateIncident = $scope.Widgets.CreateImIncidentForm1.dataoutput;
    //         var incType = requestBodyUpdateIncident.RequestBody.incidentType;

    //         if (incType == 'Comp Loss') {
    //             requestBodyUpdateIncident.RequestBody.description = requestBodyUpdateIncident.LossDesc;
    //         } else if (incType == 'Violation') {
    //             requestBodyUpdateIncident.RequestBody.description = requestBodyUpdateIncident.ViolationDesc;
    //         } else if (incType == 'Accident') {
    //             requestBodyUpdateIncident.RequestBody.description = requestBodyUpdateIncident.AccidentDesc;
    //         }

    //         $scope.Variables.updateIMIncident.setInput(requestBodyUpdateIncident);
    //         $scope.Variables.updateIMIncident.invoke();

    //     } else {

    //         $scope.Widgets.CreateImIncidentForm1.submit();
    //     }
    //     $scope.Variables.isIncidentUpdateRequest.dataSet.dataValue = false;
    //     $scope.checkInvokePage();
    // };

    $scope.saveIncButtonClick = function($event, $isolateScope) {;


        var incidentDate = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue;
        var incidentDriver = $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue;
        var accidentDesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue;
        var incidentAmount = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue;
        var incidentVehicle = $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue;
        var violationDesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue;
        var lossdesc = $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue;
        var incidentsFormValid = $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$valid;

        var incidentTabFormFilledComplete = false;
        var incidentTabFormInComplete = false;
        var incidentType = $scope.Widgets.RequestBody_incidentType.datavalue;

        if ((incidentType == 'Accident' && ((!incidentDate || incidentDate == "") || !incidentDriver || !accidentDesc || !incidentAmount ||
                incidentVehicle == undefined)) || (incidentType == 'Violation' && ((!incidentDate || incidentDate == "") || !incidentDriver || !violationDesc)) || (incidentType == 'Comp Loss' && ((!incidentDate || incidentDate == "") || !incidentDriver || !lossdesc || !incidentAmount ||
                incidentVehicle == undefined))) {

            $scope.Widgets.CreateImIncidentForm1.submit();
            return false;

        }

        if ($scope.Variables.completedNewIteration.dataSet.dataValue) {
            $scope.isIncidentsTab = true;
            $scope.Widgets.newQuoteConfDialog.open();
        } else {
            $scope.updateIncidentsTab();
        }
    };


    $scope.RequestBody_dateLicensedBlur = function($event, $isolateScope, item, currentItemWidgets) {;
        //Check the difference between policy date & Defensive Driver Course Date
        //If the difference is 3 years or more then show error.
        /*$scope.errMessage = '';
        var curDate = new Date();

        if(new Date(policyDate) > new Date(defensiveDriverCourseDate)){
          $scope.errMessage = 'End Date should be greater than start date';
          return false;
        }*/

        var formName = $isolateScope.$parent.ngform.$name
        var courseDate = new Date($scope.Widgets[formName].formWidgets.RequestBody_dateLicensed.datavalue);

        $scope.todaysDate = new Date();
        $scope.pastDateValid = new Date().setFullYear($scope.todaysDate.getFullYear() - 3);
        $scope.currentDriverForm = formName;
        if (courseDate < $scope.pastDateValid) {
            $scope.Widgets.courseDateAlert.open();
        }








        // $scope.Widgets.DriverForm0.formWidgets.RequestBody_dateLicensed.validationmessage =
        //     'Invalid Defensive Driver Course Date Specified. Date cannot be more than 3 years in the past. Please use format mm/dd/yyyy';


    };


    $scope.updateIMIncidentonSuccess = function(variable, data, options) {;
        var driverIdList = "";;
        if ($scope.Variables.driversIds.dataSet.length > 0) {
            for (var id in $scope.Variables.driversIds.dataSet) {
                driverIdList = driverIdList + $scope.Variables.driversIds.dataSet[id] + ",";
            }
        }
        driverIdList = driverIdList.substring(0, driverIdList.length - 1);
        var driverId = driverIdList;

        driverIdList = '(' + driverIdList + ')';
        var query = "incidentType='Accident'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke.invoke();
        $scope.Variables.GetAccidentListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetAccidentListByPolicyId.invoke();

        query = "incidentType='Violation'  and driverId in " + driverIdList;
        $scope.Variables.GetIncidentTypeListInvoke1.setInput("q", query);
        $scope.Variables.GetIncidentTypeListInvoke1.invoke();

        //query = "incidentType='Comp Loss'  and driverId in " + driverIdList;
        //$scope.Variables.GetIncidentTypeListInvoke2.setInput("q", query);
        //$scope.Variables.GetIncidentTypeListInvoke2.invoke();
        $scope.Variables.GetCompLossListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetCompLossListByPolicyId.invoke();

        //$scope.Variables.GetAccidentList.setInput("driverIds", driverId);
        //$scope.Variables.GetAccidentList.invoke();


        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = "";
        $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = "";

        $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setPristine();
        $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setUntouched();

    };


    $scope.button27Click = function($event, $isolateScope, item, currentItemWidgets) {

    };


    $scope.CreateImPolicyQuestions1InvokeonSuccess = function(variable, data, options) {

        $scope.Variables.carrierIds.addItem(data.policyQuestionsId);
        $scope.carriersMap.set($scope.Variables.currentPolicyQuestionsFormName.dataSet.dataValue, data.policyQuestionsId);
    };

    $scope.popover2Show = function($event, $isolateScope, item, currentItemWidgets) {
        $scope.Variables.GetMessages.dataSet = item.underwritingMessages
    };


    $scope.popover2_1Show = function($event, $isolateScope, item, currentItemWidgets) {
        $scope.Variables.GetDiscounts.dataSet = item.discountDetails
    };


    $scope.popover7Show = function($event, $isolateScope, item, currentItemWidgets) {
        $scope.Variables.GetPayments.dataSet = item.payDetails
    };


    $scope.CreateImApplicantInvoke1onSuccess = function(variable, data, options) {
        $scope.Variables.coApplicantId.dataSet.dataValue = data.applicantId;
        $scope.Variables.getImApplicant.invoke();

        //imaginea
        var requestBody = {
            driverLicenseNumber: data.licenseToDrive,
            dob: data.dob,
            firstName: data.firstName,
            gender: data.gender,
            industry: data.industry,
            relation: data.relation,
            lastName: data.lastName,
            maritalStatus: data.maritalStatus,
            occupation: data.occupation,
            policyId: data.policyId,
            goodDriver: "''",
            matDriver: "''",
            driverTraining: "''",
            student100: "''",
            goodStudent: "''"
        }

        $scope.Variables.CreateImDriverInvoke.setInput("RequestBody", requestBody);
        //creating second driver
        $scope.Variables.CreateImDriverInvoke.invoke();
    };


    $scope.CreateImAddressInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.addressId.dataSet.dataValue = data.addressId;
        $scope.Variables.getAnswersForStateQuestions.setInput("stateName", data.stateCode)
        //$scope.$root.notifyToUser("Applicant Address is Created", "success");
    };


    $scope.RequestBody_questionAnswer1Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "No") {
            $scope.popupMessage = "<h2>Warning:</h2> Carrier options are limited when vehicles aren't registered to the Named Insured. Progressive may be the only carrier option."
            $scope.Widgets.dialog2.open();
        } else {
            $scope.Widgets.dialog2.close()
        }

    };

    $scope.RequestBody_questionAnswer2Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "No") {
            $scope.popupMessage = '<h2>Warning:</h2> Alternate garage address must be entered.'
            $scope.Widgets.dialog2.open()
        }
    };


    $scope.RequestBody_questionAnswer3Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "Yes") {
            $scope.popupMessage = 'Refer the Knowledge article <a href="https://plymouthrock.my.salesforce.com/articles/Standard_Article/Carriers-who-offer-TNC?popup=false&navBack=H4sIAAAAAAAAAIuuVipWslLyzssvz0lNSU_1yM9NVdJRygaKFSSmp4ZkluSA-KVAvn58aaZ-NkyhPpCDosu-ODWxKDnD1jmxqCgztahYuzwjXzs_LS21SDvEz1mpNhYAYrZ7qWsAAAA" target="_blank">Carriers who offer TNC</a> to determine eligibility.'
            $scope.Widgets.dialog2.open()
        }
    };


    $scope.RequestBody_questionAnswer4Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "Yes") {
            $scope.popupMessage = '<h2>Warning:</h2> Check guidelines to determine  eligibility. '
            $scope.Widgets.dialog2.open()
        }
    };


    $scope.RequestBody_questionAnswer5Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "No") {
            $scope.popupMessage = '<h2>Warning:</h2> Some states require that residents switch their license within a specific time frame. For Suspended Licenses: See Knowledge Article:<a href="https://plymouthrock.my.salesforce.com/articles/Standard_Article/Suspended-Revoked-License-Rules?navBack=H4sIAAAAAAAAAIuuVipWslLyzssvz0lNSU_1yM9NVdJRygaKFSSmp4ZkluSA-KVAvn58aaZ-NkyhPpCDqqs2FgCHRwp0TQAAAA&popup=false" target="_blank">Suspended/Revoked License Rules</a>'
            $scope.Widgets.dialog2.open()
        }
    };


    $scope.RequestBody_questionAnswer10Change = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "Yes") {
            $scope.popupMessage = 'Refer to the Knowledge Article:<a href="https://plymouthrock.my.salesforce.com/kA00B000000DwEZ?srPos=0&srKp=ka0&lang=en_US" target="_blank"> SR-22 and Financial Responsibility Filing'
            $scope.Widgets.dialog2.open()
        }
    };



    $scope.GetStateDropDownValuesonSuccess = function(variable, data, options) {


    };


    $scope.button17_2Click = function($event, $isolateScope) {

        var carrierTabIncomplete = false;

        if ($scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == "true") {
            $scope.Widgets.tabpane1._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Client</span><!-- ngIf: badgevalue --></div></a>';

        }

        if ($scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == "true") {
            $scope.Widgets.tabpane2._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Eligibility</span><!-- ngIf: badgevalue --></div></a>';
        }

        if ($scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == "true") {
            $scope.Widgets.policy._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Policy</span><!-- ngIf: badgevalue --></div></a>';
        }

        if ($scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == "true") {
            $scope.Widgets.driver._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Driver</span><!-- ngIf: badgevalue --></div></a>';
        }

        if ($scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == "true") {
            $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Vehicles</span><!-- ngIf: badgevalue --></div></a>';
        }

        if ($scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == "true") {
            $scope.Widgets.coverages._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Coverages</span><!-- ngIf: badgevalue --></div></a>';

        }

        $scope.Widgets.tabs1.previous();
        $window.scrollTo(0, 0);
    };


    $scope.button28Click = function($event, $isolateScope) {
        $scope.coveragesButtonClick = true
        var coveragesTabIncomplete = false;
        var coverageTabRequestBody_bi = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_bi.datavalue;
        var coverageTabRequestBody_pd = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.datavalue;
        var coverageTabRequestBody_mp = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_mp.datavalue;
        var coverageTabRequestBody_uim = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue;
        var coverageTabRequestBody_um = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue;

        var coverageTabRequestBody_umuim = $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uiuim.datavalue;
        var coverageTabDriver_assignment = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.Driver_assignment.datavalue;
        var coverageTabRequestBody_comprehensive = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_comprehensive.datavalue;
        var coverageTabRequestBody_collDeduct = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_collDeduct.datavalue;
        var coverageTabRequestBody_rentalDeduct = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_rentalDeduct.datavalue;
        var coverageTabRequestBody_towingDeduct = $scope.Widgets.CreateImVehicleCoverageForm0.formWidgets.RequestBody_towingDeduct.datavalue;

        var coverageTabcoverageValue = $scope.Widgets.StateCoverageForm0.formWidgets.coverageValue.datavalue;
        var coverageTabRequestBody_coverageName = $scope.Widgets.StateCoverageForm0.formWidgets.RequestBody_coverageName.datavalue;
        if (!coverageTabRequestBody_bi || !coverageTabRequestBody_pd || !coverageTabRequestBody_mp || !coverageTabRequestBody_uim || !coverageTabRequestBody_um || !coverageTabDriver_assignment || !coverageTabRequestBody_comprehensive || !coverageTabRequestBody_collDeduct || !coverageTabRequestBody_rentalDeduct || !coverageTabRequestBody_towingDeduct || !coverageTabcoverageValue ||
            !coverageTabRequestBody_coverageName
        ) {

            $scope.Widgets.coverages._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Coverages</span><!-- ngIf: badgevalue --></div></a>';
            coveragesTabIncomplete = true;
            $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue = true;
        } else {
            $scope.Widgets.coverages._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Coverages</span><!-- ngIf: badgevalue --></div></a>';
            coveragesTabIncomplete = false;
            $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue = false;
        }

        $scope.Widgets.tabs1.previous();
        $window.scrollTo(0, 0);
    };

    $scope.button32Click = function($event, $isolateScope) {
        $scope.driverButtonClick = true
        var driverTabIncomplete = false;
        var i = 0;
        var numOfDrivers = $scope.Widgets.numDrivers.datavalue;
        numOfDrivers = (numOfDrivers == 0) ? 1 : numOfDrivers;
        for (i = 0; i < numOfDrivers; i++) {
            var dynamicFormName = 'DriverForm' + i;
            if ($scope.Widgets[dynamicFormName][dynamicFormName].$invalid) {
                //$scope.Widgets.driver.title = "Driver*";
                $scope.Widgets.driver._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Driver</span><!-- ngIf: badgevalue --></div></a>';

                $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue = true;
                driverTabIncomplete = true;
                break;
            }
        }

        if (!driverTabIncomplete) {
            $scope.Widgets.driver._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding">Driver</span><!-- ngIf: badgevalue --></div></a>';

            $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue = false;
        }
        $scope.Widgets.tabs1.previous();
        $window.scrollTo(0, 0);
    };


    $scope.CreateImVehicleCoverageInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.VehicleCoverageIds.addItem(data.vehicleCoverageId);
    };

    $scope.CreateImVehicleAssignmentonSuccess = function(variable, data, options) {
        $scope.Variables.VehicleAssignmentIds.addItem(data.vehicleAssignmentId);
        $scope.Variables.getVehicleAssignmntByPolicyId.invoke();
    };

    $scope.CreateImStateSpecificCoverageInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.StateCoverageIds.addItem(data.stateSpecificCoverageId);
    };

    $scope.CreateVehicleAddressonSuccess = function(variable, data, options) {
        $scope.Variables.vehicleAddressIds.addItem(data.addressId);
        $scope.saveVehicle(data.addressId, $scope.vehFormForAddress);
    };

    $scope.altGarageChange = function($event, $isolateScope, item, currentItemWidgets, newVal, oldVal) {

        var formName = $isolateScope.$parent.ngform.$name;
        var currentForm = "";
        var formNumber = formName.slice(-1);
        var createImVehicleUseFormName = 'CreateImVehicleUseForm' + formNumber;
        var createVehicleAddressFormName = 'CreateVehicleAddressForm' + formNumber;

        for (var k in $scope.Widgets) {
            if (k == formName) {
                currentForm = $scope.Widgets[k];
                break;
            }
        }
        if (newVal == "Yes") {
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].RequestBody_address2_formWidget.required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].RequestBody_city_formWidget.required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].RequestBody_stateCode_formWidget.required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].RequestBody_county_formWidget.required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].RequestBody_zipCode5_formWidget.required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][1].required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][3].required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][4].required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][5].required = true;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][6].required = true;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][1].required = true;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][3].required = true;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][4].required = true;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][5].required = true;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][6].required = true;

        } else {
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][1].required = false;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][3].required = false;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][4].required = false;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][5].required = false;
            $scope.Widgets[createImVehicleUseFormName][createImVehicleUseFormName][createVehicleAddressFormName].element[0][6].required = false;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][1].required = false;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][3].required = false;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][4].required = false;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][5].required = false;
            $scope.Widgets.CreateVehicleAddressForm0.$element[0][6].required = false;
        }
    };


    $scope.RequestBody_incidentTypeChange = function($event, $isolateScope, newVal, oldVal) {

        if (newVal === 'Accident') {

            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.required = true;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.required = true;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.required = true;

            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.required = false;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.required = false;
        } else if (newVal === 'Violation') {
            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.required = true;

            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.required = false;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.required = false;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.required = false;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.required = false;

        } else {

            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.required = true;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.required = true;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.required = true;

            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.required = false;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.required = false;
        }

        $scope.Variables.isIncidentUpdateRequest.dataSet.dataValue = false;

    };

    $scope.UpdateImApplicantonSuccess = function(variable, data, options) {
        $scope.Variables.applicantId.dataSet.dataValue = data.applicantId;
        $scope.Variables.getImApplicant.invoke();
        debugger

        var requestBody = $scope.Variables.getDriversByPolicyId.dataSet.content[0]
        requestBody.driverLicenseNumber = data.licenseToDrive;
        requestBody.dob = data.dob;
        requestBody.firstName = data.firstName;
        requestBody.gender = data.gender;
        requestBody.industry = data.industry;
        requestBody.lastName = data.lastName;
        requestBody.maritalStatus = data.maritalStatus;
        requestBody.occupation = data.occupation;
        requestBody.policyId = data.policyId;

        $scope.Variables.updateImDriver.setInput("RequestBody", requestBody);
        $scope.Variables.updateImDriver.setInput("ID", requestBody.driverId);
        $scope.Variables.updateImDriver.invoke();

        $scope.Variables.applicantId.dataSet.dataValue = data.applicantId;
        $scope.Variables.GetDriverOccupationType.invoke();
        // Mask Loaded Data 
        $timeout(function() {
            maskLoadedData();
        }, 5000);

    };

    $scope.GetApplicantInfoByPolicyIdVaronSuccess = function(variable, data, options) {;
        $timeout(function() {
            if ($scope.Widgets.tabs1.activeTab.name === 'tabpane1') {
                if (data.content && data.content.length > 0) {
                    if (data.content[0].industry && data.content[0].industry != null && data.content[0].industry != undefined && data.content[0].industry != '') {
                        $scope.Widgets.CreateImApplicantForm1.formWidgets.RequestBody_industry.datavalue = data.content[0].industry;
                        $scope.Variables.GetOccupationType.setInput("group_name", "Student");
                        //$scope.Variables.GetOccupationType.invoke();                
                    }
                    debugger
                    data.content[0].ssn = $scope.formatSSN(data.content[0].ssn);
                    data.content[0].dob = $scope.formatDate(data.content[0].dob);

                    $scope.Widgets.CreateImApplicantForm1.formWidgets.firstName.datavalue = data.content[0].firstName;
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.middleName.datavalue = data.content[0].middleName;
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.lastName.datavalue = data.content[0].lastName;
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.suffix.datavalue = data.content[0].suffix;
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.gender.datavalue = data.content[0].gender;
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.dob.datavalue = data.content[0].dob;
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.RequestBody_education.datavalue = data.content[0].education;

                    $scope.Widgets.CreateImApplicantForm1.formWidgets.occupation.datavalue = data.content[0].occupation;

                    $scope.Widgets.CreateImApplicantForm1.formWidgets.maritalStatus.datavalue = data.content[0].maritalStatus;
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.ssn.datavalue = $scope.formatSSN(data.content[0].ssn);
                    $scope.Widgets.CreateImApplicantForm1.formWidgets.licenseToDrive.datavalue = data.content[0].licenseToDrive;

                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_address1.datavalue = data.content[0].address1;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_address2.datavalue = data.content[0].address2;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_city.datavalue = data.content[0].city;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_stateCode.datavalue = data.content[0].stateCode;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_county.datavalue = data.content[0].county;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_zipCode5.datavalue = data.content[0].zipCode5;
                    $scope.Widgets.createResidenceInfoForm1.formWidgets.years_at_current.datavalue = data.content[0].yearsAtCurrent;
                    $scope.Widgets.createResidenceInfoForm1.formWidgets.months_at_current.datavalue = data.content[0].monthsAtCurrent;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_email.datavalue = data.content[0].email;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_phoneType.datavalue = data.content[0].phoneType;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_phoneNumber.datavalue = data.content[0].phoneNumber;
                    $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_policyId.datavalue = data.content[0].policyId;

                    $scope.Variables.applicantId.dataSet.dataValue = data.content[0].applicantId;
                    $scope.Variables.addressId.dataSet.dataValue = data.content[0].addressId;

                    $scope.Variables.residenceInfoId.dataSet.dataValue = data.content[0].residenceId;



                }
            }

        }, 2000);
    };

    $scope.GetCoApplicantInfoByPolicyIdVaronSuccess = function(variable, data, options) {;
        $timeout(function() {



            if (data.content && data.content.length > 0) {
                $scope.Variables.coApplicantId.dataSet.dataValue = data.content[0].applicantId;

                if (data.content[0].industry && data.content[0].industry != null && data.content[0].industry != undefined && data.content[0].industry != '') {
                    $scope.Widgets.CoApplicant.formWidgets.RequestBody_industry.datavalue = data.content[0].industry;
                    $scope.Variables.GetCoAppOccupationType.setInput("group_name", data.content[0].industry);
                    //$scope.Variables.GetCoAppOccupationType.invoke();            
                }

                $scope.Widgets.CreateImApplicantForm1.formWidgets.coapplicant.datavalue = 1;


                $scope.Widgets.CoApplicant.formWidgets.RequestBody_firstName.datavalue = data.content[0].firstName;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.firstName = data.content[0].firstName;


                $scope.Widgets.CoApplicant.formWidgets.RequestBody_middleName.datavalue = data.content[0].middleName;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.middleName = data.content[0].middleName;


                $scope.Widgets.CoApplicant.formWidgets.form_field121.datavalue = data.content[0].lastName;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.lastName = data.content[0].lastName;



                $scope.Widgets.CoApplicant.formWidgets.RequestBody_suffix.datavalue = data.content[0].suffix;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.suffix = data.content[0].suffix;




                $scope.Widgets.CoApplicant.formWidgets.RequestBody_dob.datavalue = data.content[0].dob;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.dob = data.content[0].dob;


                $scope.Widgets.CoApplicant.formWidgets.RequestBody_maritalStatus.datavalue = data.content[0].maritalStatus;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.maritalStatus = data.content[0].maritalStatus;

                $scope.Widgets.CoApplicant.formWidgets.RequestBody_relation.datavalue = data.content[0].relation;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.relation = data.content[0].relation

                $scope.Widgets.CoApplicant.formWidgets.RequestBody_occupation.datavalue = data.content[0].occupation;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.occupation = data.content[0].occupation;



                debugger
                data.content[0].dob = $scope.formatDate(data.content[0].dob);
                $scope.Widgets.CoApplicant.formWidgets.RequestBody_ssn.datavalue = $scope.formatSSN(data.content[0].ssn);
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.ssn = $scope.formatSSN(data.content[0].ssn)

            }

            if (data.content && data.content.length > 0 && $scope.Variables.GetCoApplicantInfoByPolicyIdVar.dataSet.content[0].gender != null) {
                $scope.Widgets.CoApplicant.formWidgets.RequestBody_gender.datavalue = $scope.Variables.GetCoApplicantInfoByPolicyIdVar.dataSet.content[0].gender;
                $scope.Widgets.CoApplicant.dataoutput.RequestBody.gender = data.content[0].gender;
            }
        }, 2000);
    };

    $scope.loadAllQuoteData = function() {
        $scope.policyId = $scope.Variables.selectedPolicyId.dataSet.dataValue;
        var query = "policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;
        $scope.viewQuery = query;


        $scope.Variables.getPolicyInfo.setInput("q", $scope.viewQuery);
        $scope.Variables.getPolicyInfo.invoke()

        $scope.Variables.getPriorPolicyInfo.setInput("q", $scope.viewQuery);
        $scope.Variables.getPriorPolicyInfo.invoke()

        $scope.Variables.getApplicantByPolicyId.setInput("q", query);
        $scope.Variables.getApplicantByPolicyId.invoke();
        $scope.Variables.getImApplicant.setInput("q", query);
        $scope.Variables.getImApplicant.invoke();
        $scope.Variables.getAddressByPolicyId.setInput("q", query);
        $scope.Variables.getAddressByPolicyId.invoke();
        $scope.Variables.getResidenceInfoByPolicyId.setInput("q", query);
        $scope.Variables.getResidenceInfoByPolicyId.invoke();
        $scope.Variables.GetCoApplicantInfoByPolicyIdVar.setInput("policyId", $scope.Variables.selectedPolicyId.dataSet.dataValue);
        $scope.Variables.GetCoApplicantInfoByPolicyIdVar.invoke();


        $scope.Variables.getDriversByPolicyId.setInput("q", query);
        $scope.Variables.getDriversByPolicyId.invoke()






        // $scope.Variables.GetImVehicleList.setInput("q", query);
        // $scope.Variables.GetImVehicleList.invoke();



        var group_name = $scope.Widgets.CreateImApplicantForm1.formWidgets.RequestBody_industry.datavalue;


        // $scope.Variables.getVehicleAssignmntByPolicyId.setInput("q", query);
        //$scope.Variables.getVehicleAssignmntByPolicyId.invoke();





        // var cq = $scope.Variables.GetCarrierQuestions;
        // cq.setInput("policyId", $scope.policyId);
        // cq.invoke();


    };

    $scope.RequestBody_industryChange = function($event, $isolateScope, newVal, oldVal) {


        var widgetName = $event.$scope.name;
        widgetName = widgetName.substr(0, widgetName.length - 11);

        var index = widgetName.charAt(widgetName.length - 1);

        if (index == 0) {
            $scope.Variables.GetDriverOccupationType.setInput("group_name", newVal);
            $scope.Variables.GetDriverOccupationType.invoke();

        }

        if (index == 1) {
            $scope.Variables.GetDriverOccupationType1.setInput("group_name", newVal);
            $scope.Variables.GetDriverOccupationType1.invoke();
        }

        if (index == 2) {

            $scope.Variables.GetDriverOccupationType2.setInput("group_name", newVal);
            $scope.Variables.GetDriverOccupationType2.invoke();
        }
        if (index == 3) {

            $scope.Variables.GetDriverOccupationType3.setInput("group_name", newVal);
            $scope.Variables.GetDriverOccupationType3.invoke();
        }
        if (index == 4) {

            $scope.Variables.GetDriverOccupationType4.setInput("group_name", newVal);
            $scope.Variables.GetDriverOccupationType4.invoke();
        }
        if (index == 5) {
            $scope.Variables.GetDriverOccupationType5.setInput("group_name", newVal);
            $scope.Variables.GetDriverOccupationType5.invoke();
        }

        $timeout(function() {
            var index = widgetName.charAt(widgetName.length - 1);
            if (index == 0) {

                $scope.Widgets.RequestBody_occupation.dataSet = $scope.Variables.GetDriverOccupationType.dataSet.content;
            }
            if (index == 1) {
                $scope.Widgets.RequestBody_occupation1.dataSet = $scope.Variables.GetDriverOccupationType1.dataSet.content;
            }
            if (index == 2) {
                $scope.Widgets.RequestBody_occupation2.dataSet = $scope.Variables.GetDriverOccupationType2.dataSet.content;

            }
            if (index == 3) {
                $scope.Widgets.RequestBody_occupation3.dataSet = $scope.Variables.GetDriverOccupationType3.dataSet.content;

            }
            if (index == 4) {
                $scope.Widgets.RequestBody_occupation4.dataSet = $scope.Variables.GetDriverOccupationType4.dataSet.content;
            }
            if (index == 5) {
                $scope.Widgets.RequestBody_occupation5.dataSet = $scope.Variables.GetDriverOccupationType5.dataSet.content;

            }


        }, 1000)


    };

    $scope.driverActivePage = 1;
    $scope.driveButton1Click = function($event, $isolateScope) {
        debugger
        if ($isolateScope.$element[0].textContent.length == 3) {
            $scope.numDriversValue = 2;
        } else {
            $scope.numDriversValue = 1;
        }
        $scope.Widgets.tabs4.goToTab(1, {});
        $scope.driverActivePage = 1;
    };

    $scope.driveButton2Click = function($event, $isolateScope) {
        $scope.Widgets.tabs4.goToTab(2, {});
        $scope.driverActivePage = 2;
        if ($isolateScope.$element[0].textContent.length == 3) {
            $scope.numDriversValue = 2;
        } else {
            $scope.numDriversValue = 1;
        }
    };

    $scope.driveButton3Click = function($event, $isolateScope) {
        $scope.Widgets.tabs4.goToTab(3, {});
        $scope.driverActivePage = 3;
        if ($isolateScope.$element[0].textContent.length == 3) {
            $scope.numDriversValue = 2;
        } else {
            $scope.numDriversValue = 1;
        }
    };

    $scope.vehicleActivePage = 1;
    $scope.vehButtonNextClick = function($event, $isolateScope) {
        $scope.Widgets.tabs5.next();
        $scope.vehicleActivePage = 2;
        $window.scrollTo(0, 0);
        if ($isolateScope.$element[0].textContent.length == 3) {
            $scope.numDriversValue = 2;
        } else {
            $scope.numDriversValue = 1;
        }
    };

    $scope.vehButtonPrevClick = function($event, $isolateScope) {
        $scope.Widgets.tabs5.previous();
        $scope.vehicleActivePage = 1;
        $window.scrollTo(0, 0);
        if ($isolateScope.$element[0].textContent.length == 3) {
            $scope.numVehiclesValue = 2;
        } else {
            $scope.numVehiclesValue = 1;
        }
    };

    $scope.GetCurrentDateVaronSuccess = function(variable, data, options) {
        // $scope.Widgets.DriverForm0.formWidgets.RequestBody_dateLicensed.maxdate = data.content[0].currentDate;
        // $scope.currentDate = data.content[0].currentDate;
    };

    $scope.GetCourseMinDateVaronSuccess = function(variable, data, options) {

        // $scope.Widgets.DriverForm0.formWidgets.RequestBody_dateLicensed.mindate = data.content[0].courseMinDate;
        // $scope.courseMinDate = data.content[0].courseMinDate;
    };


    $scope.onIncidentSelect = function($event, $isolateScope) {;
        $scope.incidentsButtonClick = false

        $scope.Variables.GetAccidentListByPolicyId.setInput("policyId", $scope.policyId);
        $scope.Variables.GetAccidentListByPolicyId.invoke();

        var accidentQuery = "incidentType='Accident' and policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;
        //$scope.Variables.GetIncidentTypeListInvoke.setInput("q", accidentQuery);
        //$scope.Variables.GetIncidentTypeListInvoke.invoke();

        var violationQuery = "incidentType='Violation' and policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;
        $scope.Variables.GetIncidentTypeListInvoke1.setInput("q", violationQuery);
        $scope.Variables.GetIncidentTypeListInvoke1.invoke();

        //var lossQuery = "incidentType='Comp Loss' and policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;
        //$scope.Variables.GetIncidentTypeListInvoke2.setInput("q", lossQuery);
        //$scope.Variables.GetIncidentTypeListInvoke2.invoke();
        $scope.Variables.GetCompLossListByPolicyId.setInput("policyId", $scope.Variables.selectedPolicyId.dataSet.dataValue);
        $scope.Variables.GetCompLossListByPolicyId.invoke();

        //$scope.controlFlowTab($event, $isolateScope, "incident");
        ;
        $timeout(function() {
            debugger;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.required = true;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.required = true;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.required = true;

            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.required = false;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.required = false;

            if (!$scope.Variables.selectedPolicyId.dataSet.dataValue || $scope.Variables.GetAccidentListByPolicyId.dataSet.length < 1 || $scope.Variables.GetAccidentListByPolicyId.dataSet.numberOfElements < 1) {
                //Do not show Accident, Violation and Comp Loss Headers.
                console.log('Hide accident header');;
                $scope.Widgets.label34_2.show = false;
            } else {
                $scope.Widgets.label34_2.show = true;
            }

            if (!$scope.Variables.selectedPolicyId.dataSet.dataValue || $scope.Variables.GetIncidentTypeListInvoke1.dataSet.length < 1 || $scope.Variables.GetIncidentTypeListInvoke1.dataSet.numberOfElements < 1) {
                //Do not show Accident, Violation and Comp Loss Headers.
                console.log('Hide Violation header');;
                $scope.Widgets.label35_1.show = false;
            } else {
                $scope.Widgets.label35_1.show = true;
            }

            if (!$scope.Variables.selectedPolicyId.dataSet.dataValue || $scope.Variables.GetCompLossListByPolicyId.dataSet.length < 1 || $scope.Variables.GetCompLossListByPolicyId.dataSet.numberOfElements < 1) {
                //Do not show Accident, Violation and Comp Loss Headers.
                console.log('Hide Comp Loss header');;
                $scope.Widgets.label36_2.show = false;
            } else {
                $scope.Widgets.label36_2.show = true;
            }
        }, 1000)

    }
    $scope.coveragesSelect = function($event, $isolateScope) {
        debugger
        $scope.coveragesButtonClick = false

        if ($scope.viewQuery) {
            $scope.Variables.GetImStateSpecificCoverageInvoke.setInput("q", $scope.viewQuery);
            $scope.Variables.GetImStateSpecificCoverageInvoke.invoke();

            $scope.Variables.GetImCoverageByPolicyId.setInput("q", $scope.viewQuery);
            $scope.Variables.GetImCoverageByPolicyId.invoke();

            $scope.Variables.GetImVehicleCoverageInvoke.setInput("q", $scope.viewQuery);
            $scope.Variables.GetImVehicleCoverageInvoke.invoke();

            $scope.Variables.getPolicyVehicles.setInput("q", $scope.viewQuery);
            $scope.Variables.getPolicyVehicles.invoke();
        }






        //$scope.controlFlowTab($event, $isolateScope, "coverage");
        if ($scope.Variables.selectedPolicyId.dataSet.dataValue != null) {
            var query = "policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;
            $scope.Variables.GetImStateSpecificCoverageInvoke.setInput("q", query);
            $scope.Variables.GetImStateSpecificCoverageInvoke.invoke();
        }


        //imaginea
        $timeout(function() {
            debugger
            $scope.setCoverageDrivers();
        }, 3000);


        $timeout(function() {
            for (var k in $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets) {
                if (k.indexOf('CreateImVehicleCoverageForm') != -1) {
                    var formScope = $scope.Widgets.GetVehiclesByPolicyIdList1.Widgets[k];
                    formScope.elScope.ngform = formScope[formScope.name];
                }
            }
        }, 500)

        debugger

        $timeout(function() {
            for (var i = 0; i < $scope.Variables.GetImStateSpecificCoverageInvoke.dataSet.content.length; i++) {
                for (var k in $scope.Widgets.GetStateDropDownValuesList2.Widgets) {
                    if (k.indexOf('StateCoverageForm') > -1) {

                        var index = k.charAt(k.length - 1);
                        if ($scope.Variables.StateCoverageIds.dataSet[index] == undefined || true) {

                            if ($scope.Variables.GetImStateSpecificCoverageInvoke.dataSet.content[i].coverageName == $scope.Widgets.GetStateDropDownValuesList2.Widgets[k].formWidgets.RequestBody_coverageName.datavalue) {
                                $scope.Widgets.GetStateDropDownValuesList2.Widgets[k].formWidgets.coverageValue.datavalue = $scope.Variables.GetImStateSpecificCoverageInvoke.dataSet.content[i].coverageValue
                            }
                        }
                    }
                }
            }

        }, 3000)

        // Toggle Display of Column in Coverages to fill empty spaces of unavailable fileds
        $timeout(function() {
            debugger
            manageCoverageDisplayFields();
        }, 2000);

    };
    $scope.getAddressByPolicyIdonSuccess = function(variable, data, options) {

        if (data.content.length > 0) {
            $scope.Variables.addressId.dataSet.dataValue = data.content[0].addressId;
        }

        // var cq = $scope.Variables.GetCarrierQuestions;
        // var input = $scope.Widgets.CreateImAddressForm1.dataoutput.RequestBody.stateCode ? $scope.Widgets.CreateImAddressForm1.dataoutput.RequestBody.stateCode : "AL";

        // cq.setInput("policyId", $scope.policyId);
        // cq.invoke();
    };

    $scope.getPolicyQuestionsonSuccess = function(variable, data, options) {

        for (var i = 0; i < data.content.length; i++) {
            var questionData = data.content[i];
            var questionMasterId = questionData.questionMasterId;
            var questionAnswerId = questionData.questionAnswerId;
            var answerText = questionData.answerText;
            if (questionAnswerId) {
                $scope.carriersAnswersMap.set(questionMasterId, questionAnswerId);
            } else {
                $scope.carriersAnswersMap.set(questionMasterId, answerText);
                //$scope.carriersAnswersMap.set(questionMasterId, "answerText");
            }

        }

        for (var k in $scope.Widgets) {
            if (k.indexOf('CreateImPolicyQuestionsForm') > -1) {

                //$scope.Widgets[k].formWidgets.Answercontainer.$element.data().$scope.item;
                var questionMasterId = $scope.Widgets[k].dataoutput.RequestBody.questionMasterId;
                var answerTextOut = $scope.Widgets[k].dataoutput.RequestBody.answerText;
                var questionAnswerIdOut = $scope.Widgets[k].dataoutput.RequestBody.questionAnswerId;

                $scope.Widgets[k].dataoutput.RequestBody.answerText = $scope.carriersAnswersMap.get(questionMasterId);
                $scope.Widgets[k].dataoutput.RequestBody.questionAnswerId = $scope.carriersAnswersMap.get(questionMasterId);

            }
        }
    };

    $scope.CopyQuoteProconSuccess = function(variable, data, options) {

        debugger
        $scope.policyId = data.newPolicyId;
        $scope.Variables.selectedPolicyId.dataSet.dataValue = data.newPolicyId;
        $scope.loadAllQuoteData();
        // $scope.Variables.QuoteSubmittedTime.dataSet.dataValue = $scope.computeDateTime($scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime);
        if ($scope.viewQuery) {
            $scope.Variables.getPolicyInfo.setInput("q", $scope.viewQuery);
        } else {
            var q = "policyId = " + $scope.policyId;
            $scope.Variables.getPolicyInfo.setInput("q", q);
        }

        $scope.Variables.getPolicyInfo.invoke({}, function(data) {
            debugger
            $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.ID = $scope.policyId;

            $scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime = new Date().toISOString().slice(0, 19);
            $scope.Variables.getPolicyInfo.dataSet.content[0].submittedStatus = 'Y';

            $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.RequestBody = $scope.Variables.getPolicyInfo.dataSet.content[0]
            $scope.Variables.UpdateImPolicyInfoInvoke.invoke();
        }, function(error) {
            // Error Callback
            console.log("error", error)
        });


        if ($scope.isClientTab) {
            $scope.Widgets.tabs1.goToTab(1);
        } else if ($scope.isEligibilityTab) {
            $scope.Widgets.tabs1.goToTab(2);
        } else if ($scope.isPolicyTab) {
            $scope.Widgets.tabs1.goToTab(3);
        } else if ($scope.isDriverTab || $scope.isDriverDelete) {
            $scope.Widgets.tabs1.goToTab(4);
        } else if ($scope.isVehiclesTab || $scope.isVehicleDelete) {
            $scope.Widgets.tabs1.goToTab(5);
        } else if ($scope.isIncidentsTab) {
            $scope.Widgets.tabs1.goToTab(6);
        } else if ($scope.isCoveragesTab) {
            if ($scope.isQuotesTab) {
                $scope.Widgets.tabs1.goToTab(9);
            } else {
                $scope.Widgets.tabs1.goToTab(7);
            }

        }





        $timeout(function() {
            if ($scope.isClientTab) {
                $scope.updateClientTab();
            } else if ($scope.isEligibilityTab) {
                $scope.updateEligibilityTab();
            } else if ($scope.isPolicyTab) {
                $scope.updatePolicyTab();
            } else if ($scope.isDriverTab) {
                $scope.updateDriverTab();
            } else if ($scope.isVehiclesTab) {
                $scope.updateVehiclesTab();
            } else if ($scope.isIncidentsTab) {
                $scope.updateIncidentsTab();
            } else if ($scope.isCoveragesTab) {
                $scope.updateCoveragesTab();
            } else if ($scope.isCarrierTab) {
                $scope.updateCarrierTab();
            } else if ($scope.isVehicleDelete) {
                $scope.handleVehicleDeletesRequote();
            } else if ($scope.isDriverDelete) {
                $scope.handleDriverDeletesRequote();

            }
        }, 4000);
    };
    $scope.GetCarrierQuestionsonSuccess = function(variable, data, options) {
        for (var i = 0; i <= data.length; i++) {
            var carrData = data[i]
            if (carrData) {

                if (carrData.answerDataType == "DROPDOWN") {
                    var gaq = $scope.Variables.GetAnswersByQuestion123;
                    gaq.setInput("qmi", carrData.questionMasterId);
                    gaq.invoke();
                }
            }
        }
        if ($scope.Variables.RequestFromClientPage.dataSet.dataValue) {
            $scope.policyId = $scope.Variables.selectedPolicyId.dataSet.dataValue;
            var query = "policyId= " + $scope.Variables.selectedPolicyId.dataSet.dataValue;

            $scope.Variables.getPolicyQuestions.setInput("q", query);
            $scope.Variables.getPolicyQuestions.invoke();
        }

    };

    $scope.getApplicantByPolicyIdonSuccess = function(variable, data, options) {
        if (data.content.length > 0) {
            $scope.Variables.applicantId.dataSet.dataValue = data.content[0].applicantId;
            if (data.content[0].gender != "''" && data.content[0].gender != null && data.content[0].gender != undefined && data.content[0].gender != "") {
                $scope.Widgets.CreateImApplicantForm1.formWidgets.gender.datavalue = $scope.Variables.getApplicantByPolicyId.dataSet.content[0].gender;
            }

            $scope.Widgets.CreateImApplicantForm1.dataoutput.RequestBody.gender = $scope.Variables.getApplicantByPolicyId.dataSet.content[0].gender;

            $scope.Widgets.CreateImApplicantForm1.dataoutput.RequestBody.occupation = $scope.Variables.getApplicantByPolicyId.dataSet.content[0].occupation;
            data.content[0].ssn = $scope.formatSSN(data.content[0].ssn);
            data.content[0].dob = $scope.formatDate(data.content[0].dob);

        }
    };

    $scope.getEligibiliityByPolicyIdonSuccess = function(variable, data, options) {;

        if ($scope.Widgets.CreateImEligibilityForm1 == null) {
            return false;
        }

        if (data.content.length > 0) {

            $scope.Variables.eligibilityId.dataSet.dataValue = data.content[0].eligibilityId;


            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer1 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer1.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer1;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer1 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer1;
            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer2 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer2.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer2;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer2 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer2;
            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer3 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer3.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer3;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer3 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer3;

            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer4 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer4.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer4;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer4 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer4;
            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer5 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer5.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer5;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer5 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer5;
            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer7 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer7.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer7;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer7 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer7;

            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer8 != "''" && $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer8 != null) {


                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer8.datavalue = $scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer8.split(",")

                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer8 = $scope.Variables.CreateImEligibilityInvoke.dataBinding.RequestBody.questionAnswer8.split(",")

                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer8.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer8;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer8 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer8;
            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer10 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer10.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer10;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer10 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer10;
            }
            if ($scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer12 != "''") {
                $scope.Widgets.CreateImEligibilityForm1.formWidgets.RequestBody_questionAnswer12.datavalue = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer12;
                $scope.Widgets.CreateImEligibilityForm1.dataoutput.RequestBody.questionAnswer12 = $scope.Variables.getEligibiliityByPolicyId.dataSet.content[0].questionAnswer12;




            }

        }



    };

    $scope.getPolicyInfoonSuccess = function(variable, data, options) {
        $scope.policyId = data.content[0].policyId;

        $scope.Variables.QuoteSubmittedTime.dataSet.dataValue = $scope.computeDateTime($scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime);

    };

    $scope.getPriorPolicyInfoonSuccess = function(variable, data, options) {
        debugger
        if (data.content.length > 0) {
            $scope.Variables.priorPolicyId.dataSet.dataValue = data.content[0].priorPolicyInfoId;
        }
    };

    $scope.getDriversByPolicyIdonSuccess = function(variable, data, options) {
        if ($scope.Widgets.tabs1.activeTab.name === 'driver') {
            $scope.Variables.driversIds.clearData();
            for (var i = 0; i < data.content.length; i++) {
                $scope.Variables.driversIds.addItem(data.content[i].driverId);
                var currentForm = "DriverForm" + i;
                $scope.Widgets[currentForm].formWidgets.RequestBody_gender.datavalue = data.content[i].gender;
                $scope.Widgets[currentForm].dataoutput.RequestBody.gender = data.content[i].gender;

                $scope.Widgets[currentForm].formWidgets.RequestBody_driverTraining.datavalue = data.content[i].driverTraining;
                $scope.Widgets[currentForm].dataoutput.RequestBody.driverTraining = data.content[i].driverTraining;

                $scope.Widgets[currentForm].formWidgets.RequestBody_goodStudent.datavalue = data.content[i].goodStudent;
                $scope.Widgets[currentForm].dataoutput.RequestBody.goodStudent = data.content[i].goodStudent;

                $scope.Widgets[currentForm].formWidgets.RequestBody_matDriver.datavalue = data.content[i].matDriver;
                $scope.Widgets[currentForm].dataoutput.RequestBody.matDriver = data.content[i].matDriver;

                $scope.Widgets[currentForm].formWidgets.RequestBody_student100.datavalue = data.content[i].student100;
                $scope.Widgets[currentForm].dataoutput.RequestBody.student100 = data.content[i].student100;

                $scope.Widgets[currentForm].formWidgets.RequestBody_goodDriver.datavalue = data.content[i].goodDriver;
                $scope.Widgets[currentForm].dataoutput.RequestBody.goodDriver = data.content[i].goodDriver;

            }
            var numDriver = $scope.Variables.getDriversByPolicyId.dataSet.content.length

            debugger;
            $scope.Variables.numDriversList.dataSet.dataValue = numDriver;
            for (var i = numDriver; i < 5; i++) {
                var currentForm = $scope.Widgets["DriverForm" + i];
                if (currentForm.$element.hasClass('ng-dirty') && currentForm.formWidgets.RequestBody_firstname.datavalue != null && currentForm.formWidgets.RequestBody_firstname.datavalue.length > 1) {
                    numDriver = i + 1;
                } else {
                    break;
                }
            }
            $scope.setDriversButton(numDriver);
            $scope.numDriversValue = numDriver;
            $scope.setCoverageDrivers();
        }

    };

    $scope.getVehicleUseByPolicyIdonSuccess = function(variable, data, options) {
        $scope.Variables.vehicleUseIds.clearData();
        $timeout(function() {
            for (var i = 0; i < data.content.length; i++) {
                $scope.Variables.vehicleUseIds.addItem(data.content[i].vehicleUseId);

                var currentForm = "CreateImVehicleUseForm" + i;

                if (data.content[i].newVehicle != "''" && data.content[i].newVehicle != null) {
                    $scope.Widgets[currentForm].formWidgets.RequestBody_newVehicle.datavalue = data.content[i].newVehicle;
                    $scope.Widgets[currentForm].formWidgets.RequestBody_newVehicle.defaultvalue = data.content[i].newVehicle;
                    $scope.Widgets[currentForm].dataoutput.RequestBody.newVehicle = data.content[i].newVehicle;
                }
            }
        }, 2000)
    };

    $scope.GetImVehicleCoverageInvokeonSuccess = function(variable, data, options) {
        $timeout(function() {
            $scope.Variables.VehicleCoverageIds.clearData();
            for (var i = 0; i < data.content.length; i++) {
                $scope.Variables.VehicleCoverageIds.addItem(data.content[i].vehicleCoverageId);
                var formName = "CreateImVehicleCoverageForm" + i;
                console.log("the form name is " + formName);
                debugger

                if ($scope.Widgets[formName] != undefined) {
                    if ($scope.Widgets[formName].formWidgets.RequestBody_comprehensive != undefined) {
                        $scope.Widgets[formName].formWidgets.RequestBody_comprehensive.datavalue = data.content[i].comprehensive;
                    }
                    if ($scope.Widgets[formName].formWidgets.RequestBody_collDeduct != undefined) {
                        $scope.Widgets[formName].formWidgets.RequestBody_collDeduct.datavalue = data.content[i].collDeduct;
                    }
                    if ($scope.Widgets[formName].formWidgets.RequestBody_rentalDeduct != undefined) {
                        $scope.Widgets[formName].formWidgets.RequestBody_rentalDeduct.datavalue = data.content[i].rentalDeduct;
                    }
                    if ($scope.Widgets[formName].formWidgets.RequestBody_towingDeduct != undefined) {
                        $scope.Widgets[formName].formWidgets.RequestBody_towingDeduct.datavalue = data.content[i].towingDeduct;
                    }
                    if ($scope.Widgets[formName].formWidgets.RequestBody_fullGlass != undefined) {
                        $scope.Widgets[formName].formWidgets.RequestBody_fullGlass.datavalue = data.content[i].fullGlass;
                    }
                    if ($scope.Widgets[formName].formWidgets.RequestBody_loanLeseCov != undefined) {
                        $scope.Widgets[formName].formWidgets.RequestBody_loanLeseCov.datavalue = data.content[i].loanLeseCov;
                    }
                }

                var formNamecov = "QuoteVehCovForm" + i;

                if ($scope.Widgets[formNamecov] != undefined) {
                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_comprehensive != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_comprehensive.datavalue = data.content[i].comprehensive;
                    }
                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_collDeduct != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_collDeduct.datavalue = data.content[i].collDeduct;
                    }

                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_policyId != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_policyId.datavalue = data.content[i].policyId;
                    }
                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_rentalDeduct != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_rentalDeduct.datavalue = data.content[i].rentalDeduct;
                    }
                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_towingDeduct != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_towingDeduct.datavalue = data.content[i].towingDeduct;
                    }
                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_fullGlass != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_fullGlass.datavalue = data.content[i].fullGlass;
                    }
                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_loanLeseCov != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_loanLeseCov.datavalue = data.content[i].loanLeseCov;
                    }
                    if ($scope.Widgets[formNamecov].formWidgets.RequestBody_vehicleId != undefined) {
                        $scope.Widgets[formNamecov].formWidgets.RequestBody_vehicleId.datavalue = data.content[i].vehicleId;
                    }
                }
            }

        }, 2000);
    };

    $scope.GetImCoverageByPolicyIdonSuccess = function(variable, data, options) {
        if (data.content.length > 0) {
            $scope.Variables.coverageId.dataSet.dataValue = data.content[0].coverageId
        }
    };

    $scope.GetImStateSpecificCoverageInvokeonSuccess = function(variable, data, options) {
        $scope.Variables.StateCoverageIds.clearData();
        for (var i = 0; i < data.content.length; i++) {
            $scope.Variables.StateCoverageIds.addItem(data.content[i].stateSpecificCoverageId);
        }
    };

    $scope.getVehicleAssignmntByPolicyIdonSuccess = function(variable, data, options) {
        $scope.Variables.VehicleAssignmentIds.clearData();
        for (var i = 0; i < data.content.length; i++) {
            $scope.Variables.VehicleAssignmentIds.addItem(data.content[i].vehicleAssignmentId);
            //add your code here
            var formName = "CreateImVehicleCoverageForm" + i;
            console.log("the form name is " + formName);
            if ($scope.Widgets[formName] && $scope.Widgets[formName].formWidgets.Driver_assignment != undefined) {
                debugger
                $scope.Widgets[formName].formWidgets.Driver_assignment.datavalue = data.content[i].driverId;
            }

        }
    };

    $scope.getQuestionText = function(questionText, mandatoryInd) {
        var astrix = "";
        if (mandatoryInd == 'Y') {
            astrix = "*";
        }
        return questionText + " " + astrix;
    }

    $scope.getQuestionType = function(recordLock, mandatoryInd) {

        var questionType = "";
        if (mandatoryInd == 'Y') {
            questionType = "Mandatory";
        } else if (recordLock == 'Y') {
            questionType = "Readonly";
        }
        return questionType;
    }

    $scope.GetOccupationTypeonSuccess = function(variable, data, options) {
        if (data.content.length > 0) {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.CreateImApplicantForm1.formWidgets.occupation.datavalue = data.content[0].listItemValue;
                $scope.Widgets.CreateImApplicantForm1.formWidgets.occupation.disabled = true
            } else {
                $scope.Widgets.CreateImApplicantForm1.formWidgets.occupation.disabled = false;
            }
        }
    };

    $scope.RequestBody_annualMilesBlur = function($event, $isolateScope) {
        var newVal = $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_annualMiles.datavalue;
        $scope.Variables.imVehFormName.dataSet.dataValue = $isolateScope.name;
        if (newVal < 5000 || newVal > 20000) {
            $scope.Widgets.confirmdialog2.open();
        }
    };

    $scope.RequestBody_annualMiles1Blur = function($event, $isolateScope) {
        var newVal = $scope.Widgets.CreateImVehicleUseForm1.formWidgets.RequestBody_annualMiles1.datavalue;
        $scope.Variables.imVehFormName.dataSet.dataValue = $isolateScope.name;
        if (newVal < 5000 || newVal > 20000) {
            $scope.Widgets.confirmdialog2.open();
        }
    };

    $scope.RequestBody_annualMiles2Blur = function($event, $isolateScope) {
        var newVal = $scope.Widgets.CreateImVehicleUseForm2.formWidgets.RequestBody_annualMiles2.datavalue;
        $scope.Variables.imVehFormName.dataSet.dataValue = $isolateScope.name;
        if (newVal < 5000 || newVal > 20000) {
            $scope.Widgets.confirmdialog2.open();
        }
    };

    $scope.RequestBody_annualMiles3Blur = function($event, $isolateScope) {
        var newVal = $scope.Widgets.CreateImVehicleUseForm3.formWidgets.RequestBody_annualMiles3.datavalue;
        $scope.Variables.imVehFormName.dataSet.dataValue = $isolateScope.name;
        if (newVal < 5000 || newVal > 20000) {
            $scope.Widgets.confirmdialog2.open();
        }
    };

    $scope.deleteDriverButton0Click = function($event, $isolateScope) {
        if ($scope.Variables.getDriversByPolicyId.dataSet.content) {
            if ($scope.Variables.getDriversByPolicyId.dataSet.content.length > 0) {

                if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                    var vehId = $scope.Variables.getDriversByPolicyId.dataSet.content[0].driverId;
                    $scope.Variables.deleteImDriver.setInput("ID", vehId);
                    $scope.Variables.deleteImDriver.invoke();
                } else {
                    $scope.handleDriverDeletesRequote();
                    $scope.isDriverDelete = true;
                    $scope.deleteVehicleIndex = 0;
                    $scope.Widgets.newQuoteConfDialog.open();
                }

            }
        }
    };


    $scope.handleVehicleDeletesRequote = function() {
        $scope.deleteVehicleIndex;
        var vehId = $scope.Variables.getPolicyVehicles.dataSet.content[$scope.deleteVehicleIndex].vehicleId;
        $scope.Variables.deleteVehicle.setInput("ID", vehId);
        $timeout(function() {
            // $scope.Variables.deleteVehicle.invoke();
            $scope.Variables.deleteVehicle.update();
        }, 500);


    }
    $scope.handleDriverDeletesRequote = function() {
        $scope.deleteDriverIndex;
        var driverId = $scope.Variables.getDriversByPolicyId.dataSet.content[$scope.deleteDriverIndex].driverId;
        $scope.Variables.deleteImDriver.setInput("ID", driverId);
        $scope.Variables.deleteImDriver.invoke();
    }


    $scope.deleteDriverButton1Click = function($event, $isolateScope) {

        if ($scope.Variables.getDriversByPolicyId.dataSet.content && $scope.Variables.getDriversByPolicyId.dataSet.content.length > 1) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var driverId = $scope.Variables.getDriversByPolicyId.dataSet.content[1].driverId;
                $scope.Variables.deleteImDriver.setInput("ID", driverId);
                $scope.Variables.deleteImDriver.invoke();
            } else {
                $scope.isDriverDelete = true;
                $scope.deleteDriverIndex = 1;
                $scope.Widgets.newQuoteConfDialog.open();
            }
        } else {
            $scope.setDriversButton(1);
            $scope.Widgets.DriverForm1.show = false;
            var currentForm = $scope.Widgets["DriverForm1"];
            $scope.clearDriverForm(currentForm);
            // $scope.Widgets.DriverForm1.formWidgets.RequestBody_policyId.datavalue = $scope.policyId;
            // $scope.Widgets.DriverForm1.dataoutput.RequestBody.policyId = $scope.policyId;
            // $scope.Widgets.DriverForm1.formWidgets.RequestBody_rated.datavalue = "Rated";
            // $scope.Widgets.DriverForm1.formWidgets.RequestBody_dob.datavalue = "''";
            $scope.Widgets.numDrivers.datavalue = 1;

        }


    };
    $scope.deleteDriverButton2Click = function($event, $isolateScope) {

        if ($scope.Variables.getDriversByPolicyId.dataSet.content && $scope.Variables.getDriversByPolicyId.dataSet.content.length > 2) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var driverId = $scope.Variables.getDriversByPolicyId.dataSet.content[2].driverId;
                $scope.Variables.deleteImDriver.setInput("ID", driverId);
                $scope.Variables.deleteImDriver.invoke();
            } else {
                $scope.isDriverDelete = true;
                $scope.deleteDriverIndex = 2;
                $scope.Widgets.newQuoteConfDialog.open();
            }
        } else {
            $scope.Widgets.DriverForm2.show = false;
            $scope.setDriversButton(2);
            $scope.Widgets.numDrivers.datavalue = 2;
            var currentForm = $scope.Widgets["DriverForm2"];
            $scope.clearDriverForm(currentForm);
        }

    };

    $scope.deleteDriverButton3Click = function($event, $isolateScope) {

        if ($scope.Variables.getDriversByPolicyId.dataSet.content && $scope.Variables.getDriversByPolicyId.dataSet.content.length > 3) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var driverId = $scope.Variables.getDriversByPolicyId.dataSet.content[3].driverId;
                $scope.Variables.deleteImDriver.setInput("ID", driverId);
                $scope.Variables.deleteImDriver.invoke();
            } else {
                $scope.isDriverDelete = true;
                $scope.deleteDriverIndex = 3;
                $scope.Widgets.newQuoteConfDialog.open();
            }
        } else {
            $scope.Widgets.DriverForm3.show = false;
            $scope.setDriversButton(3);
            $scope.Widgets.numDrivers.datavalue = 3;
            var currentForm = $scope.Widgets["DriverForm3"];
            $scope.clearDriverForm(currentForm);
        }


    };

    $scope.deleteDriverButton4Click = function($event, $isolateScope) {
        if ($scope.Variables.getDriversByPolicyId.dataSet.content && $scope.Variables.getDriversByPolicyId.dataSet.content.length > 4) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var driverId = $scope.Variables.getDriversByPolicyId.dataSet.content[4].driverId;
                $scope.Variables.deleteImDriver.setInput("ID", driverId);
                $scope.Variables.deleteImDriver.invoke();
            } else {
                $scope.isDriverDelete = true;
                $scope.deleteDriverIndex = 4;
                $scope.Widgets.newQuoteConfDialog.open();
            }
        } else {
            $scope.Widgets.DriverForm4.show = false;
            $scope.setDriversButton(4);
            $scope.Widgets.numDrivers.datavalue = 4;
            var currentForm = $scope.Widgets["DriverForm4"];
            $scope.clearDriverForm(currentForm);
        }

    };

    $scope.deleteDriverButton5Click = function($event, $isolateScope) {
        debugger;
        if ($scope.Variables.getDriversByPolicyId.dataSet.content && $scope.Variables.getDriversByPolicyId.dataSet.content.length > 5) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var driverId = $scope.Variables.getDriversByPolicyId.dataSet.content[5].driverId;
                $scope.Variables.deleteImDriver.setInput("ID", driverId);
                $scope.Variables.deleteImDriver.invoke();
            } else {
                $scope.isDriverDelete = true;
                $scope.deleteDriverIndex = 5;
                $scope.Widgets.newQuoteConfDialog.open();
            }
        } else {
            $scope.Widgets.DriverForm5.show = false;
            $scope.setDriversButton(5);
            $scope.Widgets.numDrivers.datavalue = 5;
            var currentForm = $scope.Widgets["DriverForm5"];
            $scope.clearDriverForm(currentForm);
        }
    };

    $scope.deleteVehicleButton0Click = function($event, $isolateScope) {

        if ($scope.Variables.getPolicyVehicles.dataSet.content) {
            if ($scope.Variables.getPolicyVehicles.dataSet.content.length > 0) {
                if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                    var vehId = $scope.Variables.getPolicyVehicles.dataSet.content[0].vehicleId;
                    $scope.Variables.deleteVehicle.setInput("ID", vehId);
                    $timeout(function() {
                        // $scope.Variables.deleteVehicle.invoke();
                        $scope.Variables.deleteVehicle.update();
                    }, 500);
                } else {
                    $scope.isVehicleDelete = true;
                    $scope.deleteVehicleIndex = 0;
                    $scope.Widgets.newQuoteConfDialog.open();


                }
            }
        } else {
            var currentForm = $scope.Widgets["VehicleForm0"];
            currentForm.show = false;
            var currentForm1 = $scope.Widgets["CreateImVehicleUseForm0"];
            var currentForm2 = $scope.Widgets["CreateVehicleAddressForm0"];
            currentForm1.show = false;
            currentForm2.show = false;
            $scope.clearVehicleForm(currentForm);
            $scope.clearVehicleUseForm(currentForm1);
            $scope.clearVehicleAddressForm(currentForm2);
            $scope.Widgets.numVehicles.datavalue = 1;
            $scope.setVehiclesButton(1);

        }
    };

    $scope.deleteVehicleButton1Click = function($event, $isolateScope) {

        if ($scope.Variables.getPolicyVehicles.dataSet.content && $scope.Variables.getPolicyVehicles.dataSet.content.length > 1) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var vehId = $scope.Variables.getPolicyVehicles.dataSet.content[1].vehicleId;
                var delVar = $scope.Variables.deleteVehicle1;
                delVar.setInput("ID", vehId);
                $timeout(function() {
                    //$scope.Variables.deleteVehicle.invoke();
                    delVar.update();
                }, 500);
            } else {
                $scope.isVehicleDelete = true;
                $scope.deleteVehicleIndex = 1;
                $scope.Widgets.newQuoteConfDialog.open();

            }
        } else {
            var currentForm = $scope.Widgets["VehicleForm1"];
            currentForm.show = false;
            var currentForm1 = $scope.Widgets["CreateImVehicleUseForm1"];
            var currentForm2 = $scope.Widgets["CreateVehicleAddressForm1"];
            currentForm1.show = false;
            currentForm2.show = false;
            $scope.clearVehicleForm(currentForm);
            $scope.clearVehicleUseForm(currentForm1);
            $scope.clearVehicleAddressForm(currentForm2);
            $scope.Widgets.numVehicles.datavalue = 1;
            $scope.setVehiclesButton(1);
        }

        //$scope.Widgets.VehicleForm1.show = false;
        //$scope.Widgets.CreateImVehicleUseForm1.show = false;
        //$scope.Widgets.numVehicles.datavalue = 1;
        //$scope.setVehiclesButton(1);

    };
    $scope.deleteVehicleButton2Click = function($event, $isolateScope) {

        if ($scope.Variables.getPolicyVehicles.dataSet.content && $scope.Variables.getPolicyVehicles.dataSet.content.length > 2) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var vehId = $scope.Variables.getPolicyVehicles.dataSet.content[2].vehicleId;
                $scope.Variables.deleteVehicle.setInput("ID", vehId);
                $timeout(function() {
                    //$scope.Variables.deleteVehicle.invoke();
                    $scope.Variables.deleteVehicle.update();
                }, 500);
            } else {
                $scope.isVehicleDelete = true;
                $scope.deleteVehicleIndex = 2;
                $scope.Widgets.newQuoteConfDialog.open();

            }
        } else {
            var currentForm = $scope.Widgets["VehicleForm2"];
            currentForm.show = false;
            var currentForm1 = $scope.Widgets["CreateImVehicleUseForm2"];
            currentForm1.show = false;
            var currentForm2 = $scope.Widgets["CreateVehicleAddressForm2"];
            currentForm2.show = false;
            $scope.clearVehicleForm(currentForm);
            $scope.clearVehicleUseForm(currentForm1);
            $scope.Widgets.numVehicles.datavalue = 2;
            $scope.setVehiclesButton(2);

        }
        //$scope.Widgets.VehicleForm2.show = false;
        //$scope.Widgets.CreateImVehicleUseForm2.show = false;
        // $scope.Widgets.numVehicles.datavalue = 2;
        // $scope.setVehiclesButton(2);


    };
    $scope.deleteVehicleButton3Click = function($event, $isolateScope) {

        if ($scope.Variables.getPolicyVehicles.dataSet.content && $scope.Variables.getPolicyVehicles.dataSet.content.length > 3) {
            if (!$scope.Variables.completedNewIteration.dataSet.dataValue) {
                var vehId = $scope.Variables.getPolicyVehicles.dataSet.content[3].vehicleId;
                $scope.Variables.deleteVehicle1.setInput("ID", vehId);
                $timeout(function() {
                    //$scope.Variables.deleteVehicle.invoke();
                    $scope.Variables.deleteVehicle1.update();
                }, 500);
            } else {
                $scope.isVehicleDelete = true;
                $scope.deleteVehicleIndex = 3;
                $scope.Widgets.newQuoteConfDialog.open();

            }
        } else {
            var currentForm = $scope.Widgets["VehicleForm3"];
            currentForm.show = false;
            var currentForm1 = $scope.Widgets["CreateImVehicleUseForm3"];
            currentForm1.show = false;
            var currentForm3 = $scope.Widgets["CreateVehicleAddressForm3"];
            currentForm3.show = false;
            $scope.clearVehicleForm(currentForm);
            $scope.clearVehicleUseForm(currentForm1);
            $scope.Widgets.numVehicles.datavalue = 3;
            $scope.setVehiclesButton(3);
        }
        //$scope.Widgets.VehicleForm3.show = false;
        //$scope.Widgets.CreateImVehicleUseForm3.show = false;
        //$scope.Widgets.numVehicles.datavalue = 3;
        // $scope.setVehiclesButton(3);


    };

    $scope.GetRelationType1onSuccess = function(variable, data, options) {
        var temp = data.content[3];
        data.content[data.numberOfElements - 1] = temp;
        data.content[3] = data.content[data.numberOfElements - 1];
        data.content.pop();
        $scope.Variables.GetRelationType1.dataSet.content = data.content;

        $scope.Widgets.CoApplicant.formWidgets.RequestBody_relation.dataSet = $scope.Variables.GetRelationType1.dataSet;
        $scope.$apply();
    };

    $scope.createResidenceInfoonSuccess = function(variable, data, options) {
        $scope.Variables.residenceInfoId.dataSet.dataValue = data.residenceId;
    };

    $scope.getResidenceInfoByPolicyIdonSuccess = function(variable, data, options) {
        if (data.length > 0 && data.content[0].residenceId != null) {
            $scope.Variables.residenceInfoId.dataSet.dataValue = data.content[0].residenceId;
        }
    };

    $scope.GetStateOrderValuesonSuccess = function(variable, data, options) {


        var data = $scope.Variables.GetStateOrderValues.dataSet.content;
        data.push({
            "listDesc": "nodesc",
            "displayOrder": 1000
        });
        var answer = [];

        for (var i = 0; i < data.length - 1; i++) {

            var temp1 = {
                "listDesc": data[i].listDesc,
                "listItemValue": "",
                "listName": data[i].listName,
                "defaultValue": "",
                "helpText": data[i].helpText,
            };

            var temp = [];
            while (data[i].listDesc == data[i + 1].listDesc) {
                temp.push(data[i]);
                i++;
            }
            temp.push(data[i]);

            for (var x = 0; x < temp.length; x++) {
                for (var y = x; y < temp.length; y++) {
                    if (temp[x].displayOrder > temp[y].displayOrder) {
                        var t = temp[x];
                        temp[x] = temp[y];
                        temp[y] = t;
                    }
                }
            }

            for (var j = 0; j < temp.length; j++) {
                if (temp[j].defaultValueInd == "Y") {
                    temp1.defaultValue = temp[j].listItemValue;
                }
                if (j == temp.length - 1)
                    temp1.listItemValue += temp[j].listItemValue;
                else
                    temp1.listItemValue += temp[j].listItemValue + ",";
            }

            answer.push(temp1);
        }
        $scope.Variables.StateDefaultOrder.dataSet = answer;

    };

    $scope.coverageValueBlur = function($event, $isolateScope, item, currentItemWidgets, idx) {

        if ($scope.Widgets['StateCoverageForm' + idx]['StateCoverageForm' + idx].$invalid) {
            $scope.Widgets['ErrorMessageCoverageValue' + idx].show = true;

        } else {
            $scope.Widgets['ErrorMessageCoverageValue' + idx].show = false;
        }
    };

    $scope.GetCoAppOccupationTypeonSuccess = function(variable, data, options) {
        if (data.content.length > 0) {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.CoApplicant.formWidgets.RequestBody_occupation.datavalue = data.content[0].listItemValue;
                $scope.Widgets.CoApplicant.formWidgets.RequestBody_occupation.disabled = true
            } else {
                $scope.Widgets.CoApplicant.formWidgets.RequestBody_occupation.disabled = false;
            }
        }
    };


    $scope.ssnChange = function($event, $isolateScope, newVal, oldVal) {
        var data = $scope.formatSSN(newVal);
        $isolateScope.datavalue = data
        //maskSSN($isolateScope.$element, data);
        if ($isolateScope.datavalue == $scope.Widgets.CoApplicant.formWidgets.RequestBody_ssn.datavalue) {
            $scope.$root.notifyToUser("Applicant and Co-Applicant cannot have same SSN", "error");
            $isolateScope.datavalue = undefined;
        }
    };



    $scope.RequestBody_ssnChange = function($event, $isolateScope, newVal, oldVal) {
        var data = $scope.formatSSN(newVal);
        $isolateScope.datavalue = data
        //maskSSN($isolateScope.$element, data);
        if ($isolateScope.datavalue == $scope.Widgets.CreateImApplicantForm1.formWidgets.ssn.datavalue) {
            $scope.$root.notifyToUser("Applicant and Co-Applicant cannot have same SSN", "error");
            $isolateScope.datavalue = undefined;
        }
    };

    $scope.getImApplicantonSuccess = function(variable, data, options) {
        // Mask Loaded Data 
        $timeout(function() {
            maskLoadedData();
        }, 5000);

    };

    $scope.button38Click = function($event, $isolateScope) {

        if ($scope.Widgets.checkboxset1.datavalue == undefined) {
            $scope.$root.notifyToUser("You must select which carriers you'd like to resubmit with", "warning");
            return false;
        }
        $scope.requote = true
        $scope.isCoveragesTab = true
        $scope.isQuotesTab = true
        $scope.Variables.SelectedCarrierIds.dataSet = $scope.Widgets.checkboxset1.datavalue;
        var sv = $scope.Variables.CopyQuoteProc
        sv.setInput("policy_id", $scope.Variables.selectedPolicyId.dataSet.dataValue)
        sv.setInput("new_policy_id", 1)
        sv.invoke();
    };

    $scope.getAnswersForStateQuestionsonSuccess = function(variable, data, options) {
        console.log($scope);
        $scope.carrierQAnswersDropdownMap = data;
    };


    $scope.getPolicyVehiclesonSuccess = function(variable, data, options) {
        debugger;

        var addressDetails = $scope.Variables.getAddressByPolicyId.dataSet.content;


        $scope.Variables.vehicleIds.clearData();
        for (var i = 0; i < data.content.length; i++) {
            $scope.Variables.vehicleIds.addItem(data.content[i].vehicleId);
            if (data.content[i].garageAddressId != null) {
                for (var k = 0; k < addressDetails.length; k++) {
                    if (addressDetails[k].addressId == data.content[i].garageAddressId) {
                        $scope.fillVehicleAddressForm(i, addressDetails[k]);
                    }
                }

            }

        }

        var numVehicles = $scope.Variables.getPolicyVehicles.dataSet.content.length;
        $scope.Variables.numVehiclesList.dataSet.dataValue = numVehicles;
        for (var i = 1; i < 3; i++) {
            var currentForm = $scope.Widgets["VehicleForm" + i];
            if (currentForm) {
                if (currentForm.$element.hasClass('ng-dirty')) {
                    numVehicles = i + 1;
                } else {
                    break;
                }
            }
        }

        if ($scope.Widgets.tabs1.activeTab.name === 'vehicle') {
            $scope.setVehiclesButton(numVehicles);
            for (var i = 0; i < numVehicles; i++) {
                var currentForm = $scope.Widgets["VehicleForm" + i];
                if (currentForm && data.content[i]) {
                    $scope.RequestBody_yearChange("", "", "", "", data.content[i].make, data.content[i].model, currentForm)
                }
            }
        }
        $scope.numVehiclesValue = numVehicles;
        // Setting default drivers for all forms
        $scope.setCoverageDrivers();
    };

    $scope.fillVehicleAddressForm = function(index, addressContent) {
        var currentForm = $scope.Widgets["CreateVehicleAddressForm" + index];
        currentForm.formWidgets.RequestBody_address2.datavalue = addressContent.address2;
        currentForm.formWidgets.RequestBody_address1.datavalue = addressContent.address1;
        currentForm.formWidgets.RequestBody_city.datavalue = addressContent.city;
        currentForm.formWidgets.RequestBody_stateCode.datavalue = addressContent.stateCode
        currentForm.formWidgets.RequestBody_county.datavalue = addressContent.county;
        currentForm.formWidgets.RequestBody_zipCode5.datavalue = addressContent.zipCode5;

    }

    $scope.ssnBlur = function($event, $isolateScope) {
        maskSSN($isolateScope.$element, $isolateScope.datavalue);
    };


    $scope.licenseToDriveChange = function($event, $isolateScope, newVal, oldVal) {
        maskDL($isolateScope.$element, newVal);
    };


    $scope.licenseToDriveClick = function($event, $isolateScope) {
        $isolateScope.$element.val($isolateScope.datavalue);
    };


    $scope.licenseToDriveBlur = function($event, $isolateScope) {
        maskDL($isolateScope.$element, $isolateScope.datavalue);
    };


    $scope.RequestBody_ssnClick = function($event, $isolateScope) {
        //$isolateScope.$element.val($isolateScope.datavalue);
    };


    $scope.licenseToDriveChange = function($event, $isolateScope, newVal, oldVal) {
        maskDL($isolateScope.$element, newVal);
    };


    $scope.VehicleForm0Success = function($event, $isolateScope, $data) {;
        if ($scope.Variables.vehicleUseIds.dataSet[0] == undefined) {

            //$data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_vehicleId.datavalue = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm0.dataoutput.RequestBody.vehicleId = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm0.submit();

        } else {
            var vehicleUseId = $scope.Variables.vehicleUseIds.dataSet[0];
            $scope.Variables.updateImVehicleUse.setInput($scope.Widgets.CreateImVehicleUseForm0.dataoutput);
            $scope.Variables.updateImVehicleUse.setInput("ID", vehicleUseId);
            $scope.Variables.updateImVehicleUse.invoke();

        }
    };


    $scope.VehicleForm1Success = function($event, $isolateScope, $data) {;
        if ($scope.Variables.vehicleUseIds.dataSet[1] == undefined) {

            //$data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm1.formWidgets.RequestBody_vehicleId.datavalue = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm1.dataoutput.RequestBody.vehicleId = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm1.submit();

        } else {
            var vehicleUseId = $scope.Variables.vehicleUseIds.dataSet[index];
            $scope.Variables.updateImVehicleUse.setInput($scope.Widgets.CreateImVehicleUseForm1.dataoutput);
            $scope.Variables.updateImVehicleUse.setInput("ID", vehicleUseId);
            $scope.Variables.updateImVehicleUse.invoke();

        }
    };


    $scope.VehicleForm2Success = function($event, $isolateScope, $data) {;
        if ($scope.Variables.vehicleUseIds.dataSet[2] == undefined) {

            //$data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm2.formWidgets.RequestBody_vehicleId.datavalue = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm2.dataoutput.RequestBody.vehicleId = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm2.submit();

        } else {
            var vehicleUseId = $scope.Variables.vehicleUseIds.dataSet[index];
            $scope.Variables.updateImVehicleUse.setInput($scope.Widgets.CreateImVehicleUseForm2.dataoutput);
            $scope.Variables.updateImVehicleUse.setInput("ID", vehicleUseId);
            $scope.Variables.updateImVehicleUse.invoke();

        }
    };



    $scope.VehicleForm3Success = function($event, $isolateScope, $data) {;
        if ($scope.Variables.vehicleUseIds.dataSet[3] == undefined) {

            //$data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm3.formWidgets.RequestBody_vehicleId.datavalue = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm3.dataoutput.RequestBody.vehicleId = $data.vehicleId;
            $scope.Widgets.CreateImVehicleUseForm3.submit();

        } else {
            var vehicleUseId = $scope.Variables.vehicleUseIds.dataSet[index];
            $scope.Variables.updateImVehicleUse.setInput($scope.Widgets.CreateImVehicleUseForm3.dataoutput);
            $scope.Variables.updateImVehicleUse.setInput("ID", vehicleUseId);
            $scope.Variables.updateImVehicleUse.invoke();

        }
    };




    $scope.GetIncidentTypeListInvokeonSuccess = function(variable, data, options) {
        console.log($scope)

    };

    //OnBeforeRender event is executed after varibale returns the data and beforethe form rendering
    $scope.carrierQuestionsFormBeforerender = function($metadata, $isolateScope) {
        //$metadata is the metadata returned from variable
        var formFields = []; //create an empty fields array
        var carrierName = "";
        //loop through the data
        _.forEach($metadata, function(value, index) {
            //For recording first field of each carrier type
            if (value.carrierName != carrierName) {
                $scope.newGroupFields.push({
                    'key': value.questionMasterId,
                    'value': value.carrierName
                });
                carrierName = value.carrierName;
            }
            var field = {
                'name': value.questionMasterId,
                'displayname': value.questionText,
                'type': 'string', //dataType with map is assignedto type
                'defaultValue': $scope.carriersAnswersMap.get(value.questionMasterId) != undefined ? $scope.carriersAnswersMap.get(value.questionMasterId) : value.answerText,
                'show': true,
                'key': value.questionMasterId,
                'class': 'test',
                'disabled': (value.recordLock == 'YES'),
                'required': (value.mandatoryInd == 'YES'),
                'validationMessage': 'Please enter value'
            };

            //Specific properties can be added based on some conditions
            if (value.answerDataType == "DROPDOWN") {
                field.widget = 'select';
                field.validationMessage = 'Please select value';
                field.displayfield = 'answerKey';
                field.datafield = 'questionAnswerId';
                field.defaultValue = $scope.carriersAnswersMap.get(value.questionMasterId) == undefined ? parseInt(value.questionAnswerId) : $scope.carriersAnswersMap.get(value.questionMasterId);
                field.dataset = 'bind:carrierQAnswersDropdownMap[' + value.questionMasterId + ']';
            }
            formFields.push(field); //For the field object

        });
        // Construct Group Labels
        $timeout(function() {
            createCarrierLabels($scope.newGroupFields);
        }, 2000);
        //Return the new metadata constructed, Form will use this to render thefields
        return formFields;
    };

    function createCarrierLabels(groups) {
        //var searchKey="[key="+obj.key+"]"
        _.forEach(groups, function(obj) {
            if ($("label:contains(" + obj.value + "    )").length > 0) {
                console.log(obj.value);
                $("label:contains(" + obj.value + ")").remove();
            }
            $("div").find("[key=" + obj.key + "]").prepend("<label class='carrier-group-headercls'>" + obj.value + "    " + "</label>");

        });
    }

    function getOptionAnwerText(questionId, answerId) {
        var answer;
        _.forEach($scope.carrierQAnswersDropdownMap[questionId], function(obj) {
            if (obj.questionAnswerId == answerId) {
                answer = obj.answerValue;
            }
        });
        return answer;
    }

    $scope.carrierQuestionsFormSubmit = function($event, $isolateScope, $formData) {
        var carrierData = [];
        //var $formData = $scope.Widgets.carrierQuestionsForm.dataoutput;
        _.forEach(Object.keys($formData), function(value) {
            var question = _.forEach($scope.Variables.svGetCarrierQuestions.dataSet, function(obj) {
                if (obj.questionMasterId == value) {
                    carrierData.push({
                        'questionMasterId': parseInt(value),
                        'questionAnswerId': parseInt($formData[value]),
                        'answerText': (obj.answerDataType == "TEXT") ? $formData[value] : getOptionAnwerText(value, $formData[value]),
                        'questionText': obj.questionText
                    })
                }
            });
        });
        $scope.carrierTabData = carrierData;;
        //$scope.Variables.savePolicyQuestions.setInput("policyId", $scope.policyId);



        var forms = [];


        // for (var k in $scope.Widgets.GetCarrierQuestionsList1.Widgets) {
        //     if (k.indexOf('CreateImPolicyQuestionsForm') > -1) {
        //         forms.push(k);
        //     }
        // }

        if (!$scope.validateCarriersTab()) {
            return false;
        }
        forms.push("carrierQuestionsForm");
        var isPageDirty = $scope.checkIfPageDirty(forms);
        if ($scope.Variables.completedNewIteration.dataSet.dataValue && isPageDirty) {
            $scope.isCarrierTab = true;
            $scope.Widgets.newQuoteConfDialog.open();
        } else {
            $scope.updateCarrierTab();
        }
        $scope.Variables.PusherCarrier.dataSet = [];
        $scope.Widgets.tabs1.next();
        if ($scope.viewQuery) {
            $scope.Variables.getPolicyInfo.setInput("q", $scope.viewQuery);
        } else {
            var q = "policyId = " + $scope.policyId;
            $scope.Variables.getPolicyInfo.setInput("q", q);
        }

        $scope.Variables.getPolicyInfo.invoke({}, function(data) {
            $scope.Variables.QuoteSubmittedTime.dataSet.dataValue = $scope.computeDateTime(data.content[0].submittedTime);
            $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.RequestBody = $scope.Variables.getPolicyInfo.dataSet.content[0]
            $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.RequestBody.insertTimestamp = $scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime;
            if ($scope.viewQuery) {
                $scope.Variables.UpdateImPolicyInfoInvoke.setInput("q", $scope.viewQuery);
            } else {
                var q = "policyId = " + $scope.policyId;
                $scope.Variables.UpdateImPolicyInfoInvoke.setInput("q", q);
            }

            $scope.Variables.UpdateImPolicyInfoInvoke.invoke();
        }, function(error) {
            // Error Callback
            console.log("error", error)
        })




    };

    $scope.computeDateTime = function(data) {
        // if (data == null) {
        //     return "";
        // }
        var date1 = new Date(new Date().toISOString().slice(0, 19));
        var startTime = new Date(data);
        startTime = new Date(startTime.getTime() - (startTime.getTimezoneOffset() * 60000));
        var hrs = startTime.getHours();
        var min = startTime.getMinutes();
        var ampm = hrs >= 12 ? 'PM' : 'AM';
        hrs = hrs % 12;
        hrs = hrs ? hrs : 12;
        min = min < 10 ? '0' + min : min;
        var dateCust = (startTime.getMonth() + 1) + "/" + startTime.getDate() + "/" + startTime.getFullYear() + "  " + hrs + ':' + min + ' ' + ampm;
        return dateCust;
    }


    $scope.validateCarriersTab = function() {
        var isDataMissed = false;
        $scope.missingDataTab = 0;
        $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.ID = $scope.policyId;

        if ($scope.Variables.getPolicyInfo.dataSet.content) {

            $scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime = new Date().toISOString().slice(0, 19);
            $scope.Variables.getPolicyInfo.dataSet.content[0].submittedStatus = 'Y';
            $scope.Variables.QuoteSubmittedTime.dataSet.dataValue = $scope.computeDateTime($scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime);

            $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.RequestBody = $scope.Variables.getPolicyInfo.dataSet.content[0]
            $scope.Variables.UpdateImPolicyInfoInvoke.dataBinding.RequestBody.insertTimestamp = $scope.Variables.getPolicyInfo.dataSet.content[0].submittedTime;
            $scope.Variables.UpdateImPolicyInfoInvoke.invoke();
        }

        var tabIssues = false;

        var carrierTabIncomplete = false;

        if ($scope.Widgets.CreateImApplicantForm1 && ($scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == true)) {
            $scope.Widgets.tabpane1._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Client</span><!-- ngIf: badgevalue --></div></a>';
            isDataMissed = true;
            if ($scope.missingDataTab == 0) {
                $scope.missingDataTab = 1;
            }

        }

        if ($scope.Widgets.CreateImEligibilityForm1 && ($scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == true)) {
            $scope.Widgets.tabpane2._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Eligibility</span><!-- ngIf: badgevalue --></div></a>';
            isDataMissed = true;
            if ($scope.missingDataTab == 0) {
                $scope.missingDataTab = 2;
            }
        }

        if ($scope.Widgets.CreateImPriorPolicyInfoForm1 && ($scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == true)) {
            $scope.Widgets.policy._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Policy</span><!-- ngIf: badgevalue --></div></a>';
            isDataMissed = true;
            if ($scope.missingDataTab == 0) {
                $scope.missingDataTab = 3;
            }
        }

        if ($scope.Widgets.DriverForm0 && ($scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == "true" || $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == true)) {
            $scope.Widgets.driver._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Driver</span><!-- ngIf: badgevalue --></div></a>';
            isDataMissed = true;
            if ($scope.missingDataTab == 0) {
                $scope.missingDataTab = 4;
            }

        }

        if ($scope.Widgets.CreateVehicleAddressForm0 && ($scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == "true" || $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == true)) {
            $scope.Widgets.vehicle._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Vehicles</span><!-- ngIf: badgevalue --></div></a>';
            isDataMissed = true;
            if ($scope.missingDataTab == 0) {
                $scope.missingDataTab = 5;
            }

        }

        if ($scope.Widgets.CreateImCoverageForm1 && ($scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == true)) {
            $scope.Widgets.coverages._headerElement[0].innerHTML = '<a href="javascript:void(0);" role="button" tabindex="0"><div class="tab-heading"><!-- ngIf: paneicon --> <span ng-bind="title" class="ng-binding" style="color:#FF0000">Coverages</span><!-- ngIf: badgevalue --></div></a>';
            isDataMissed = true;
            if ($scope.missingDataTab == 0) {
                $scope.missingDataTab = 7;
            }
        }


        // if ($scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == "true" || $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == "true" || $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == "true" || $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == "true" || carrierTabIncomplete || $scope.Widgets.CreateImApplicantForm1.formWidgets.clientTabIncomplete.datavalue == true || $scope.Widgets.CreateImEligibilityForm1.formWidgets.eligibilityTabIncomplete.datavalue == true || $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.policyTabIncomplete.datavalue == true || $scope.Widgets.DriverForm0.formWidgets.driverTabIncomplete.datavalue == true || $scope.Widgets.CreateVehicleAddressForm0.formWidgets.vehicleTabIncomplete.datavalue == true || $scope.Widgets.CreateImCoverageForm1.formWidgets.coveragesTabIncomplete.datavalue == true) {
        if (isDataMissed) {
            $scope.Widgets.requiredInfoMissingDialog.open();
            return false;
        }
        return true;
    }

    $scope.tabpaneCarrierSelect = function($event, $isolateScope) {
        $scope.Variables.getAnswersForStateQuestions.invoke();
        if ($scope.viewQuery) {
            $scope.Variables.getPolicyQuestions.setInput("q", $scope.viewQuery);
            $scope.Variables.getPolicyQuestions.invoke();
        }

    };



    $scope.cancelClick = function($event, $isolateScope) {

    };





    $scope.savePolicyQuestionsonSuccess = function(variable, data, options) {;
        // $scope.Widgets.tabs1.next();
        var sv = $scope.Variables.GetCarrierXml;
        sv.setInput("policyId", $scope.policyId);
        sv.invoke();


    };


    $scope.tabpane2Select = function($event, $isolateScope) {;
        $scope.eligibilityButtonClick = false
        //$scope.controlFlowTab($event, $isolateScope, "eligibility");
        //eligselect
        //reload issue
        //sessionStorage.setItem('currentPolicyId', $scope.policyId);
        if ($scope.viewQuery) {
            $scope.Variables.getEligibiliityByPolicyId.setInput("q", $scope.viewQuery);
            $scope.Variables.getEligibiliityByPolicyId.invoke();
        }

    };


    $scope.policySelect = function($event, $isolateScope) {
        $scope.policyButtonClick = false
        $window.scrollTo(0, 0);
        if ($scope.viewQuery) {
            $scope.Variables.getPolicyInfo.setInput("q", $scope.viewQuery);
            $scope.Variables.getPolicyInfo.invoke({}, function(data) {
                $scope.policyFillCheckAuthValue(data);
            }, function(error) {
                // Error Callback
                console.log("error", error)
            });
            $scope.Variables.getPriorPolicyInfo.setInput("q", $scope.viewQuery);
            $scope.Variables.getPriorPolicyInfo.invoke({}, function(data) {

                $timeout(function() {
                    $scope.priorPolicyFilldata(data);

                }, 2000);

            }, function(error) {
                // Error Callback
                console.log("error", error)
            });
        }
    };


    $scope.priorPolicyFilldata = function(data) {
        if (data.content != undefined) {
            debugger
            var priorPolContent = data.content[0];
            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_policyId.datavalue = priorPolContent.policyId;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.policyId = priorPolContent.policyId
            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorCarrierType.datavalue = priorPolContent.priorCarrierType;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue = priorPolContent.expiration;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorLiabilityLimit.datavalue = priorPolContent.priorLiabilityLimit;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.form_field_Reason_No_Prior.datavalue = priorPolContent.reasonNoPrior;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorPolicyPremium.datavalue = priorPolContent.priorPolicyPremium;


            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_priorLiabilityLimit.datavalue = priorPolContent.priorLiabilityLimit;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithPriorCarrier.datavalue = priorPolContent.yearsWithPriorCarrier != null ? priorPolContent.yearsWithPriorCarrier.toString() : "0";

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithPriorCarrier.datavalue = priorPolContent.monthsWithPriorCarrier != null ? priorPolContent.monthsWithPriorCarrier.toString() : "0";

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue = priorPolContent.yearsWithContinuousCoverage != null ? priorPolContent.yearsWithContinuousCoverage.toString() : "0";

            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithContinuousCoverage.datavalue = priorPolContent.monthsWithContinuousCoverage != null ? priorPolContent.monthsWithContinuousCoverage.toString() : "0";

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.priorCarrierType = priorPolContent.priorCarrierType

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.expiration = priorPolContent.expiration;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.reasonNoPrior = priorPolContent.reasonNoPrior;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.priorPolicyPremium = priorPolContent.priorPolicyPremium;
            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.priorLiabilityLimit = priorPolContent.priorLiabilityLimit;

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.yearsWithPriorCarrier = priorPolContent.yearsWithPriorCarrier != null ? priorPolContent.yearsWithPriorCarrier.toString() : "0";

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.monthsWithPriorCarrier = priorPolContent.monthsWithPriorCarrier != null ? priorPolContent.monthsWithPriorCarrier.toString() : "0";

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.yearsWithContinuousCoverage = priorPolContent.yearsWithContinuousCoverage != null ? priorPolContent.yearsWithContinuousCoverage.toString() : "0";

            $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.monthsWithContinuousCoverage = priorPolContent.monthsWithContinuousCoverage != null ? priorPolContent.monthsWithContinuousCoverage.toString() : "0";
        }
    };

    $scope.policyFillCheckAuthValue = function(data) {
        if (data.content != undefined) {
            $timeout(function() {
                if (data.content[0].creditCheckAuth != "''" && data.content[0].creditCheckAuth != null && data.content[0].creditCheckAuth != undefined && data.content[0].creditCheckAuth != "") {
                    $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_creditCheckAuth.datavalue = data.content[0].creditCheckAuth;
                    $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_creditCheckAuth.defaultvalue = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_creditCheckAuth.datavalue;
                    $scope.Widgets.UpdateImPolicyInfoForm1.dataoutput.RequestBody.creditCheckAuth = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_creditCheckAuth.datavalue;

                }
                debugger
                if (data.content[0].multiPolicyDiscount != "''" && data.content[0].multiPolicyDiscount != null) {
                    $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.multipolicy_discount.datavalue = data.content[0].multiPolicyDiscount;
                    $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.multipolicy_discount.defaultvalue = data.content[0].multiPolicyDiscount;
                    $scope.Widgets.UpdateImPolicyInfoForm1.dataoutput.RequestBody.multiPolicyDiscount = data.content[0].multiPolicyDiscount

                    $scope.Variables.UpdateImPolicyInfoInvoke.dataSet.multiPolicyDiscount = data.content[0].multiPolicyDiscount;

                }
            }, 1000);
        }

    }



    $scope.GetAccidentListByPolicyIdonSuccess = function(variable, data, options) {;
        console.log('GetAccidentListByPolicyIdonSuccess');

        if (!$scope.Variables.selectedPolicyId.dataSet.dataValue || $scope.Variables.GetAccidentListByPolicyId.dataSet.length < 1 || $scope.Variables.GetAccidentListByPolicyId.dataSet.numberOfElements < 1) {
            //Do not show Accident, Violation and Comp Loss Headers.
            console.log('Hide accident header');;
            $scope.Widgets.label34_2.show = false;
        } else {
            $scope.Widgets.label34_2.show = true;
        }


    };


    $scope.GetIncidentTypeListInvoke1onSuccess = function(variable, data, options) {

        if (!$scope.Variables.selectedPolicyId.dataSet.dataValue || $scope.Variables.GetIncidentTypeListInvoke1.dataSet.length < 1 || $scope.Variables.GetIncidentTypeListInvoke1.dataSet.numberOfElements < 1) {
            //Do not show Accident, Violation and Comp Loss Headers.
            console.log('Hide accident header');;
            $scope.Widgets.label35_1.show = false;
        } else {
            $scope.Widgets.label35_1.show = true;
        }
    };


    $scope.GetIncidentTypeListInvoke2onSuccess = function(variable, data, options) {

        if (!$scope.Variables.selectedPolicyId.dataSet.dataValue || $scope.Variables.GetIncidentTypeListInvoke2.dataSet.length < 1 || $scope.Variables.GetIncidentTypeListInvoke2.dataSet.numberOfElements < 1) {
            //Do not show Accident, Violation and Comp Loss Headers.
            console.log('Hide accident header');;
            $scope.Widgets.label36_2.show = false;
        } else {
            $scope.Widgets.label36_2.show = true;
        }
    };


    /*    $scope.updateImVehicleonSuccess = function(variable, data, options) {
            var formName = variable.dataBinding.formName
            var count = formName.charAt(formName.length - 1);
            var vehicleUseId = $scope.Variables.vehicleUseIds.dataSet[count];
            $scope.Variables.updateImVehicleUse.setInput($scope.Widgets["CreateImVehicleUseForm" + count].dataoutput);
            $scope.Variables.updateImVehicleUse.setInput("ID", vehicleUseId);
            $scope.Variables.updateImVehicleUse.invoke();
        };*/


    $scope.firstNameFocus = function($event, $isolateScope) {
        $isolateScope.placeholder = "";
    };


    $scope.firstNameBlur = function($event, $isolateScope) {
        $isolateScope.placeholder = "First Name";
    };


    $scope.GetCompLossListByPolicyIdonSuccess = function(variable, data, options) {

        if (!$scope.Variables.selectedPolicyId.dataSet.dataValue || $scope.Variables.GetCompLossListByPolicyId.dataSet.length < 1 || $scope.Variables.GetCompLossListByPolicyId.dataSet.numberOfElements < 1) {
            //Do not show Accident, Violation and Comp Loss Headers.
            console.log('Hide CompLoss header');;
            $scope.Widgets.label36_2.show = false;
        } else {
            $scope.Widgets.label36_2.show = true;
        }
    };



    $scope.ssnClick = function($event, $isolateScope) {
        // $isolateScope.$element.val($isolateScope.datavalue);
    };



    $scope.RequestBody_effectiveBlur = function($event, $isolateScope, newVal, oldVal) {
        $isolateScope.datavalue = $scope.formatDate($event.currentTarget.value);


        var sel = $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue;
        var selectedDate = new Date($scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue);
        var todaysDate = new Date();
        todaysDate.setMilliseconds(0);
        todaysDate.setMinutes(0);
        todaysDate.setSeconds(0);
        todaysDate.setHours(0);
        $scope.nextYearDate = new Date().setFullYear(todaysDate.getFullYear() + 1);
        $scope.prevYearDate = new Date().setFullYear(todaysDate.getFullYear() - 1);
        if (selectedDate < todaysDate || selectedDate > $scope.nextYearDate) {
            $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue = "";

            $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.validationmessage = "Invalid Policy Effective Date. Date can not be more than 1 year in the future or in past."
        }


    };


    $scope.RequestBody_expirationChange = function($event, $isolateScope, newVal, oldVal) {;
        // $isolateScope.datavalue = $scope.formatDate($event.currentTarget.value);

        // var sel = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue;
        // var selectedDate = new Date($scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue);
        // var todaysDate = new Date();
        // todaysDate.setMilliseconds(0);
        // todaysDate.setMinutes(0);
        // todaysDate.setSeconds(0);
        // todaysDate.setHours(0);
        // $scope.nextYearDate = new Date().setFullYear(todaysDate.getFullYear() + 1);
        // $scope.prevYearDate = new Date().setFullYear(todaysDate.getFullYear() - 1);
        // if (selectedDate != null && (selectedDate < $scope.prevYearDate || selectedDate > $scope.nextYearDate)) {
        //     $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue = "";
        //     $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.validationmessage = " Invalid Expiration Date. Date can not be more than 1 year in the future or more than 1 year in past."
        // } else {
        //     if (selectedDate != null && newVal.length == 10) {
        //         $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue = newVal;
        //         $scope.Widgets.UpdateImPolicyInfoForm1.dataoutput.RequestBody.effective = newVal;
        //     }

        // }
    };


    $scope.RequestBody_effectiveClick = function($event, $isolateScope) {
        $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.validationmessage = "Enter a valid date MM/DD/YYYY."

    };

    $scope.RequestBody_expirationClick = function($event, $isolateScope) {
        $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.validationmessage = " Enter a valid date MM/DD/YYYY."
    };




    $scope.RequestBody_ssnBlur = function($event, $isolateScope) {
        maskSSN($isolateScope.$element, $isolateScope.datavalue);
    };

    $scope.deleteImDriveronSuccess = function(variable, data, options) {
        var q = "policyId = " + $scope.policyId;
        $scope.Variables.getDriversByPolicyId.setInput("q", q);
        $scope.Variables.getDriversByPolicyId.invoke();

    };


    $scope.deleteVehicleonSuccess = function(variable, data, options) {
        var q = "policyId = " + $scope.policyId;
        $scope.Variables.getPolicyVehicles.setInput("q", q);
        $scope.Variables.getPolicyVehicles.invoke();


        $scope.Variables.getVehicleUseByPolicyId.setInput("q", q);
        $scope.Variables.getVehicleUseByPolicyId.invoke();

        $scope.Variables.GetImVehicleCoverageInvoke.setInput("q", q);
        $scope.Variables.GetImVehicleCoverageInvoke.invoke();

        $scope.Variables.getVehicleAssignmntByPolicyId.setInput("q", q);
        $scope.Variables.getVehicleAssignmntByPolicyId.invoke();




    };



    // $scope.RequestBody_expirationChange = function($event, $isolateScope, newVal, oldVal) {
    //     $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue = newVal;
    // };


    $scope.picture4Click = function($event, $isolateScope, item, currentItemWidgets) {
        _.forEach($scope.Variables.getCarrierList.dataSet.content, function(obj) {
            if (obj.carrierId == item.carrierId) {
                window.open(obj.url, '_blank');
            }
        })
    };


    $scope.GetCoAppOccupationTypeonBeforeUpdate = function(variable, inputData, options) {;

        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }

    };


    $scope.GetOccupationTypeonBeforeUpdate = function(variable, inputData, options) {;
        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }
    };


    $scope.GetDriverOccupationTypeonBeforeUpdate = function(variable, inputData, options) {
        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }
    };




    $scope.GetDriverOccupationTypeonSuccess = function(variable, data, options) {

        if (data.content.length > 0 && $scope.Widgets.tabs1.activeTab.name === 'driver') {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.DriverForm0.formWidgets.RequestBody_occupation.datavalue = data.content[0].listItemValue;
                $scope.Widgets.DriverForm0.formWidgets.RequestBody_occupation.disabled = true
            } else {
                $scope.Widgets.DriverForm0.formWidgets.RequestBody_occupation.disabled = false;
            }
        }
    };


    $scope.GetDriverOccupationType1onSuccess = function(variable, data, options) {

        if (data.content.length > 0) {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.DriverForm1.formWidgets.RequestBody_occupation1.datavalue = data.content[0].listItemValue;
                $scope.Widgets.DriverForm1.formWidgets.RequestBody_occupation1.disabled = true
            } else {
                $scope.Widgets.DriverForm1.formWidgets.RequestBody_occupation1.disabled = false;
            }
        }
    };


    $scope.GetDriverOccupationType2onSuccess = function(variable, data, options) {

        if (data.content.length > 0) {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.DriverForm2.formWidgets.RequestBody_occupation1.datavalue = data.content[0].listItemValue;
                $scope.Widgets.DriverForm2.formWidgets.RequestBody_occupation1.disabled = true
            } else {
                $scope.Widgets.DriverForm2.formWidgets.RequestBody_occupation1.disabled = false;
            }
        }
    };


    $scope.GetDriverOccupationType3onSuccess = function(variable, data, options) {

        if (data.content.length > 0) {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.DriverForm3.formWidgets.RequestBody_occupation1.datavalue = data.content[0].listItemValue;
                $scope.Widgets.DriverForm3.formWidgets.RequestBody_occupation1.disabled = true
            } else {
                $scope.Widgets.DriverForm3.formWidgets.RequestBody_occupation1.disabled = false;
            }
        }
    };


    $scope.GetDriverOccupationType4onSuccess = function(variable, data, options) {

        if (data.content.length > 0) {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.DriverForm4.formWidgets.RequestBody_occupation1.datavalue = data.content[0].listItemValue;
                $scope.Widgets.DriverForm4.formWidgets.RequestBody_occupation1.disabled = true
            } else {
                $scope.Widgets.DriverForm4.formWidgets.RequestBody_occupation1.disabled = false;
            }
        }
    };


    $scope.GetDriverOccupationType5onSuccess = function(variable, data, options) {

        if (data.content.length > 0) {
            if (specialIndustries.includes(data.content[0].listItemValue)) {
                $scope.Widgets.DriverForm5.formWidgets.RequestBody_occupation1.datavalue = data.content[0].listItemValue;
                $scope.Widgets.DriverForm5.formWidgets.RequestBody_occupation1.disabled = true
            } else {
                $scope.Widgets.DriverForm5.formWidgets.RequestBody_occupation1.disabled = false;
            }
        }
    };


    $scope.GetCoAppOccupationTypeonError = function(variable, data, options) {
        console.log("GetCoAppOccupationTypeonError data" + data);
    };

    $scope.GetOccupationTypeonError = function(variable, data, options) {
        console.log("GetCoAppOccupationTypeonError data" + data);
    };


    $scope.GetDriverOccupationType2onBeforeUpdate = function(variable, inputData, options) {
        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }
    };


    $scope.GetDriverOccupationType4onBeforeUpdate = function(variable, inputData, options) {
        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }
    };


    $scope.GetDriverOccupationType1onBeforeUpdate = function(variable, inputData, options) {
        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }
    };


    $scope.GetDriverOccupationType3onBeforeUpdate = function(variable, inputData, options) {
        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }
    };


    $scope.GetDriverOccupationType5onBeforeUpdate = function(variable, inputData, options) {
        if (inputData.group_name == undefined || inputData.group_name.length < 2) {
            return false;
        }
    };


    $scope.deleteVehicleonError = function(variable, data, options) {
        console.log("deleteVehicleonErrorfailed" + data);
    };


    $scope.label45Click = function($event, $isolateScope, item, currentItemWidgets) {
        $scope.picture4Click($event, $isolateScope, item, currentItemWidgets);
    };


    $scope.deleteVehicle1onSuccess = function(variable, data, options) {
        debugger;
        var q = "policyId = " + $scope.policyId;
        $scope.Variables.getPolicyVehicles.setInput("q", q);
        $scope.Variables.getPolicyVehicles.invoke();


        $scope.Variables.getVehicleUseByPolicyId.setInput("q", q);
        $scope.Variables.getVehicleUseByPolicyId.invoke();

        $scope.Variables.GetImVehicleCoverageInvoke.setInput("q", q);
        $scope.Variables.GetImVehicleCoverageInvoke.invoke();

        $scope.Variables.getVehicleAssignmntByPolicyId.setInput("q", q);
        $scope.Variables.getVehicleAssignmntByPolicyId.invoke();
        $scope.$apply();

    };


    $scope.getScriptingMessageForTabsonSuccess = function(variable, data, options) {

        $scope.tabsDataMap = new Map();

        for (var i = 0; i < data.content.length; i++) {

            var tabName = data.content[i].pageName;
            var tabData = data.content[i].script;

            $scope.tabsDataMap.set(tabName, tabData)
        }

    };

    $scope.tabpane11Select = function($event, $isolateScope) {
        var sv = $scope.Variables.GetQuotesInvoke
        sv.setInput("first_name", $scope.Widgets.CreateImApplicantForm1.dataoutput.RequestBody.firstName)
        sv.setInput("last_name", $scope.Widgets.CreateImApplicantForm1.dataoutput.RequestBody.lastName)
        sv.invoke();
        $scope.Variables.goToPage_QuoteSubmission.invoke()

    };


    $scope.updateImDriveronSuccess = function(variable, data, options) {
        //imaginea
        // var q = "policyId = " + data.policyId;
        // $scope.Variables.getDriversByPolicyId.setInput("q", q);
        // $scope.Variables.getDriversByPolicyId.invoke();

    };




    $scope.dobBlur = function($event, $isolateScope) {
        var date1 = new Date($scope.Widgets.CreateImApplicantForm1.formWidgets.dob.datavalue)
        var date2 = new Date();
        if (date1 > date2) {
            $scope.Widgets.CreateImApplicantForm1.formWidgets.dob.datavalue = null
            $scope.Widgets.CreateImApplicantForm1.dataoutput.RequestBody.dob = null
            $scope.Widgets.CreateImApplicantForm1.formWidgets.dob.validationmessage = " Enter a date which should not exceeds present date."
        }

        $isolateScope.datavalue = $scope.formatDate($isolateScope.datavalue);
    };


    $scope.RequestBody_dobBlur = function($event, $isolateScope) {
        var date1 = new Date($scope.Widgets.CoApplicant.formWidgets.RequestBody_dob.datavalue)
        var date2 = new Date();
        if (date1 > date2) {
            $scope.Widgets.CoApplicant.formWidgets.RequestBody_dob.datavalue = null

            $scope.Widgets.CoApplicant.dataoutput.RequestBody.dob = null
            $scope.Widgets.CoApplicant.formWidgets.RequestBody_dob.validationmessage = " Enter a date which should not exceeds present date."
        }
        $isolateScope.datavalue = $scope.formatDate($isolateScope.datavalue);
    };

    $scope.Driver_dobBlur = function($event, $isolateScope) {
        debugger
        var currentForm = "";
        var formName = $isolateScope.$parent.ngform.$name;
        for (var k in $scope.Widgets) {
            if (k == formName) {
                currentForm = $scope.Widgets[k];
                break;
            }
        }
        $scope.currentForm = currentForm;
        var date1 = new Date($scope.currentForm.formWidgets.RequestBody_dob.datavalue)
        var date2 = new Date();
        if (date1 > date2) {
            $scope.currentForm.formWidgets.RequestBody_dob.datavalue = null
            $scope.currentForm.dataoutput.RequestBody.dob = null

            $scope.currentForm.formWidgets.RequestBody_dob.validationmessage = " Enter a date which should not exceeds present date."

        }
        $isolateScope.datavalue = $scope.formatDate($isolateScope.datavalue);

    };


    $scope.RequestBody_comprehensiveChange = function($event, $isolateScope, item, currentItemWidgets, newVal, oldVal) {
        debugger;
        var formName = '';
        for (var k in currentItemWidgets) {
            if (k.indexOf('CreateImVehicleCoverageForm') != -1) {
                formName = k;
                break;
            }
        }
        var currentForm = $scope.Widgets[formName];
        if (newVal == "No Coverage") {
            if (currentForm) {
                currentForm.formWidgets.RequestBody_collDeduct.datavalue = "No Coverage";
                currentForm.dataoutput.RequestBody.collDeduct = "No Coverage";
                currentForm.formWidgets.RequestBody_collDeduct.disabled = true;

                currentForm.formWidgets.RequestBody_rentalDeduct.disabled = true;
                currentForm.formWidgets.RequestBody_rentalDeduct.datavalue = "No Coverage";
                currentForm.dataoutput.RequestBody.rentalDeduct = "No Coverage";

                currentForm.formWidgets.RequestBody_towingDeduct.disabled = true;
                currentForm.formWidgets.RequestBody_towingDeduct.datavalue = "No Coverage";
                currentForm.dataoutput.RequestBody.towingDeduct = "No Coverage";

                currentForm.formWidgets.RequestBody_fullGlass.disabled = true;
                currentForm.formWidgets.RequestBody_fullGlass.datavalue = "No";
                currentForm.dataoutput.RequestBody.fullGlass = "No";
                currentForm.formWidgets.RequestBody_loanLeseCov.disabled = true;


            }
        } else {
            currentForm.formWidgets.RequestBody_collDeduct.disabled = false;
            currentForm.formWidgets.RequestBody_rentalDeduct.disabled = false;
            currentForm.formWidgets.RequestBody_towingDeduct.disabled = false;
            currentForm.formWidgets.RequestBody_fullGlass.disabled = false;
            currentForm.formWidgets.RequestBody_loanLeseCov.disabled = false;
        }
    };


    $scope.RequestBody_biChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue = newVal
        $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.um = newVal;
        $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue = newVal
        $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.uim = newVal;
        if (newVal.includes("CSL")) {
            debugger
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.datavalue = "No coverage"
            $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.pd = "No coverage"
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.disabled = true
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.disabled = true
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.disabled = true
        } else {
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.datavalue = ""
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.disabled = false
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.disabled = false
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.disabled = false
        }

    };

    $scope.RequestBody_uimChange = function($event, $isolateScope, newVal, oldVal) {
        console.log($scope)
        debugger

        var biValue = $scope.Widgets.RequestBody_bi_formWidget.datavalue;
        $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue = newVal
        $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.um = newVal;

        if (biValue.includes("/") && newVal.includes("CSL")) {
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue = null
            $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.uim = null
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.validationmessage = " UM/UIM cannot be CSL if BI limit is split."
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue = null
            $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.um = null
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.validationmessage = " UM/UIM cannot be CSL if BI limit is split."
        } else

        if (biValue.includes("/") && newVal.includes("/")) {
            var newbi = biValue.split("/");
            var splituim = newVal.split("/");
            if (parseInt(newbi[0]) < parseInt(splituim[0]) || parseInt(newbi[1]) < parseInt(splituim[1])) {
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue = null
                $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.uim = null
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.validationmessage = " UM/UIM cannot be higher than BI limit."
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue = null
                $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.um = null
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.validationmessage = " UM/UIM cannot be higher than BI limit."
            }

        }
    };



    $scope.RequestBody_umChange = function($event, $isolateScope, newVal, oldVal) {
        console.log($scope)


        var biValue = $scope.Widgets.RequestBody_bi_formWidget.datavalue;
        $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue = newVal
        $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.uim = newVal;

        if (biValue.includes("/") && newVal.includes("CSL")) {
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue = null
            $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.uim = null
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.validationmessage = " UM/UIM cannot be CSL if BI limit is split."

            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue = null
            $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.um = null
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.validationmessage = " UM/UIM cannot be CSL if BI limit is split."

        } else

        if (biValue.includes("/") && newVal.includes("/")) {
            var newbi = biValue.split("/");
            var splituim = newVal.split("/");
            if (parseInt(newbi[0]) < parseInt(splituim[0]) || parseInt(newbi[1]) < parseInt(splituim[1])) {
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.datavalue = null
                $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.uim = null
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_uim.validationmessage = " UM/UIM cannot be higher than BI limit."
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.datavalue = null
                $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.um = null
                $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_um.validationmessage = " UM/UIM cannot be higher than BI limit."
            }

        }

    };


    $scope.UpdateCoApplicantonSuccess = function(variable, data, options) {


        var requestBody = $scope.Variables.getDriversByPolicyId.dataSet.content[1]
        requestBody.driverLicenseNumber = data.licenseToDrive;
        requestBody.dob = data.dob;
        requestBody.firstName = data.firstName;
        requestBody.gender = data.gender;
        requestBody.industry = data.industry;
        requestBody.lastName = data.lastName;
        requestBody.relation = data.relation;
        requestBody.maritalStatus = data.maritalStatus;
        requestBody.occupation = data.occupation;
        requestBody.policyId = data.policyId;


        $scope.Variables.updateImDriver.setInput("RequestBody", requestBody);
        $scope.Variables.updateImDriver.setInput("ID", requestBody.driverId);
        $scope.Variables.updateImDriver.invoke();

        $scope.Variables.coApplicantId.dataSet.dataValue = data.applicantId;
        $scope.Variables.GetDriverOccupationType1.invoke();
        // Mask Loaded Data 
        $timeout(function() {
            maskLoadedData();
        }, 1000);



    };


    $scope.tabpane1Select = function($event, $isolateScope) {
        debugger
        $scope.clientButtonClick = false
        // Mask Loaded Data 
        $timeout(function() {
            maskLoadedData();
        }, 3000);

    };


    $scope.RequestBody_yearChange = function($event, $isolateScope, newVal, oldVal, makeVin = null, modelVin = null, viewCurrForm = null) {
        debugger
        var yearForm = "";
        if (viewCurrForm == null) {
            debugger
            var yearFormName = $isolateScope.$parent.ngform.$name;
            for (var k in $scope.Widgets) {
                if (k == yearFormName) {
                    yearForm = $scope.Widgets[k];
                    break;
                }
            }
        } else {
            yearForm = viewCurrForm
        }

        $scope.yearForm = yearForm;
        var yearCall = $scope.Variables.GetYearMake;

        yearCall.setInput("YEAR", yearForm.formWidgets.RequestBody_year.datavalue)
        yearCall.setInput("username", "stg")
        yearCall.setInput("password", "stg")
        yearCall.invoke({}, function(data) {
            if (data.response.makeList.make != undefined) {
                debugger
                if (yearForm.name == "VehicleForm0") {
                    $scope.Variables.makeList.dataSet = data.response.makeList.make
                } else if (yearForm.name == "VehicleForm1") {
                    $scope.Variables.makeList1.dataSet = data.response.makeList.make
                } else if (yearForm.name == "VehicleForm2") {
                    $scope.Variables.makeList2.dataSet = data.response.makeList.make
                } else if (yearForm.name == "VehicleForm3") {
                    $scope.Variables.makeList3.dataSet = data.response.makeList.make
                }
                if (makeVin != null) {
                    yearForm.formWidgets.RequestBody_make.datavalue = makeVin
                    $scope.RequestBody_makeChange($event, $isolateScope, "", "", modelVin, yearForm)

                }
            }
        }, function(error) {
            // Error Callback
        });
    };


    $scope.RequestBody_makeChange = function($event, $isolateScope, newVal, oldVal, modelVin = null, viewCurrForm = null) {

        var makeForm = "";


        if (viewCurrForm == null) {
            debugger
            var makeFormName = $isolateScope.$parent.ngform.$name;
            for (var k in $scope.Widgets) {
                if (k == makeFormName) {
                    makeForm = $scope.Widgets[k];
                    break;
                }
            }
        } else {
            makeForm = viewCurrForm
        }


        $scope.makeForm = makeForm;
        var yearMakeCall = $scope.Variables.GetMakeYearModel;

        yearMakeCall.setInput("YEAR", makeForm.formWidgets.RequestBody_year.datavalue)
        yearMakeCall.setInput("MAKE", makeForm.formWidgets.RequestBody_make.datavalue)

        yearMakeCall.setInput("username", "stg")
        yearMakeCall.setInput("password", "stg")
        yearMakeCall.invoke({}, function(data) {
            debugger
            if (data.Model != undefined) {
                if (makeForm.name == "VehicleForm0") {
                    $scope.Variables.modelList.dataSet = data.Model
                } else if (makeForm.name == "VehicleForm1") {
                    $scope.Variables.modelList1.dataSet = data.Model
                } else if (makeForm.name == "VehicleForm2") {
                    $scope.Variables.modelList2.dataSet = data.Model
                } else if (makeForm.name == "VehicleForm3") {
                    $scope.Variables.modelList3.dataSet = data.Model
                }
                if (modelVin != null) {
                    makeForm.formWidgets.RequestBody_model.datavalue = modelVin
                    //$scope.RequestBody_modelChange($event, $isolateScope, "", "", modelVin, makeForm)

                }
            }
        }, function(error) {
            // Error Callback
        });

    };


    $scope.RequestBody_industry0Change = function($event, $isolateScope, newVal, oldVal) {
        // Make Applicant as as driver1
        $scope.Widgets.CreateImApplicantForm1.formWidgets.RequestBody_industry.datavalue = newVal;
    };


    $scope.RequestBody_occupationChange = function($event, $isolateScope, newVal, oldVal) {
        if ($scope.Widgets.DriverForm1) {
            $scope.Widgets.DriverForm1.formWidgets.RequestBody_occupation1.datavalue = newVal;
        }



    };


    $scope.RequestBody_industry1Change = function($event, $isolateScope, newVal, oldVal) {
        // Make Co-Applicant as as driver2
        $scope.Widgets.CoApplicant.formWidgets.RequestBody_industry.datavalue = newVal;
        // Fetch relavent Occupations
        $scope.Variables.GetDriverOccupationType1.setInput("group_name", newVal);
        $scope.Variables.GetDriverOccupationType1.invoke();
    };


    $scope.RequestBody_occupation1Change = function($event, $isolateScope, newVal, oldVal) {
        // Make Co-Applicant as as driver2
        $scope.Widgets.CoApplicant.formWidgets.RequestBody_occupation.datavalue = newVal;
    };


    $scope.occupationChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.DriverForm0.formWidgets.RequestBody_occupation.datavalue = newVal;
    };


    $scope.RequestBody_occupationChange1 = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.CreateImApplicantForm1.formWidgets.occupation.datavalue = newVal;
    };




    $scope.RequestBody_priorCarrierTypeChange = function($event, $isolateScope, newVal, oldVal) {
        if (newVal == "No Prior Insurance") {
            $scope.Widgets.RequestBody_priorLiabilityLimit_formWidget.class = '';
        }

    };





    $scope.CreateImApplicantInvokeonBeforeUpdate = function(variable, inputData, options) {

        inputData.RequestBody.dob = $scope.formatDate(inputData.RequestBody.dob);
    };


    $scope.CreateImApplicantInvoke1onBeforeUpdate = function(variable, inputData, options) {
        inputData.RequestBody.dob = $scope.formatDate(inputData.RequestBody.dob);

    };


    $scope.RequestBody_monthsWithPriorCarrierChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithContinuousCoverage.datavalue = newVal
        $scope.Widgets.CreateImPriorPolicyInfoForm1.datouput.RequestBody.monthsWithContinuousCoverage = newVal

    };


    $scope.RequestBody_yearsWithPriorCarrierChange = function($event, $isolateScope, newVal, oldVal) {
        $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue = newVal
        $scope.Widgets.CreateImPriorPolicyInfoForm1.datouput.RequestBody.yearsWithContinuousCoverage = newVal

    };


    $scope.validateContinuous = function() {
        debugger
        $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue = null;



        $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithContinuousCoverage.datavalue = null;

        $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.validationmessage = "  Prior Coverage is never longer than Continuous Coverage ";
        $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.yearsWithContinuousCoverage = null;
        $scope.Widgets.CreateImPriorPolicyInfoForm1.dataoutput.RequestBody.monthsWithContinuousCoverage = null;
    }

    $scope.calculateContinuousCoverage = function() {
        debugger
        var priorYear = $scope.Widgets.RequestBody_yearsWithPriorCarrier_formWidget.datavalue;
        var priorMonth = $scope.Widgets.RequestBody_monthsWithPriorCarrier_formWidget.datavalue;

        var covYrDatavalue = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_yearsWithContinuousCoverage.datavalue

        var covMtDatavalue = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithContinuousCoverage.datavalue

        if (parseInt(priorYear) == parseInt(covYrDatavalue) && (covMtDatavalue == null || covMtDatavalue == undefined)) {
            return false
        } else if (parseInt(priorMonth) > parseInt($scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_monthsWithContinuousCoverage.datavalue)) {
            $scope.validateContinuous();
        }
        var coverageYear = covYrDatavalue == null ? 0 : covYrDatavalue
        var coverageMonth = covMtDatavalue == null ? 0 : covMtDatavalue
        var priorValue = parseInt(priorYear) * 12 + parseInt(priorMonth);
        var coverageValue = parseInt(coverageYear) * 12 + parseInt(coverageMonth);
        if (priorValue > coverageValue) {
            $scope.validateContinuous();
        }
    };

    $scope.RequestBody_expirationBlur = function($event, $isolateScope) {
        $isolateScope.datavalue = $scope.formatDate($event.currentTarget.value);

        var newVal = $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue;
        var selectedDate = new Date($scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue);
        var todaysDate = new Date();
        todaysDate.setMilliseconds(0);
        todaysDate.setMinutes(0);
        todaysDate.setSeconds(0);
        todaysDate.setHours(0);
        $scope.nextYearDate = new Date().setFullYear(todaysDate.getFullYear() + 1);
        $scope.prevYearDate = new Date().setFullYear(todaysDate.getFullYear() - 1);
        if (selectedDate != null && (selectedDate < $scope.prevYearDate || selectedDate > $scope.nextYearDate)) {
            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.datavalue = "";
            $scope.Widgets.CreateImPriorPolicyInfoForm1.formWidgets.RequestBody_expiration.validationmessage = " Invalid Expiration Date. Date can not be more than 1 year in the future or more than 1 year in past."
        } else {
            if (selectedDate != null && newVal.length == 10) {
                $scope.Widgets.UpdateImPolicyInfoForm1.formWidgets.RequestBody_effective.datavalue = newVal;
                $scope.Widgets.UpdateImPolicyInfoForm1.dataoutput.RequestBody.effective = newVal;
            }

        }

    };

    $scope.tabs1Change = function($event, $isolateScope, newPaneIndex, oldPaneIndex) {
        console.log($scope)
        debugger
        if (oldPaneIndex == 0) {
            if ($scope.clientButtonClick == false) {
                $scope.button3Click();
            }
        } else if (oldPaneIndex == 1) {
            if ($scope.eligibilityButtonClick == false) {
                $scope.button20Click();
            }
        } else if (oldPaneIndex == 2) {
            if ($scope.policyButtonClick == false) {
                $scope.button6Click();
            }
        } else if (oldPaneIndex == 3) {
            if ($scope.driverButtonClick == false) {
                $scope.button31Click();
            }
        } else if (oldPaneIndex == 4) {
            if ($scope.vehicleButtonClick == false) {
                $scope.button21Click();
            }
        } else if (oldPaneIndex == 5) {
            if ($scope.incidentsButtonClick == false) {
                $scope.button17_1Click();
            }
        } else if (oldPaneIndex == 6) {
            if ($scope.coveragesButtonClick == false) {
                $scope.button29Click();
            }
        }

    };


    $scope.RequestBody_pdChange = function($event, $isolateScope, newVal, oldVal) {
        debugger
        var biValue = $scope.Widgets.RequestBody_bi_formWidget.datavalue;
        var pdValue = $scope.Widgets.RequestBody_pd_formWidget.datavalue;

        if (biValue.includes("/") && pdValue == "No coverage") {
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.datavalue = null
            $scope.Widgets.CreateImCoverageForm1.dataoutput.RequestBody.pd = null
            $scope.Widgets.CreateImCoverageForm1.formWidgets.RequestBody_pd.validationmessage = "No Coverage cannot be selected for PD with split BI limits."

        };

    };


    $scope.calculateAnnualMiles = function($event, $isolateScope, newVal, oldVal) {
        debugger

        var formName = $isolateScope.$parent.ngform.$name;
        var currentForm = $scope.Widgets[formName];

        var daysPerWeek = currentForm.formWidgets.RequestBody_daysPerWk.datavalue;

        var oneWayMiles = currentForm.formWidgets.RequestBody_oneWayMiles.datavalue;
        var useage = currentForm.formWidgets.RequestBody_useage.datavalue;

        if (daysPerWeek != null && oneWayMiles != null && (useage == "To/From Work" || useage == "To/From School")) {

            var annualMiles = (daysPerWeek * oneWayMiles * 2 * 52) + 5000;

            if (formName.indexOf('1') != -1) {
                $scope.Widgets.CreateImVehicleUseForm1.formWidgets.RequestBody_annualMiles1.datavalue = annualMiles;
                $scope.Widgets.CreateImVehicleUseForm1.dataoutput.RequestBody.annualMiles = annualMiles;
            } else if (formName.indexOf('2') != -1) {
                $scope.Widgets.CreateImVehicleUseForm2.formWidgets.RequestBody_annualMiles2.datavalue = annualMiles;
                $scope.Widgets.CreateImVehicleUseForm2.dataoutput.RequestBody.annualMiles = annualMiles;
            } else if (formName.indexOf('3') != -1) {
                $scope.Widgets.CreateImVehicleUseForm3.formWidgets.RequestBody_annualMiles3.datavalue = annualMiles;
                $scope.Widgets.CreateImVehicleUseForm3.dataoutput.RequestBody.annualMiles = annualMiles;
            } else {
                $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_annualMiles.datavalue = annualMiles;
                $scope.Widgets.CreateImVehicleUseForm0.dataoutput.RequestBody.annualMiles = annualMiles;
            }

        }



    };


    $scope.GetCountyonSuccess = function(variable, data, options) {
        console.log($scope)
        debugger
        if (data.content.length > 0) {
            $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_county.datavalue = data.content[0].county
            $scope.Widgets.CreateImAddressForm1.dataoutput.RequestBody.county = data.content[0].county
        } else {
            $scope.Widgets.CreateImAddressForm1.formWidgets.RequestBody_county.datavalue = undefined
            $scope.Widgets.CreateImAddressForm1.dataoutput.RequestBody.county = null
        }

    };


    $scope.getPolicyVehiclesonError = function(variable, data, options) {
        debugger;

    };


    $scope.GetMakeYearModelonBeforeUpdate = function(variable, inputData, options) {
        if (inputData.YEAR == undefined || inputData.MAKE == undefined) {
            return false;
        }

    };

    $scope.GetYearMakeonBeforeUpdate = function(variable, inputData, options) {

        if (inputData.YEAR == undefined) {
            return false;
        }

    };

}]);

Application.$controller("newQuoteConfDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.newQuoteConfDialogOk = function($event, $isolateScope) {
            $scope.checkIterationProcess();
        };

        $scope.newQuoteConfDialogCancel = function($event, $isolateScope) {
            $scope.isClientTab = false
            $scope.isEligibilityTab = false
            $scope.isPolicyTab = false
            $scope.isDriverTab = false
            $scope.isVehiclesTab = false
            $scope.isIncidentsTab = false
            $scope.isCoveragesTab = false
            $scope.isCarrierTab = false
            $scope.isVehicleDelete = false
            $scope.isDriverDelete = false
            $scope.Variables.isCopyProcUnderProgress.dataSet.dataValue = false;
        };



    }
]);
Application.$controller("confirmIncidentsController", ["$scope", "$window",
    function($scope, $window) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.button20_1Click = function($event, $isolateScope) {
            $scope.Widgets.confirmIncidents.close();
            $scope.Widgets.tabs1.next();
            $window.scrollTo(0, 0);
        };

        $scope.button21_1Click = function($event, $isolateScope) {
            $scope.Widgets.confirmIncidents.close();
            $scope.Widgets.tabs1.goToTab(7);
            $window.scrollTo(0, 0);
        };

    }
]);

Application.$controller("courseDateAlertController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.courseDateAlertOk = function($event, $isolateScope) {;
            $scope.Widgets[$scope.currentDriverForm].formWidgets.RequestBody_dateLicensed.datavalue = "";
            $scope.Widgets.courseDateAlert.close();
        };

    }
]);

Application.$controller("dialog2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("confirmdialog2Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.confirmdialog2Ok = function($event, $isolateScope) {

        };

        $scope.confirmdialog2Cancel = function($event, $isolateScope) {
            if ($scope.Variables.imVehFormName.dataSet.dataValue.indexOf('1') != -1) {
                $scope.Widgets.CreateImVehicleUseForm1.formWidgets.RequestBody_annualMiles1.datavalue = "";
            } else if ($scope.Variables.imVehFormName.dataSet.dataValue.indexOf('2') != -1) {
                $scope.Widgets.CreateImVehicleUseForm2.formWidgets.RequestBody_annualMiles2.datavalue = "";
            } else if ($scope.Variables.imVehFormName.dataSet.dataValue.indexOf('3') != -1) {
                $scope.Widgets.CreateImVehicleUseForm3.formWidgets.RequestBody_annualMiles3.datavalue = "";
            } else {
                $scope.Widgets.CreateImVehicleUseForm0.formWidgets.RequestBody_annualMiles.datavalue = "";
            }
        };
    }
]);

Application.$controller("requiredInfoMissingDialogController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.requiredInfoMissingDialogOk = function($event, $isolateScope) {
            if ($scope.missingDataTab != 0)
                $scope.Widgets.tabs1.goToTab($scope.missingDataTab, {});

        };

        $scope.requiredInfoMissingDialogCancel = function($event, $isolateScope) {


        };
    }
]);

Application.$controller("confirmIncidentsSaveController", ["$scope", "$window",
    function($scope, $window) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.incident_returnClick = function($event, $isolateScope) {;

            //Save the Incident 
            $scope.Widgets.confirmIncidentsSave.close();
            /*
            if ($scope.Variables.completedNewIteration.dataSet.dataValue) {
                $scope.isIncidentsTab = true;
                $scope.Widgets.newQuoteConfDialog.open();
            } else {
                $scope.updateIncidentsTab();
            }
            $scope.Widgets.tabs1.previous();*/
            $window.scrollTo(0, 0);
        };

        $scope.incident_deleteClick = function($event, $isolateScope) {;
            //Clear the incident form and move to the next/previous tab.
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.form_field132_formWidget.$viewValue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = "";

            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setPristine();
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setUntouched();

            $scope.Widgets.RequestBody_incidentType.datavalue = 'Accident';
            $scope.Widgets.confirmIncidentsSave.close();
            $scope.Widgets.tabs1.previous();
            $window.scrollTo(0, 0);
        };

    }
]);

Application.$controller("confirmIncidentsSave1Controller", ["$scope", "$window",
    function($scope, $window) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.incident_return1Click = function($event, $isolateScope) {;

            //Save the Incident 
            $scope.Widgets.confirmIncidentsSave1.close();

            /*if ($scope.Variables.completedNewIteration.dataSet.dataValue) {
                $scope.isIncidentsTab = true;
                $scope.Widgets.newQuoteConfDialog.open();
            } else {
                $scope.updateIncidentsTab();
            }
            $scope.Widgets.tabs1.next();*/
            $window.scrollTo(0, 0);
        };

        $scope.incident_delete1Click = function($event, $isolateScope) {;
            //Clear the incident form and move to the next/previous tab.

            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.form_field132_formWidget.$viewValue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = "";

            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setPristine();
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setUntouched();

            $scope.Widgets.RequestBody_incidentType.datavalue = 'Accident';

            $scope.Widgets.confirmIncidentsSave1.close();
            $scope.Widgets.tabs1.next();
            $window.scrollTo(0, 0);
        };

    }
]);

Application.$controller("confirmIncidentsSave2Controller", ["$scope", "$window",
    function($scope, $window) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.incident_save2Click = function($event, $isolateScope) {;

            //Save the Incident 
            $scope.Widgets.confirmIncidentsSave2.close();

            if ($scope.Variables.completedNewIteration.dataSet.dataValue) {
                $scope.isIncidentsTab = true;
                $scope.Widgets.newQuoteConfDialog.open();
            } else {
                $scope.updateIncidentsTab();
            }
            $scope.Widgets.tabs1.previous();
            $window.scrollTo(0, 0);
        };

        $scope.incident_delete2Click = function($event, $isolateScope) {;
            //Clear the incident form and move to the next/previous tab.

            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.form_field132_formWidget.$viewValue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = "";

            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setPristine();
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setUntouched();

            $scope.Widgets.RequestBody_incidentType.datavalue = 'Accident';

            $scope.Widgets.confirmIncidentsSave2.close();
            $scope.Widgets.tabs1.previous();
            $window.scrollTo(0, 0);
        };

    }
]);

Application.$controller("confirmIncidentsSave3Controller", ["$scope", "$window",
    function($scope, $window) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.incident_save3Click = function($event, $isolateScope) {;

            //Save the Incident 
            $scope.Widgets.confirmIncidentsSave3.close();

            if ($scope.Variables.completedNewIteration.dataSet.dataValue) {
                $scope.isIncidentsTab = true;
                $scope.Widgets.newQuoteConfDialog.open();
            } else {
                $scope.updateIncidentsTab();
            }
            $scope.Widgets.tabs1.next();
            $window.scrollTo(0, 0);

        };

        $scope.incident_delete3Click = function($event, $isolateScope) {;
            //Clear the incident form and move to the next/previous tab.

            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.form_field132_formWidget.$viewValue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.RequestBody_driverId.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.AccidentDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field128.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field129.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.ViolationDesc.datavalue = "";
            $scope.Widgets.CreateImIncidentForm1.formWidgets.LossDesc.datavalue = "";

            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setPristine();
            $scope.Widgets.CreateImIncidentForm1.CreateImIncidentForm1.$setUntouched();

            $scope.Widgets.RequestBody_incidentType.datavalue = 'Accident';

            $scope.Widgets.confirmIncidentsSave3.close();
            $scope.Widgets.tabs1.next();
            $window.scrollTo(0, 0);
        };

    }
]);

Application.$controller("incidentDateAlertController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.incidentDateAlertOk = function($event, $isolateScope) {;;
            $scope.Widgets.CreateImIncidentForm1.formWidgets.form_field132.datavalue = "";
            $scope.Widgets.incidentDateAlert.close();
        };

    }
]);

Application.$controller("confirmIncidents Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("notificationalertdialog Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("dialog2 Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("newQuoteConfDialog Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("confirmdialog2 Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("requiredInfoMissingDialog Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("courseDateAlert Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("confirmIncidentsSave Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("confirmIncidentsSave1 Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("confirmIncidentsSave2 Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("confirmIncidentsSave3 Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);

Application.$controller("incidentDateAlert Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);