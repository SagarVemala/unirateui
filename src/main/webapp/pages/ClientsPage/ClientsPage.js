Application.$controller("ClientsPagePageController", ["$scope", "$rootScope", function($scope, $rootScope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
        $rootScope.linkClicked = false;
    };

    $scope.clientTabClick = function($event, $scope, $data) {}


    $scope.getClientDetailsByNameTable1Click = function($event, $isolateScope, $rowData) {
        debugger;
        var polId = $rowData.policyId;
        $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;
        if ($event.target.textContent.indexOf("Quote Tab") != -1) {
            $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;
            $scope.Variables.requestForQuotesTab.dataSet.dataValue = true;
            $scope.Variables.RequestFromClientPage.dataSet.dataValue = false;
            $scope.Variables.requestFromSearchPage.dataSet.dataValue = true;
            $scope.Variables.goToPage_AutoQuote.invoke()
        } else if ($event.target.textContent.indexOf("Client Tab") != -1) {
            $rootScope.isForClientPageSearch = true;
            $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;
            $scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
            $scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
            if ($rowData.status == 'Y') {
                $scope.Variables.completedNewIteration.dataSet.dataValue = true;

            } else {
                $scope.Variables.completedNewIteration.dataSet.dataValue = false;
            }
            $scope.getClientDataForPolicy(polId);
            $scope.Variables.goToPage_AutoQuote.invoke()
        } else {
            if ($rootScope.linkClicked) {
                $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;
                $scope.Variables.requestForQuotesTab.dataSet.dataValue = false;
                $scope.Variables.RequestFromClientPage.dataSet.dataValue = true;
                $scope.Variables.goToPage_AutoQuote.invoke()
                $rootScope.linkClicked = false
            }

        }

    };

    $scope.getClientDataForPolicy = function(polId) {
        // var q = "policyId = " + polId;
        // $scope.Variables.getApplicantByPolicyId.setInput("q", q);
        // $scope.Variables.getApplicantByPolicyId.invoke();

        //$rootScope.loadAllDataForPolicy(polId);
        $scope.Variables.selectedPolicyId.dataSet.dataValue = polId;



        // $rootScope.$emit('loadQuoteData');
    };



}]);


Application.$controller("getClientDetailsByNameTable1Controller", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }

]);
