Application.$controller("CarrierManagementPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.TableFetchStatesForCarrierClick = function($event, $isolateScope, $rowData) {
        if ($rowData.carrierStates != undefined && $rowData.carrierStates != "" && $rowData.carrierStates != null) {
            $scope.Variables.stvEditStates.dataSet = $rowData.carrierStates.split(',');
        }
    };


    $scope.svFetchStatesForSelectedCarrieronSuccess = function(variable, data, options) {
        $scope.Variables.stvEditStates.dataSet = [];
        if (data.content[0].carrierStates != undefined && data.content[0].carrierStates != "" && data.content[0].carrierStates != null) {
            $scope.Variables.stvEditStates.dataSet = data.content[0].carrierStates.split(',');
        }
    };


    $scope.svFetchStatesForCarrieronSuccess = function(variable, data, options) {
        var activeCarrier = [];
        var inActiveCarrier = [];
        _.forEach(data.content, function(obj) {
            if (obj.carrierStates == undefined || obj.carrierStates == "" || obj.carrierStates == null) {
                inActiveCarrier.push(obj);
            } else {
                activeCarrier.push(obj);
            }
        });
        activeCarrier = _.sortBy(activeCarrier, 'carrierName');
        inActiveCarrier = _.sortBy(inActiveCarrier, 'carrierName');
        data.content = activeCarrier;
        _.forEach(inActiveCarrier, function(obj) {
            data.content.push(obj);
        });

    };

}]);


Application.$controller("dialogEditCarrierController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;

        $scope.buttonSaveClick = function($event, $isolateScope) {
            $scope.Variables.svUpdateStatesForCarrier.dataBinding.RequestBody = {
                "states": $scope.Widgets.checkboxsetStates.datavalue.join(),
                "carrier": $scope.Widgets.selectCarrier.datavalue
            };
            $scope.Variables.svUpdateStatesForCarrier.invoke();
        };


        $scope.selectCarrierChange = function($event, $isolateScope, newVal, oldVal) {
            $scope.Variables.svFetchStatesForSelectedCarrier.setInput({
                'carrier': newVal
            });
            $scope.Variables.svFetchStatesForSelectedCarrier.invoke();
        };


        $scope.dialogEditCarrierClose = function($event, $isolateScope) {
            $scope.Variables.stvEditStates.dataSet = [];
        };

    }
]);


Application.$controller("TableFetchStatesForCarrierController", ["$scope",
    function($scope) {
        "use strict";
        $scope.ctrlScope = $scope;
    }
]);
