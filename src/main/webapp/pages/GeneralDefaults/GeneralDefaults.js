Application.$controller("GeneralDefaultsPageController", ["$scope", function($scope) {
    "use strict";
    $scope.listValues = [];
    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.checkboxLockChange = function($event, $isolateScope, item, currentItemWidgets, newVal, oldVal) {
        $scope.Variables.svUpdateDropdownListLock.dataBinding.RequestBody = {
            "defaultLock": newVal,
            "dropDownListId": item.dropdownListId
        };
        $scope.Variables.svUpdateDropdownListLock.invoke();
    };


    $scope.svFetchGeneralDefaultsListonSuccess = function(variable, data, options) {
        // Collecting unique group for drop down values
        $scope.listValues = [];
        _.forEach(data.content, function(obj) {
            if (!$scope.listValues.includes(obj.listName)) {
                if (obj.listName != null) {
                    $scope.listValues.push(obj.listName);
                }
            }
        });
        if ($scope.listValues.length > 0) {
            $scope.Variables.svGetDropDownValuesForGroup.setInput({
                'list_item': $scope.listValues
            });
            $scope.Variables.svGetDropDownValuesForGroup.invoke();
        }
    };

    $scope.loadDataSet = function(listItem) {
        var dataSet = [];
        var defaultValue;
        _.forEach($scope.Variables.svGetDropDownValuesForGroup.dataSet.content, function(obj) {
            if (obj.listName == listItem) {
                dataSet.push(obj);
                if (obj.defaultValueInd == 'Y') {
                    defaultValue = obj;
                }
            }
        });
        return {
            'dataSet': dataSet,
            'defaultValue': defaultValue
        };
    };


    $scope.svGetDropDownValuesForGrouponSuccess = function(variable, data, options) {
        _.forEach($scope.Widgets.ListFetchGeneralDefaults.getWidgets('selectDefault'), function(obj) {
            var data = $scope.loadDataSet(obj.$parent.item.listName);
            obj.dataset = data.dataSet;
            obj.datavalue = data.defaultValue
        });

    };


    $scope.selectDefaultChange = function($event, $isolateScope, item, currentItemWidgets, newVal, oldVal) {
        var options = [];
        _.forEach($isolateScope.dataset, function(obj) {
            if (newVal.dropdownListItemId == obj.dropdownListItemId) {
                options.push({
                    "listItemId": obj.dropdownListItemId,
                    "defaultValue": "Y"
                })
            } else {
                options.push({
                    "listItemId": obj.dropdownListItemId,
                    "defaultValue": "N"
                })
            }
        })
        $scope.Variables.svUpdateDropDownDefault.dataBinding.RequestBody = options;
        $scope.Variables.svUpdateDropDownDefault.invoke();
    };



}]);