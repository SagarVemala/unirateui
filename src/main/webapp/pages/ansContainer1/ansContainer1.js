Application.$controller("ansContainer1PageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.text1Change = function($event, $isolateScope, newVal, oldVal) {

    };


    $scope.answersChange = function($event, $isolateScope, newVal, oldVal) {
        debugger;
        console.log("inside on change for answer default" + $scope.Widgets.answers.datavalue);

        var requestBody = {
            qmi: $scope.pageParams.qmi
        };

        $scope.Variables.updateAnswerLookupDefaultNo.setInput("RequestBody", requestBody);
        $scope.Variables.updateAnswerLookupDefaultNo.invoke();

        var requestBody1 = {
            qai: newVal
        };

        $scope.Variables.updateAnswerLookUpDefaultYes.setInput("RequestBody", requestBody1);
        $scope.Variables.updateAnswerLookUpDefaultYes.invoke();

    };

}]);