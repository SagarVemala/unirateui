Application.run(function($rootScope, $location, $http, $cookies) {
    "use strict";

    console.log($rootScope);

    // App level function to show custom notification to user
    $rootScope.notifyToUser = function(messageToShow, typeOfToast) {
        $rootScope.Variables.notifyUserToast.notify({
            message: messageToShow,
            class: typeOfToast
        });
    };
    var url = $location.url();

    var searchObject = $location.search();
    //console.log(localStorage);
    //console.log($cookies.get('auth_cookie'));
    //console.log($cookies.get('JSESSIONID'));
    //console.log($cookies.get('wm_xsrf_token'));
    if (searchObject.policyid) {
        // Put the object into storage
        localStorage.setItem('policyid', searchObject.policyid);
    }
    if (searchObject.userid) {
        // Put the object into storage
        localStorage.setItem('salesforce_userid', searchObject.userid);
    }
    if (searchObject.clientappid) {
        // Put the object into storage
        localStorage.setItem('clientappid', searchObject.clientappid);
    }
    if (searchObject.quotenumber) {
        // Put the object into storage
        localStorage.setItem('quotenumber', searchObject.quotenumber);
    }
    if (searchObject.aiuserid) {
        // Put the object into storage
        localStorage.setItem('aiuserid', searchObject.aiuserid);
    }

    if (url != '' && !searchObject.policyid) {
        localStorage.removeItem('policyid');
        localStorage.removeItem('salesforce_userid');
        localStorage.removeItem('clientappid');
        localStorage.removeItem('quotenumber');
        localStorage.removeItem('aiuserid');
    }





    /* perform any action on the variables within this block(on-page-load) */
    $rootScope.onAppVariablesReady = function() {
        /*
         * variables can be accessed through '$rootScope.Variables' property here
         * e.g. $rootScope.Variables.staticVariable1.getData()
         */

        /*if ($cookies.get('wm_xsrf_token')) {
             
            $rootScope.Variables.GetSecurityInfo.invoke();

        }*/
        console.log('loggedInUser');

        console.log($rootScope.Variables.userId);

        if ($rootScope.Variables.loggedInUser.dataSet.id) {
            localStorage.setItem('userId', $rootScope.Variables.loggedInUser.dataSet.id);
            $rootScope.userId = $rootScope.Variables.loggedInUser.dataSet.id;
        }

    };

    /* perform any action on session timeout here, e.g clearing some data, etc */
    $rootScope.onSessionTimeout = function() {
        /*
         * NOTE:
         * On re-login after session timeout:
         * if the same user logs in(through login dialog), app will retain its state
         * if a different user logs in, app will be reloaded and user is redirected to respective landing page configured in Security.
         */
        // 
    };

    /*
     * This application level callback function will be invoked after the invocation of PAGE level onPageReady function.
     * Use this function to write common logic across the pages in the application.
     * activePageName : name of the page
     * activePageScope: scope of the page
     * $activePageEl  : page jQuery element
     */
    $rootScope.onPageReady = function(activePageName, activePageScope, $activePageEl) {
        // 
    };


    $rootScope.onServiceError = function(source, errorMsg, xhrObj) {
        $rootScope.Variables.appNotification.setMessage("Oops!   Something went wrong but it's not your fault. Please see your manager");
    };

    $rootScope.GetQuoteXmlonSuccess = function(variable, data, options) {
        debugger
        data.content.forEach(function(obj) {
            var parser = new DOMParser();
            var xmlDoc = parser.parseFromString(obj.responseXml, "text/xml");
            var carrierTag = xmlDoc.getElementsByTagName("Carrier")
            var quoteExecutionID = xmlDoc.getElementsByTagName("QuoteExecutionID")
            var insuraMatchID = xmlDoc.getElementsByTagName("InsuraMatchID")
            var totalPremium = xmlDoc.getElementsByTagName("ns2:TotalPrem")
            var paidInFull = xmlDoc.getElementsByTagName("ns2:Amt")
            var term = xmlDoc.getElementsByTagName("ns2:Term")
            var discounts = xmlDoc.getElementsByTagName("ns2:Discount")
            var discountDetails = [];
            var underwritingMessages = [];
            var messages = xmlDoc.getElementsByTagName("ns2:Restriction")
            var pay = xmlDoc.getElementsByTagName("ns2:PPs")
            var status = xmlDoc.getElementsByTagName("Status")
            var statusText = xmlDoc.getElementsByTagName("StatusText")
            var paymentOptions = [];
            var paymentObject = {};

            for (var i = 0; i < discounts.length; i++) {
                discountDetails.push(discounts[i].getAttribute("name"));
            }
            for (var i = 0; i < messages.length; i++) {
                underwritingMessages.push(messages[i].children[0].textContent);
            }
            for (var i = 0; i < pay.length; i++) {
                paymentObject.plan = pay[i].getAttribute("Name")
                for (var j = 0; j < pay[i].children.length; j++) {
                    if (pay[i].children[j] != undefined) {
                        if (pay[i].children[j].getAttribute("name") == "TotalAmt") {
                            console.log("TotalAmt...")
                            paymentObject.amount = "$" + pay[i].children[j].textContent
                        }
                        if (pay[i].children[j].getAttribute("name") == "Desc") {
                            console.log("Desc...")

                            paymentObject.desc = pay[i].children[j].textContent
                        }
                        if (pay[i].children[j].getAttribute("name") == "DownPayment") {
                            console.log("DownPayment...")

                            paymentObject.downPay = "$" + pay[i].children[j].textContent
                        }
                        if (pay[i].children[j].getAttribute("name") == "InstallmentPayment") {
                            console.log("InstallmentPayment...")

                            paymentObject.installPay = "$" + pay[i].children[j].textContent
                        }
                    }
                }
                paymentOptions.push(paymentObject);
                var paymentObject = {};
            }

            var carrImage = null;
            if (carrierTag[0].getAttribute("Name") != null) {
                if (carrierTag[0].getAttribute("Name") == "Bristol West") {
                    carrImage = "resources/images/imagelists/logo-bristol-west.png"
                }
                if (carrierTag[0].getAttribute("Name").includes("MetLife Auto")) {
                    carrImage = "resources/images/imagelists/Metlife.png"
                }
                if (carrierTag[0].getAttribute("Name") == "Plymouth Rock Assurance NJ") {
                    carrImage = "resources/images/imagelists/Plymouthrock.png"
                }
                if (carrierTag[0].getAttribute("Name") == "Progressive Insurance") {
                    carrImage = "resources/images/imagelists/Progressive.png"
                }
                if (carrierTag[0].getAttribute("Name") == "Safeco Insurance") {
                    carrImage = "resources/images/imagelists/safeco_logo_staticNav.jpg"
                }
                if (carrierTag[0].getAttribute("Name") == "National General") {
                    carrImage = "resources/images/imagelists/national_general.png"
                }
                if (carrierTag[0].getAttribute("Name") == "Stillwater Property & Casualty") {

                    carrImage = "resources/images/imagelists/StillWater.png"
                }
                if (carrierTag[0].getAttribute("Name") == "Mercury Auto Insurance") {
                    carrImage = "resources/images/imagelists/Mercury.jpg"
                }
                if (carrierTag[0].getAttribute("Name") == "Dairyland Insurance") {
                    carrImage = "resources/images/imagelists/Dairyland.png"
                }
                if (carrierTag[0].getAttribute("Name") == "The General") {
                    carrImage = "resources/images/imagelists/The_General.jpg"
                }
                if (carrierTag[0].getAttribute("Name") == "Omni Insurance") {
                    carrImage = "resources/images/imagelists/Good2Go_Omni.png"
                }
                if (carrierTag[0].getAttribute("Name") == "Liberty Mutual") {
                    carrImage = "resources/images/imagelists/logo-lm.png"
                }

            }
            var carrierData = {
                "image": carrImage,
                "carrierName": carrierTag[0].getAttribute("Name"),
                "carrierId": carrierTag[0].getAttribute("ID"),
                "totalPremium": totalPremium.length > 0 ? "$" + totalPremium[0].textContent + " /" + term[0].textContent + " months" : "",
                "paidInFull": paidInFull.length > 0 ? "$" + paidInFull[0].textContent : "",
                "payDetails": paymentOptions.length > 0 ? paymentOptions : "",
                "discountCount": discounts.length > 0 ? discounts.length + " discounts" : "",
                "discountDetails": discountDetails.length > 0 ? discountDetails : "",
                "underwritingMessages": underwritingMessages.length > 0 ? underwritingMessages : "",
                "status": status[0] != undefined ? status[0].textContent : "",
                "statusText": statusText[0] != undefined ? statusText[0].textContent : ""
            }
            if ($rootScope.Variables.PusherCarrier.dataSet.filter(e => e.carrierName === carrierData.carrierName).length == 0) {
                $rootScope.Variables.PusherCarrier.dataSet.push(carrierData);
                /*                $rootScope.Widgets.PusherCarrierList1.binddataset;
                                $rootScope.$apply();*/
            }


        });

    };

    // $rootScope.loadAllDataForPolicy = function(polId) {
    //     debuugger;
    // }






    $rootScope.GetCarrierQuestionsonSuccess = function(variable, data, options) {
        // 
    };


    $rootScope.loginActiononSuccess = function(variable, data, options) {

        console.log(variable);
        console.log(data);
        console.log(options);



    };




    /*
        $rootScope.GetSecurityInfoonSuccess = function(variable, data, options) {

            console.log('Get Security Info');
             

        };*/

    // For Dev activities adding temporary userId
    /*    $rootScope.GetQuotesInvokeonBeforeUpdate = function(variable, inputData, options) {
            if (inputData.user_id == undefined) {
                inputData.user_id = null
            }
        };
    */
});